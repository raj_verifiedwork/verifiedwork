<?php
ob_start();
require_once('genfunctions.php');

setcookie("user_login_failed", 0, time()+3600);

$err="";
$cap = 'notEq';

?>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Verified Work :: Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <!-- bootstrap 3.0.2 -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/custom_styles.css" rel="stylesheet" type="text/css" />
	<!-- jQuery 2.0.2 -->
	<script src="assets/js/jquery.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
	<style>
	body{
		background: url("assets/img/noisebg.png") repeat scroll 0 0 #EEEEEE;
	}
	.form-box .body > .form-group > input, .form-box .footer > .form-group > input{
		border:1px solid #ddd;
	}
	.header1{
		background: #fff;
		border-radius: 4px 4px 0 0;
		padding:10px;
		font-weight:bold;
		display: inline-block;
		width: 100%;
	}

	.form_box_new{
	
		background: linear-gradient(to bottom, rgba(255, 255, 255, 0.15) 0%, rgba(0, 0, 0, 0.15) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
		border-right: 1px solid #989899;
		border-left: 1px solid #989899;
		border-top: 1px solid #989899;
		box-shadow: 0 -6px rgba(0, 0, 0, 0.35) inset, 0 -1px rgba(0, 0, 0, 0.15) inset, 0 5px 4px rgba(0, 0, 0, 0.12), 0 -7px 2px rgba(255, 255, 255, 0.8) inset;
		display: block;
		background: #fff;
		position: relative;
		text-decoration: none;
		text-shadow: 0 1px 0 #FFFFFF;

	}

	</style>
    </head>
    <body>
	<!--<div style='position:absolute;z-index:-999;left:0;top:0;width:100%;height:100%'>
	  <img src='assets/img/coming_soon_bg.jpg' style='width:100%;height:100%' alt='VerifiedWork' />
	</div>-->
<?php

$err="";

if(isset($_POST['create']))
{
	$subdomain=safe_sql_nq(request_get('subdomainname'));
	$useremail=safe_sql_nq(request_get('useremail'));
	$regtype=request_get('regtype');

	$created_ip=$_SERVER['REMOTE_ADDR'];

	$created=date('Y-m-d H:i:s');

	$results = mysql_query("SELECT id FROM subdomain_list WHERE subdomain='$subdomain'");
	
	$username_exist = mysql_num_rows($results);
	
	if($username_exist) {

		$err='<div class="callout callout-danger">'.__USERNAME_EXISTS__.'</div>';
		
	}else{
		
		if($regtype=="0")
		{
			$token = sha1(uniqid($subdomain, true));

			$sql="INSERT INTO `subdomain_list` (`id`, `subdomain`, `email`, `created_ip`, `last_active`, `status`, `referral_url`, `verification_code`, `verified`, `createdby`, `created`, `modifiedby`, `modified`) VALUES (NULL, '$subdomain', '$useremail', '$created_ip', '-', '2', 'direct_signup', '$token', '0000-00-00 00:00:00', 'self', '$created', 'self', '$created')";
			mysql_query($sql) or die(mysql_error());

			$refid=mysql_insert_id();

			/*Start Sending Email*/
			$emres=mysql_query("select * from email_templates where emailtype='__SIGNUP_VERIFICATION__'") or die(mysql_error());
			$emr=mysql_fetch_array($emres);

			$subject=$emr['subject'];

			$url = "http://verifiedwork.com/activate.php?token=$token&refid=$refid";

			$message=$emr['content'];

			$messageNew=html_entity_decode(str_replace("%url%",$url,$message));

			send_mail_with_smtp($useremail,$subject,$messageNew);
			/*End Sending Email*/

			header("location:login.php?msg=verify");
			exit();
		}else{
			echo "TEST ELSE";
		}

	}

}

if(isset($_POST['creategoogle']))
{
	$subdomain=request_get('subdomainname');
	$useremail=request_get('useremail');
	$regtype=request_get('regtype');

	$user_name=request_get('user_name');
	$user_img=request_get('user_img');

	$created_ip=$_SERVER['REMOTE_ADDR'];

	$created=date('Y-m-d H:i:s');

	$results = mysql_query("SELECT id FROM subdomain_list WHERE subdomain='$subdomain'");
	
	$username_exist = mysql_num_rows($results);
	
	if($username_exist) {

		$err='<div class="callout callout-danger">'.__USERNAME_EXISTS__.'</div>';
		
	}else{

		$auth_userid=request_get('userid');

		$signupedby=$regtype."_signup";

		$sql="INSERT INTO `subdomain_list` (`id`, `subdomain`, `email`, `created_ip`, `last_active`, `status`, `referral_url`, `verification_code`, `verified`, `createdby`, `created`, `modifiedby`, `modified`) VALUES (NULL, '$subdomain', '$useremail', '$created_ip', '-', '0', '$signupedby', '$auth_userid', '$created', '$regtype', '$created', '$regtype', '$created')";

		mysql_query($sql) or die(mysql_error());

		$refidnew=mysql_insert_id();

		$user_sql="INSERT INTO `users` (`id`, `subdomainid`, `email`, `password`, `fname`, `lanme`, `nickname`, `show_name_options`, `login_with`, `timezone`, `status`, `last_login`, `mail_preference`, `photo`, `usertype`, `created`, `createdby`, `modified`, `modifiedby`) VALUES (NULL, '$refidnew', '$useremail', '', '$user_name', '', '', '0', '1', 'timezone', '0', '', '0', '$user_img', '1', '$created', 'google', '$created', 'google')";	

		$res=mysql_query($user_sql)or die("ERROR : ".mysql_error());

		/*Start Sending Email*/
		$emres=mysql_query("select * from email_templates where emailtype='__WELCOME_EMAIL__'") or die(mysql_error());
		$emr=mysql_fetch_array($emres);

		$subject=$emr['subject'];

		$email_username = $user_name;

		$message=$emr['content'];

		$messageNew=html_entity_decode(str_replace("%username%",$email_username,$message));

		send_mail_with_smtp($useremail,$subject,$messageNew);

		/*End of Sending Email*/

		session_start();

		set_session('VW_USER_ID',$userid);
		set_session('VW_DOMAIN_ID',$refidnew);
		set_session('VW_USER_EMAIL',$useremail);
		set_session('VW_USER_NAME',$user_name);
		set_session('VW_DOMAIN_URL',$subdomain);
		set_session('VW_USER_LOGIN_WITH',1);

		$newurl="http://".$subdomain.".verifiedwork.com/index.php?email=$useremail";

		header("location:$newurl");
		exit();



	}

}

if(isset($_REQUEST['signup']))
{
	if($_REQUEST['signup']=="gmail")
	{

		$sql="SELECT * FROM `live_domain_users` where email='$email' and status='0' and login_with='1'";

		$ures=mysql_query($sql) or die("ERROR : ".mysql_error());
		
		if(mysql_num_rows($ures)>0)
		{
			echo "login success";

			if(mysql_num_rows($ures)==1)
			{
				$sr=mysql_fetch_array($ures);

				$subdomainname=$sr['subdomain'];

				$newurl="http://".$subdomainname.".verifiedwork.com/index.php?email=$email";
				
				header("location:$newurl");
				exit;
			}else{

				$chooseLogin='<div class="form-box" id="login-box">
						    <div class="header" style="padding: 5px 10px;">Log In To Your <br>Verified Work Account</div>
							<div class="body bg-gray">
							    <div class="form-group">
				The following Verified Work systems were found with accounts using the email address you entered. To log in to your system select your login page below.
							    </div>';

				while($sr=mysql_fetch_array($ures))
				{
					$url="http://".$sr['subdomain'].".verifiedwork.com/index.php";

					$usertype="";

					if($sr['usertype']=="0")
						$usertype="User";
					else if($sr['usertype']=="1")
						$usertype="Administrator";
					else
						$usertype="Super Admin";

					$chooseLogin.='<div class="form-group"><a class="btn btn-success btn-sm" href="'.$url.'">'.$sr['subdomain'].'</a> &nbsp; '.$usertype.'&nbsp;</div>';
				}
        
				$chooseLogin.='
						</div>
						<div class="footer">                                                               
						   Click the account you would like to log in to
						</div>
				       </div>';

				echo $chooseLogin;

			}

		}

	} // end signup

}

if(isset($_POST['login']))
{

	$email=safe_sql_nq(request_get('email'));
	$password=safe_sql_nq(request_get('password'));

	if(isset($_COOKIE["user_login_failed"]))
	{
		$ulf_count_cc=$_COOKIE["user_login_failed"];

		if($ulf_count_cc>5)
		{
			$sql="UPDATE `users` set status='3' where email='$email' and status='0'";

			$ures=mysql_query($sql) or die("ERROR : ".mysql_error());
		}
		elseif($ulf_count_cc<3)
		{
			$sql="SELECT * FROM `live_domain_users` where `email`='$email' and `password`='$password' and `status`='0'";
			//echo $sql;
			$ures=mysql_query($sql) or die("ERROR : ".mysql_error());
			$ulf_count=1; // user_login_failed_count

			if(mysql_num_rows($ures)>0)
			{
				

				if(mysql_num_rows($ures)==1)
				{
					$sr=mysql_fetch_array($ures);
			
					$subdomainname=$sr['subdomain'];

					$newurl="http://".$subdomainname.".verifiedwork.com/index.php?email=".$email;

					header("location:$newurl");
					exit();

				}else{

					echo '<script>
						$(document).ready(function(){
							$("#SignIn").hide();
							$("#CreateAccount").hide();
						});	
					     </script>';


					$chooseLogin='<div class="form-box form_box_new" id="login-box">
						   	<div class="header1">
								<img src="assets/img/vw_logo.png" width="30%" style="float:left;"/> 
								<span style="float:right;">Log In </span> 
							</div>
							<div class="body bg-white">
							    <div class="form-group">
				The following Verified Work systems were found with accounts using the email address you entered. To log in to your system select your login page below.
							    </div>';

					while($sr=mysql_fetch_array($ures))
					{
						$url="http://".$sr['subdomain'].".verifiedwork.com/index.php?email=$email";

						$usertype="";

						if($sr['usertype']=="0")
							$usertype="User";
						else if($sr['usertype']=="1")
							$usertype="Administrator";
						else
							$usertype="Super Admin";

					$chooseLogin.='<div class="form-group"><a class="btn btn-success btn-sm" href="'.$url.'">'.$sr['subdomain'].'</a> &nbsp; '.$usertype.'&nbsp;</div>';
					}
        
					$chooseLogin.='
							</div>
							<div class="footer">                                                               
							   Click the account you would like to log in to
							</div> <br><br>
					       </div>';

					echo $chooseLogin;

				}

				$ulf_count=1;
			}else{
				$err="Login Failed! ";
				$ulf_count_cc=$ulf_count_cc+$ulf_count;
				setcookie("user_login_failed", $ulf_count_cc, time()+3600);
			}

		}else{

			if (request_get('captcha') == $_COOKIE['cap_code']) {
				// Captcha verification is Correct. Do something here!

				$sql="SELECT * FROM `users` where `email`='$email' and `password`='$password' and `status`='0'";

				$ures=mysql_query($sql) or die("ERROR : ".mysql_error());
				$ulf_count=1; // user_login_failed_count

				if(mysql_num_rows($ures)>0)
				{
					// login success
					$ur=mysql_fetch_array($ures);
					if(mysql_num_rows($ures)==1)
					{
						$sr=mysql_fetch_array($ures);

						$subdomainname=$sr['subdomain'];

						$newurl="http://".$subdomainname.".verifiedwork.com/index.php?email=$email";

						header("location:$newurl");
						exit();

					}else{

						echo '<script>
							$(document).ready(function(){
								$("#SignIn").hide();
								$("#CreateAccount").hide();
							});	
						     </script>';


					$chooseLogin='<div class="form-box form_box_new" id="login-box">
						<div class="header1">
							<img src="assets/img/vw_logo.png" width="30%" style="float:left;"/> 
							<span style="float:right;">Log In </span> 
						</div>
								<div class="body bg-white">
								    <div class="form-group">
					The following Verified Work systems were found with accounts using the email address you entered. To log in to your system select your login page below.
								    </div>';

						while($sr=mysql_fetch_array($ures))
						{
							$url="http://".$sr['subdomain'].".verifiedwork.com/index.php?email=$email";

							$usertype="";

							if($sr['usertype']=="0")
								$usertype="User";
							else if($sr['usertype']=="1")
								$usertype="Administrator";
							else
								$usertype="Super Admin";

					$chooseLogin.='<div class="form-group"><a class="btn btn-success btn-sm" href="'.$url.'">'.$sr['subdomain'].'</a> &nbsp; '.$usertype.'&nbsp;</div>';
						}
		
						$chooseLogin.='
								</div>
								<div class="footer">                                                               
								   Click the account you would like to log in to
								</div><br><br>
						       </div>';

						echo $chooseLogin;

					}

						$ulf_count=1;
					}else{
						$ulf_count_cc=$ulf_count_cc+$ulf_count;
						setcookie("user_login_failed", $ulf_count_cc, time()+3600);
					}

				$cap = 'Eq';
			} else {
				// Captcha verification is wrong. Take other action
				$cap = '';
			}
		
			$ulf_count_cc=$ulf_count_cc+$ulf_count;
			setcookie("user_login_failed", $ulf_count_cc, time()+3600);
		}



	}


}

?>

<form method="post">

      	<div class="form-box form_box_new" id="SignIn">
	<?php
		if(isset($err))
		{
			echo '<script>
				$(document).ready(function(){
					$("#err_disp").show();
				});	
			     </script>';

			echo  '<div id="err_disp"> <div class="callout callout-danger">'.$err.'</div> </div>';
		}

		if(isset($_REQUEST['msg'])=="verify")
		{
			echo '<div class="callout callout-info">'.__VERIFICATION_ALERT__.'</div>';
		}
	?>
	
	<?php 
		if(isset($_REQUEST['signup'])!="gmail")
		{
	?>

            <div class="header1"><img src="assets/img/vw_logo.png" width="30%" style="float:left;"/> <span style="float:right;">Sign In </span> </div>
            <form method="post">
                <div class="body bg-white">
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="E-Mail" required />
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password" required />
                    </div>
		   <?php
		
		if(isset($_COOKIE["user_login_failed"]))
		{

			$ulf_count_cc=$_COOKIE["user_login_failed"];

			//echo " ULF_count_cc : ".$ulf_count_cc;

			if($ulf_count_cc>1)
			{ 
		   ?>
		    <div class="input-group">
			<input type="text" name="captcha" id="captcha" value="" class="form-control" maxlength="6" size="6" required />
			<div class="input-group-addon" style="padding:0px;">
				<img src="captcha.php" width="64px;"/>
			</div>
		    </div>
		    </div>
                    <div class="footer">                                                               
                     	<button type="submit" name="login" id="login3" class="btn bg-olive btn-block">Sign in </button>  
                    
                    	<p><a href="#">I forgot my password</a></p>
                    
                    	<a id="ca" href="#" class="text-center">Register a new account</a>
                    </div>
		   <?php 
			}else{
		   ?>
                </div>
                <div class="footer">                                                               
                    <button type="submit" name="login" class="btn btn-danger">Sign in</button>  
                    
                    <p><a href="#">I forgot my password</a></p>
                    
                    <a id="ca" href="#" class="text-center">Register a new account</a>
                </div>
		<?php
		}

		}else{
		?>
		</div>
                <div class="footer">                                                               
                    <button type="submit" name="login" class="btn btn-danger">Sign in</button>  
                    
                    <p><a href="#">I forgot my password</a></p>
                    
                    <a id="ca" href="#" class="text-center">Register a new account</a>
                </div>
		<?php 
		}
		?>
            </form>

           <div class="margin text-center">
               <!-- <span style="color:green; font-weight:bold;">Sign in using social networks</span>-->
		<a class="btn btn-block btn-social btn-google-plus" href="<?php echo $authUrl; ?>" ><i class="fa fa-google-plus"></i> Sign in with Google </a> 
		<a class="btn btn-block btn-social btn-foursquare" href="login_with_todoist.php?job=login" ><i class="fa fa-foursquare"></i>Sign in with Todoist.com</a>
            </div><br>
        </div>


	<div class="form-box form_box_new" id="CreateAccount">
            <div class="header1"><img src="assets/img/vw_logo.png" width="30%" style="float:left;"/> <span style="float:right;">Register Account</span> </div>
            <form method="post">
                <div class="body bg-white">
                    <div class="form-group">
			<div class="input-group">
			<span class="input-group-addon">http://</span>
			<input class="form-control" type="text" name="subdomainname" id="subdomainname" placeholder="Your Login Page" maxlength="24" required>
			<span class="input-group-addon">.verifiedwork.com <span id="user-result"></span> </span>
			</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
			<span class="input-group-addon">
			E-Mail
			</span>
			<input class="form-control" type="email" name="useremail" placeholder="Your E-Mail" required >
			</div>
                    </div>
		    <div class="form-group">
                        <span>
				<input type="checkbox" onClick="EnableSubmit(this)" /> &nbsp;
				I agree to Verified Work terms.
			</span>    
                    </div>

                </div>
                <div class="footer">                    
		     <input class="form-control" type="hidden" name="regtype" value="0" >
                    <button type="submit" name="create" id="Accept" class="btn btn-danger">Create</button>
			<br>
                    <a href="#" id="si" class="text-center">I already have an account</a>
                </div>
            </form>
		<hr>
            <div class="margin text-center">
               <span>Register using social networks</span><br><br>

   		<!-- <a class="btn bg-red btn-circle" href="<?php echo $authUrl; ?>"><i class="fa fa-google-plus"></i></a> --> 
		<a class="btn btn-block btn-social btn-google-plus" href="<?php echo $authUrl; ?>" ><i class="fa fa-google-plus"></i> Sign in with Google </a> 
		<a class="btn btn-block btn-social btn-foursquare" href="login_with_todoist.php?job=signup" ><i class="fa fa-foursquare"></i>Sign in with Todoist.com</a>           
		 <!-- <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
                <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button> -->               
		<!--<a class="logout" href="?reset=1">Logout</a>-->
		<br/>
            </div>
        </div>
	<?php 
		}else{
	?>
	<div class="form-box form_box_new" id="CreateAccountWithGoogle">
	    <div class="header1"><img src="assets/img/vw_logo.png" width="30%" style="float:left;"/> <span style="float:right;">Register New Account </span> </div>
            <form method="post">
                <div class="body bg-white">
			<b>Welcome <?php echo $user_name; ?> !</b>
                    <div class="form-group">
			<div class="input-group">
			<span class="input-group-addon">http://</span>
			<input class="form-control" type="text" name="subdomainname" id="subdomainname" placeholder="Your Login Page" maxlength="24" required>
			<span class="input-group-addon">.verifiedwork.com <span id="user-result"></span> </span>
			</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
			Your E-Mail : <?php echo $email; ?>
			<input class="form-control" type="hidden" name="useremail" placeholder="Your E-Mail" value="<?php echo $email; ?>" >
			</div>
                    </div>
		    <div class="form-group">
                        <span>
				<input type="checkbox" onClick="EnableSubmit(this)" /> &nbsp;
				I agree to Verified Work terms.
			</span>    
                    </div>

                </div>
                <div class="footer">                    
		    <input class="form-control" type="hidden" name="regtype" value="google" >
		    <input class="form-control" type="hidden" name="userid" value="<?php echo $user_id; ?>" >
		    <input type="hidden" name="user_name" value="<?php echo $user_name; ?>"/>
		    <input type="hidden" name="user_img" value="<?php echo $profile_image_url; ?>"/>
                    <button type="submit" name="creategoogle" id="Accept" class="btn btn-danger">Create</button>
			<br><br>
                    <a href="#" id="si" class="text-center">I already have an account</a>
                </div>
            </form>

            <div class="margin text-center">
              <!--  <span>Register using social networks</span>-->
                <br/>
		<!-- <a class="btn bg-red btn-circle" href="<?php echo $authUrl; ?>"><i class="fa fa-google-plus"></i></a> 
		<a class="btn btn-block btn-social btn-google-plus" href="<?php echo $authUrl; ?>" ><i class="fa fa-google-plus"></i> Sign in with Google </a> -->
                <!--  <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
                <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button> -->
		
            </div>
        </div>
	<?php
		}
	?>
</form>

       <div class="pagecenter_div">
	
		   <div class="col-md-12">
                           
                        </div><!-- /.col -->
	</div>
  
        <!-- Bootstrap -->
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>        
	<script type="text/javascript">

	EnableSubmit = function(val)
	{
	    var sbmt = document.getElementById("Accept");

	    if (val.checked == true)
	    {
		sbmt.disabled = false;
	    }
	    else
	    {
		sbmt.disabled = true;
	    }
	}    

	$(document).ready(function(){

		setTimeout(function() {
		    $('#err_disp').fadeOut('fast');
		}, 10000);

		$('#err_disp').hide();

		document.getElementById("Accept").disabled = true;

		$("#ca").click(function(){
			$("#SignIn").hide();
			$("#CreateAccount").show();
		});

		$("#si").click(function(){
			$("#SignIn").show();
			$("#CreateAccount").hide();
		});

		$("#CreateAccount").hide();

		$("#subdomainname").keyup(function (e) {
			//removes spaces from username
			$(this).val($(this).val().replace(/\s/g, ''));
		});

		$("#subdomainname").blur(function (e) {

			//removes spaces from username
			$(this).val($(this).val().replace(/\s/g, ''));

			var subdomainname = $(this).val();
			if(subdomainname.length < 2){$("#user-result").html('');return;}

			if(subdomainname.length >= 2){
				$("#user-result").html('<img src="assets/img/ajax-loader.gif" width="18px" >');
				$.post('check_domain.php', {'subdomainname':subdomainname }, function(data) {
		  			$("#user-result").html(data);
				});
			}

		});

	});

	</script>
    </body>
</html>
<?php
ob_end_flush();
?>
