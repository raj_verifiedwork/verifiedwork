<?php
session_start();
require_once('genfunctions.php');

if(isset($_REQUEST['type']))
{

	$cur_user_id=get_session('VW_USER_ID');
	$cur_domain_id=get_session('VW_DOMAIN_ID');
	$cur_username=get_session('VW_USER_NAME');
	$cur_email=get_session('VW_USER_EMAIL');

	$created=date('Y-m-d H:i:s');

	if($_REQUEST['type']=="addNewTask")
	{

		$taskname=safe_sql_nq(request_get('taskname'));
		$esthrs=safe_sql_nq(request_get('esthrs'));
		$duedate=date('Y-m-d', strtotime(request_get('duedate')));
		$pid=request_get('pid');

		$created=date('Y-m-d H:i:s');

		$assigned_to=0;

		$sql="INSERT INTO `tasks` ( `id` , `pid` , `userid` , `username` , `taskname` , `duedate` , `date_reschedule_dtls` , `est_hrs` , `milestone` , `assigned_to` , `assigned_toname` , `priority` , `status` , `created` , `createdby` , `modified` , `modifiedby` ) VALUES ( NULL , '$pid', '$cur_user_id', '$cur_username', '$taskname', '$duedate', '-', '$esthrs', '0', '$assigned_to', '-', '0', '0', '$created', '$cur_username', '$created', '$cur_username')";

		$res=mysql_query($sql) or die("ERROR : ".mysql_error());

		$new_tid=mysql_insert_id();

		$taskid=$new_tid;
		$npid=$pid;

		$esthrss='';

		if($esthrs==""){
			$esthrss='';
		}else if($esthrs=="0"){
			$esthrss='';
		}else{
			$esthrss='<small class="label label-primary"><i class="fa fa-clock-o"></i> '.$esthrs.' hrs </small>';
		}

		$imgpath="";

		if($assigned_to!="0")
		{
			$sql=mysql_query("select id,email,photo,login_with from users where id='$assigned_to' and status='0'") or die("ERROR : ".mysql_error());

			$ur=mysql_fetch_array($sql);
			if($ur['photo']=="")
				$imgpath="assets/img/noavatar.png";
			else{
				if($ur['login_with']==1)
					$imgpath=$ur['photo'];
				else 
					$imgpath=$userimgpath.$ur['photo'];
			}
		}else{
			$imgpath="assets/img/noavatar.png";
		}

		$iscompleted="";
		$ischked="";
		
		$taskvid="tv-".$taskid;
		$taskeid="te-".$taskid;
		$taskaid="ta-".$taskid;
		$taskcmtcntid="tcmt-".$taskid;

		$due_date=date('d-m-Y',strtotime($created));

		$duedate1=request_get('duedate');

		$due_date_show="";
		if($duedate=="0000-00-00 00:00:00")
		{
			$due_date=date('d-m-Y',strtotime($created));
			$due_date_show="";
		}
		else{
			$due_date=date('d-m-Y',strtotime($due_date));

			if($duedate1!="")
				$due_date_show='<small class="label label-success" title="Due Date">'.date('M d',strtotime($duedate1)).'</small>';
			else
				$due_date_show="";
		}

		$cmt_count="";

		$task_res_cnt=mysql_query("SELECT count(*) as taskcmts FROM task_comments WHERE taskid='$taskid'") or die("ERROR : ".mysql_error());
		$r_cnt=mysql_fetch_array($task_res_cnt);

		if($r_cnt['taskcmts']>0)			
			$cmt_count=$r_cnt['taskcmts'];
		else
			$cmt_count="";

                $taskList.='<li id="'.$taskid.'" seqid="'.$i.'" > 
					<div id="taskdiv-'.$taskid.'">
					<div class="viewtask" id="'.$taskvid.'">

					<table border="0" width="100%" class="table table-condensed" style="display:inline;">
						<tr>
							<td style="border-top:none;">
								<span class="handle">
									<i class="fa fa-th"></i>
								</span>
							</td>
							<td style="border-top:none;" valign="top">
				<input type="checkbox" id="cbk-'.$taskid.'" value="" '.$ischked.' name="completetask" onclick="javascript:return completetask('.$taskid.','.$npid.');" />
							</td>
							<td width="75%" style="border-top:none;" valign="top">

			<span class="text" > <span class="edittaskshow" id="'.$taskaid.'"  >'.$taskname.' &nbsp;&nbsp; </span> 
			<span style="opacity:0.5; cursor:pointer;" title="Task Comments"  onclick="javascript:return showtaskcomments('.$taskid.','.$npid.');" data-toggle="modal" data-target="#comments-modal"><i class="glyphicon glyphicon-comment"></i>&nbsp;
			<span id="'.$taskcmtcntid.'"> '.$cmt_count.' </span>&nbsp;</span>&nbsp;
			<span style="opacity:0.5; cursor:pointer;" title="Completed Hours" onclick="javascript:return showtaskhrs('.$taskid.','.$npid.');" data-toggle="modal" data-target="#updatehrs-modal"><i class="glyphicon glyphicon-dashboard"></i>&nbsp;</span> &nbsp; 
			</span> 

							</td>
							<td width="10%" style="border-top:none;" valign="top"><span class="label label-primary"><i class="fa fa-clock-o"></i> '.$esthrs.' hrs</span>&nbsp;&nbsp;<span>'.$due_date_show.'</span> </td>
							<td width="5%" style="border-top:none;" valign="top">
				<img class="img-circle ap" assignedto="'.$assigned_to.'" pid="'.$npid.'" taskid="'.$taskid.'" alt="User Image" src="'.$imgpath.'" width="60%">
							</td>
							<td width="5%" style="border-top:none;" valign="top">
								<div class="tools">
									<i class="fa fa-trash-o" onclick="deleteTask('.$taskid.');"></i>
								</div>
							</td>
						</tr>
					</table>

					</div>
					<div class="edittask" id="'.$taskeid.'">
						<div class="row">
							<div class="col-xs-8">
			<textarea class="form-control" type="text" name="taskname-'.$taskid.'" id="taskname-'.$taskid.'" placeholder="Task name" required >'.$taskname.'</textarea>
							</div>
							<div class="col-xs-2">
			<input class="form-control" type="text" name="esthrs-'.$taskid.'" id="esthrs-'.$taskid.'" value="'.$esthrs.'" placeholder="Est.Hrs">
							</div>
							<div class="col-xs-2">
			<input class="form-control duedate" type="text" name="duedate-'.$taskid.'" id="duedate-'.$taskid.'" value="'.$due_date.'" placeholder="Due Date">
							</div>
						</div><br>
						<div class="row">
							<div class="col-xs-12">
					<button type="button" id="updTask-'.$taskid.'" class="btn btn-danger pull-left editupd"> Save </button> &nbsp; &nbsp;
						<button type="button" id="cancelEditTask-'.$taskid.'" class="btn btn-default editcancel" > Cancel </button>
							</div>
						</div>
					</div>
                                    </div>  </li>';
		echo $taskList;

	}

	if($_REQUEST['type']=="updTask")
	{
		$taskid=request_get('taskid');
		$taskname=safe_sql_nq(request_get('taskname'));
		$esthrs=safe_sql_nq(request_get('esthrs'));
		$duedate=date('Y-m-d', strtotime(request_get('duedate')));
		$pid=request_get('pid');

		$duedate1=request_get('duedate');

		$created=date('Y-m-d H:i:s');

		$sql="UPDATE `tasks` SET `taskname` = '$taskname', `duedate` = '$duedate', `est_hrs` = '$esthrs' WHERE `id` ='$taskid'";

		$res=mysql_query($sql) or die("ERROR : ".mysql_error());

		$taskvid="tv-".$taskid;
		$taskeid="te-".$taskid;
		$taskaid="ta-".$taskid;
		$taskcmtcntid="tcmt-".$taskid;

		$npid=$pid;

		$esthrss='';

		if($esthrs==""){
			$esthrss='';
		}else if($esthrs=="0"){
			$esthrss='';
		}else{
			$esthrss='<small class="label label-primary"><i class="fa fa-clock-o"></i> '.$esthrs.' hrs </small>';
		}
		
		$iscompleted="";
		$ischked="";

		$due_date=date('d-m-Y',strtotime($created));
		
		$due_date_show="";
		if($duedate=="0000-00-00 00:00:00")
		{
			$due_date=date('d-m-Y',strtotime($created));
			$due_date_show="";
		}
		else{
			$due_date=date('d-m-Y',strtotime($due_date));
			$due_date_show='<small class="label label-success" title="Due Date">'.date('M d',strtotime($duedate1)).'</small>';
		}

		$cmt_count="";

		$task_res_cnt=mysql_query("SELECT count(*) as taskcmts FROM task_comments WHERE taskid='$taskid'") or die("ERROR : ".mysql_error());
		$r_cnt=mysql_fetch_array($task_res_cnt);

		if($r_cnt['taskcmts']>0)			
			$cmt_count=$r_cnt['taskcmts'];
		else
			$cmt_count="";

		$assigned_to=0;
		//$assigned_to=$r['assigned_to'];
		$imgpath="";

		$sql=mysql_query("select id,email,photo,login_with from users where id='$assigned_to' and status='0'") or die("ERROR : ".mysql_error());

		$ur=mysql_fetch_array($sql);
		if($ur['photo']=="")
			$imgpath="assets/img/noavatar.png";
		else{
			if($ur['login_with']==1)
				$imgpath=$ur['photo'];
			else 
				$imgpath=$userimgpath.$ur['photo'];
		}


                $taskList.='<div class="viewtask" id="'.$taskvid.'">
				
				<table border="0" width="100%" class="table table-condensed" style="display:inline;">
					<tr>
						<td style="border-top:none;">
							<span class="handle">
								<i class="fa fa-th"></i>
							</span>
						</td>
						<td style="border-top:none;" valign="top">
			<input type="checkbox" id="cbk-'.$taskid.'" value="" '.$ischked.' name="completetask" onclick="javascript:return completetask('.$taskid.','.$npid.');" />
						</td>
						<td width="75%" style="border-top:none;" valign="top">

		<span class="text edittaskshow" id="'.$taskaid.'" >'.$taskname.' &nbsp;&nbsp; 
		<span style="opacity:0.5; cursor:pointer;" title="Task Comments"  onclick="javascript:return showtaskcomments('.$taskid.','.$npid.');" data-toggle="modal" data-target="#comments-modal"><i class="glyphicon glyphicon-comment"></i>&nbsp;
		<span id="'.$taskcmtcntid.'"> '.$cmt_count.' </span>&nbsp;</span>&nbsp;
		<span style="opacity:0.5; cursor:pointer;" title="Completed Hours" onclick="javascript:return showtaskhrs('.$taskid.','.$npid.');" data-toggle="modal" data-target="#updatehrs-modal"><i class="glyphicon glyphicon-dashboard"></i>&nbsp;</span> &nbsp; 
		</span> 

						</td>
						<td width="10%" style="border-top:none;" valign="top"><span>'.$esthrss.'</span>&nbsp;&nbsp;<span>'.$due_date_show.'</span></td>
						<td width="5%" style="border-top:none;" valign="top">
			<img class="img-circle ap" assignedto="'.$assigned_to.'" pid="'.$npid.'" taskid="'.$taskid.'" alt="User Image" src="'.$imgpath.'" width="60%">
						</td>
						<td width="5%" style="border-top:none;" valign="top">
							<div class="tools">
								<i class="fa fa-trash-o" onclick="deleteTask('.$taskid.');"></i>
							</div>
						</td>
					</tr>
				</table>

				</div>
				<div class="edittask" id="'.$taskeid.'">
					<div class="row">
						<div class="col-xs-8">
		<textarea class="form-control" type="text" name="taskname-'.$taskid.'" id="taskname-'.$taskid.'" placeholder="Task name" required >'.$taskname.'</textarea>
						</div>
						<div class="col-xs-2">
		<input class="form-control" type="text" name="esthrs-'.$taskid.'" id="esthrs-'.$taskid.'" value="'.$esthrs.'" placeholder="Est.Hrs">
						</div>
						<div class="col-xs-2">
		<input class="form-control duedate" type="text" name="duedate-'.$taskid.'" id="duedate-'.$taskid.'" value="'.$due_date.'" placeholder="Due Date">
						</div>
					</div><br>
					<div class="row">
						<div class="col-xs-12">
				<button type="button" id="updTask-'.$taskid.'" class="btn btn-danger pull-left editupd"> Save </button> &nbsp; &nbsp;
					<button type="button" id="cancelEditTask-'.$taskid.'" class="btn btn-default editcancel" > Cancel </button>
						</div>
					</div>
				</div>';
		echo $taskList;

	}

	if($_REQUEST['type']=="taskcompletionhrsHTML")
	{		
		$taskid=request_get('taskid');
		$task_res=mysql_query("SELECT * FROM tasks WHERE id='$taskid'") or die("ERROR : ".mysql_error());

		$r=mysql_fetch_array($task_res);

		$table="<table width='100%' class='table table-bordered table-condensed'>";
		$table.="<tr>";
			$table.="<td width='5%'> &nbsp; </td>";
			$table.="<td width='80%'>".$r['taskname']."</td>";
			$table.="<td width='10%'> &nbsp; </td>";
			$table.="<td width='5%'> &nbsp;</td>";
		$table.="<tr>";
		$table.="</table>";
		$table.="<div>
				<table width='100%' class='table table-bordered table-condensed table-striped'>
					<tr><th>User</th><th>Worked date</th><th>Hours</th><th>&nbsp;</th></tr>";
				
				$sql=mysql_query("select * from task_compeletion_hrs where `taskid`='$taskid'") or die("ERROR : ".mysql_error());
				
				while($t=mysql_fetch_array($sql))
				{
					$usrid=$t['userid'];

					$tid=$t['id'];

					$ures=mysql_query("select id,fname,email from users where id='$usrid'")or die("ERROR : ".mysql_error());
					$ur=mysql_fetch_array($ures);
					
					$username="";
					if($usrid==$cur_user_id)
						$username="me";
					else{
						if($ur['fname']!="")
							$username=$ur['fname'];
						else
							$username=$ur['email'];
					}
					$workeddate=$t['workdate'];
					$hours=$t['hours'];
					$table.="<tr>
							<td>".$username."</td>
							<td>".date("M d Y",strtotime($workeddate))."</td>
							<td>".$hours."</td>
							<td width='5%'><i class='fa fa-trash-o' style='cursor:pointer;' onclick='deleteTaskCmtHrs(".$tid.",this);'></i></td>
						</tr>";
				}

				$table.="</table>
			</div>";
		
		echo $table;

	}

	if($_REQUEST['type']=="todoist_taskcompletionhrsHTML")
	{		
		$taskid=request_get('taskid');
		
		$table.="<div>
				<table width='100%' class='table table-bordered table-condensed table-striped'>
					<tr><th>User</th><th>Worked date</th><th>Hours</th><th>&nbsp;</th></tr>";
				
				$sql=mysql_query("select * from task_compeletion_hrs where `taskid`='$taskid'") or die("ERROR : ".mysql_error());
				$total_hrs=0;
				while($t=mysql_fetch_array($sql))
				{
					$usrid=$t['userid'];

					$tid=$t['id'];

					$ures=mysql_query("select id,fname,email from users where id='$usrid'")or die("ERROR : ".mysql_error());
					$ur=mysql_fetch_array($ures);
					
					$username="";
					if($usrid==$cur_user_id)
						$username="me";
					else{
						if($ur['fname']!="")
							$username=$ur['fname'];
						else
							$username=$ur['email'];
					}
					$workeddate=$t['workdate'];
					$hours=$t['hours'];
					$total_hrs=$total_hrs+$hours;
					$table.="<tr>
							<td>".$username."</td>
							<td>".date("M d Y",strtotime($workeddate))."</td>
							<td align='right'>".number_format($hours,2)."</td>
							<td width='5%'><i class='fa fa-trash-o' style='cursor:pointer;' onclick='deleteTaskCmtHrs(".$tid.",this);'></i></td>
						</tr>";
				}
				$table.="<tr><td colspan='2' align='right'>&nbsp; Total Hours : </td><td align='right'>".number_format($total_hrs,2)."</td><td>&nbsp;</td></tr>";
				$table.="</table>
			</div>";
		
		echo $table;

	}


	if($_REQUEST['type']=="taskAssignTo")
	{
		$taskid=request_get('taskid');
		$pid=request_get('pid');
		$assignedto=request_get('assignedto');

		$sql="UPDATE `tasks` SET `assigned_to` = '$assignedto' WHERE `id` ='$taskid' and `pid`='$pid'";

		$res=mysql_query($sql) or die("ERROR : ".mysql_error());

	
	}
	if($_REQUEST['type']=="deleteTaskCmtHrs")
	{
		$id=request_get('rid');

		$sql=mysql_query("DELETE FROM `task_compeletion_hrs` WHERE id='$id'") or die("ERROR : ".mysql_error());
	}

	if($_REQUEST['type']=="insertTaskCompleteHrs")
	{
		$pid=request_get('projid');		
		$taskid=request_get('taskid');
		$userid=request_get('userid');
		$hours=request_get('complete_hrs');
		$workdate=date("Y-m-d",strtotime(request_get('workdate')));
		$complete_hrs_desc=request_get('complete_hrs_desc');

		$created=date('Y-m-d H:i:s');
		$cur_username=get_session('VW_USER_NAME');

		if(request_get('workdate')=="")
		{
			$workdate=date("Y-m-d",strtotime($created));
		}

		$sql="INSERT INTO `task_compeletion_hrs` ( `id` , `userid`, `pid` , `taskid` , `hours` , `workdate`, `description`, `created` , `createdby` , `modified` , `modifiedby` ) VALUES ( NULL , '$userid', '$pid', '$taskid', '$hours', '$workdate', '$complete_hrs_desc', '$created', '$cur_username', '$created', '$cur_username' )";

		$res=mysql_query($sql) or die("ERROR : ".mysql_error());
	}

	if($_REQUEST['type']=="taskcomments")
	{		
		$taskid=request_get('taskid');

		$task_res=mysql_query("SELECT * FROM tasks WHERE id='$taskid'") or die("ERROR : ".mysql_error());

		$r=mysql_fetch_array($task_res);

		$table="<table width='100%'>";
		$table.="<tr>";
			$table.="<td width='5%'> &nbsp; </td>";
			$table.="<td width='80%'>".$r['taskname']."</td>";
			$table.="<td width='10%'> &nbsp; </td>";
			$table.="<td width='5%'> &nbsp;</td>";
		$table.="<tr>";
		$table.="</table><br>";

		$task_cmt_res=mysql_query("SELECT * FROM task_comments WHERE taskid='$taskid'") or die("ERROR : ".mysql_error());
		$table.="<table width='100%' class='table' >";
		
		while($tcr=mysql_fetch_array($task_cmt_res))
		{
			$cmtid=$tcr['id'];

			$attlist="";
			$att=explode(",",$tcr['attachments']);

			foreach($att as $at)
			{
				if(!empty($at)){
	$attlist.="<div style='line-height:25px;' id='att_$id'><a href='download.php?type=download&filename=".$at."'><i class='glyphicon glyphicon-paperclip'></i>&nbsp;".basename($at)."</a>";
	 			}
			}
			
			$table.='<tr>
					<td rowspan="2" width="10%"><img src="assets/img/avatar3.png" width="80%" class="img-circle" alt="User Image"/></td>
					<td width="80%"><small>'.date('d-M-Y h:i a', strtotime($tcr['created'])).'</small></td>
					<td rowspan="2" width="10%">
						<span style="cursor:pointer;" title="Delete Comment" id="'.$cmtid.'" class="fa fa-trash-o deletecomment"></span>
					</td>
				</tr>
				<tr>
					<td style="border-top:none;">'.$tcr["comments"].' <br> '.$attlist.'</td>
				</tr>';
		}
		$table.="</table>";

		echo $table;

	}

	if($_REQUEST['type']=="addTaskComment")
	{
		$taskcomment=safe_sql_nq(request_get('taskcomment'));
		$taskid=request_get('taskid');
		$projid=request_get('projid');
		$attachments=request_get('attachments');

		$created=date('Y-m-d H:i:s');

		$sql="INSERT INTO `task_comments` (`id` , `pid` , `taskid` , `userid` , `comments` , `attachments` , `created` , `createdby` , `modified` , `modifiedby` ) VALUES (NULL , '$projid', '$taskid', '$cur_user_id', '$taskcomment', '$attachments', '$created', '$cur_username', '$created', '$cur_username')";

		$res=mysql_query($sql) or die("ERROR : ".mysql_error());

	}

	if($_REQUEST['type']=="delComment")
	{
		$id=request_get('sid');

		$sql=mysql_query("DELETE FROM `task_comments` WHERE id='$id'") or die("ERROR : ".mysql_error());
	}

	if($_REQUEST['type']=="completetask")
	{
		$taskid=request_get('taskid');

		$task_res=mysql_query("SELECT * FROM tasks WHERE id='$taskid'") or die("ERROR : ".mysql_error());

		$r=mysql_fetch_array($task_res);

		$table="<table width='100%'>";
		$table.="<tr>";
			$table.="<td width='5%'> &nbsp; </td>";
			$table.="<td width='80%'>".$r['taskname']."</td>";
			$table.="<td width='10%'> &nbsp; </td>";
			$table.="<td width='5%'> &nbsp;</td>";
		$table.="<tr>";
		$table.="</table><br>";

		echo $table;
	}

	if($_REQUEST['type']=="deletetask")
	{
		$taskid=request_get('taskid');

		$task_res=mysql_query("DELETE FROM tasks WHERE id='$taskid'") or die("ERROR : ".mysql_error());

	}
	

	if($_REQUEST['type']=="updTaskCompleteHrs")
	{
		//$complete_hrs=request_get('complete_hrs');
		$taskid=request_get('taskid');
		$projid=request_get('projid');

		$status=request_get('status');
		
		$sql="UPDATE `tasks` SET `status`='$status' WHERE `id` ='$taskid' ";

		$res=mysql_query($sql) or die("ERROR : ".mysql_error());

	}
	
	if($_REQUEST['type']=="sortorderTask")
	{
		$pid=request_get('pid');
		
		$orders =  explode(',',$_REQUEST['displayorder']);

		$i=0;
		foreach ($orders as $key=>$value) {
			$query = "UPDATE `tasks` SET displayorder = '$i' WHERE id = '$value' and pid='$pid'";
			echo $query."<br>";
			mysql_query($query) or die(mysql_error());

			$i++;
		}

	}

}

?>
