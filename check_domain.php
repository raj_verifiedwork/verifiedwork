<?php

include('dbcon.php');

if(isset($_POST["subdomainname"]))
{
	//check if its ajax request, exit script if its not
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
		die();
	}
		
	//trim and lowercase username
	$username =  strtolower(trim($_POST["subdomainname"])); 
	
	//sanitize username
	$username = filter_var($username, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH);
	
	//check username in db
	$results = mysql_query("SELECT id FROM subdomain_list WHERE subdomain='$username'");
	
	//return total count
	$username_exist = mysql_num_rows($results); //total records
	
	//if value is more than 0, username is not available
	if($username_exist) {
		die('<img src="assets/img/not-available.png" width="18px" />');
	}else{
		die('<img src="assets/img/available.png" width="18px" />');
	}
	
}

?>
