<style>
tr:hover {
  background-color: #ffffff;
}
</style>
	<div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="assets/img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p><?php echo get_session('VW_USER_NAME'); ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" id="leftmenu">
                        <li>
                            <a href="userhome.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-th"></i> <span>Configurations</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
			    <ul class="treeview-menu">
                                <li class="treeview"><a href="#"><i class="fa fa-angle-double-right"></i> Email Template <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="email_templates.php"><i class="fa fa-angle-double-right"></i> Email Template Listing </a></li>
		                        <li><a href="create_email_template.php"><i class="fa fa-angle-double-right"></i> Create Email Template </a></li>
	                        </ul>
				</li>
                                <!--<li><a href="create_email_template.php"><i class="fa fa-angle-double-right"></i> Flot</a></li>
                                <li><a href="../charts/inline.html"><i class="fa fa-angle-double-right"></i> Inline charts</a></li>-->
                            </ul>
                        </li>
			 <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Todoist Projects</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu" id="todoist_projlist">
				 <li>
					<!--<input type="hidden" id="projectaction" name="projectaction" value="add" >
					<a href="#" style="color: #F4543C; cursor:pointer;" id="addProject"><i class="fa fa-plus"></i> Add Project</a>-->
				</li>
			    </ul>
			</li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Projects</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu" id="projlist">
				<?php

				$mgres=mysql_query("SELECT * FROM user_projects WHERE userid='$cur_user_id' ORDER BY id ASC")or die(mysql_error());
				$mgr=mysql_fetch_array($mgres);
				
				$userprojectids=$mgr['project_ids'];
				$userprojectnames=$mgr['project_names'];

				$pids=explode(',',$userprojectids);
				$pnames=explode(',',$userprojectnames);

				for($i=0;$i<count($pids);$i++)
				{
					if($pids[$i]!="")
					{
						$pid=$pids[$i];

						$vpid="pv-".$pid;
						$epid="pe-".$pid;
						$apid="pa-".$pid;

						$pres=mysql_query("SELECT color,projectname FROM projects WHERE id='$pid' ORDER BY id ASC")or die(mysql_error());
						$pr=mysql_fetch_array($pres);
						$bgcolor=$pr['color'];

			echo '<li style="margin-left:10px;" id="li_'.$pids[$i].'">
				<table cellpadding="3px" width="100%" style="cursor:pointer;">
				<tr id="'.$vpid.'">
				<td valign="top" width="5%"><i class="fa fa-circle" id="projclr-'.$pid.'" val="'.$bgcolor.'" style="color:'.$bgcolor.';"></i></td>
				<td valign="top" width="90%" class="projnametd">
					<a class="projectedit" id="pid-'.$pids[$i].'"><span id="spid-'.$pids[$i].'">'.$pr['projectname'].'</span></a></td>
				<td valign="top" width="5%">
					<span style="cursor:pointer;" class="pull-right">
					<div class="btn-group">
					<button class="btn btn-default btn-flat dropdown-toggle" style="background-color: #F9F9F9; border: none;" data-toggle="dropdown" type="button">
					<span class="fa fa-ellipsis-h"></span>
					<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
					<li>
	<a style="margin-left:0px;" id="editproject" val="pid-'.$pids[$i].'" pname="'.$pr['projectname'].'" pid="'.$pids[$i].'" class="project_autogrow" href="#">Edit Project</a>
					</li>
					<li>
	<a style="margin-left:0px;" href="#" id="shareproject" onclick="javascript:return shareproject('.$pids[$i].');" data-toggle="modal" data-target="#compose-modal">Share Project</a>
					</li>
					<li style="margin-left:0px;" class="divider"></li>
					<li>
					<a style="margin-left:0px;" href="#" onclick="javascript:deleteprojfun('.$pids[$i].');">Delete Project</a>
					</li>
					</ul>
					</div></span>
				</td>
				</tr>
				</table></li>';

					}
				}

				?>				
                                <li>
					<input type="hidden" id="projectaction" name="projectaction" value="add" >
					<a href="#" style="color: #F4543C; cursor:pointer;" id="addProject"><i class="fa fa-plus"></i> Add Project</a>
				</li>
                            </ul>
                        </li>
			<li>
                            <a href="task_assigning.php">
                                <i class="fa fa-tasks"></i> <span>Task Assigning</span>
                            </a>
                        </li>
			<li>
                            <a href="task_complete_hrs.php">
                                <i class="fa fa-tasks"></i> <span>Task Completion Hrs</span>
                            </a>
                        </li>

			  <li class="treeview">
                            <a href="#">
                                <i class="fa fa-th"></i> <span> Verified work Report</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
			    <ul class="treeview-menu">
                                <li class="treeview"><a href="weekly_report.php"><i class="fa fa-angle-double-right"></i> User Wise <i class="fa fa-angle-left pull-right"></i></a>
		 <ul class="treeview-menu">
                <li><a href="weekly_report.php"><i class="fa fa-angle-double-right"></i> VW Weekly Report</a></li>
                <li><a href="custom_report.php"><i class="fa fa-angle-double-right"></i> VW Custom Report</a></li>
               </ul>
				</li>
                                <li><a href="project_report.php"><i class="fa fa-angle-double-right"></i> Project wise <i class="fa fa-angle-left pull-right"></i></a>
				</li>
                            </ul>
                        </li>
			 <li class="treeview">
                            <a href="#">
                                <i class="fa fa-th"></i> <span> Todoist Report</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
			    <ul class="treeview-menu">
                                <li class="treeview"><a href="weekly_report.php"><i class="fa fa-angle-double-right"></i> User Wise <i class="fa fa-angle-left pull-right"></i></a>
		 <ul class="treeview-menu">
                <li><a href="todo_wreport.php"><i class="fa fa-angle-double-right"></i> Todoist Weekly Report</a></li>
		   <li><a href="todoist_creport.php"><i class="fa fa-angle-double-right"></i> Todoist Custom Report</a></li>
               </ul>
				</li>
                                <li><a href="todoist_project_report.php"><i class="fa fa-angle-double-right"></i> Project wise <i class="fa fa-angle-left pull-right"></i></a>
				</li>
                            </ul>
                        </li>
                       <!-- <li class="treeview">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>UI Elements</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../UI/general.html"><i class="fa fa-angle-double-right"></i> General</a></li>
                                <li><a href="../UI/icons.html"><i class="fa fa-angle-double-right"></i> Icons</a></li>
                                <li><a href="../UI/buttons.html"><i class="fa fa-angle-double-right"></i> Buttons</a></li>
                                <li><a href="../UI/sliders.html"><i class="fa fa-angle-double-right"></i> Sliders</a></li>
                                <li><a href="../UI/timeline.html"><i class="fa fa-angle-double-right"></i> Timeline</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i> <span>Forms</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../forms/general.html"><i class="fa fa-angle-double-right"></i> General Elements</a></li>
                                <li><a href="../forms/advanced.html"><i class="fa fa-angle-double-right"></i> Advanced Elements</a></li>
                                <li><a href="../forms/editors.html"><i class="fa fa-angle-double-right"></i> Editors</a></li>                                
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-table"></i> <span>Tables</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../tables/simple.html"><i class="fa fa-angle-double-right"></i> Simple tables</a></li>
                                <li><a href="../tables/data.html"><i class="fa fa-angle-double-right"></i> Data tables</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="../calendar.html">
                                <i class="fa fa-calendar"></i> <span>Calendar</span>
                                <small class="badge pull-right bg-red">3</small>
                            </a>
                        </li>
                        <li>
                            <a href="../mailbox.html">
                                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                                <small class="badge pull-right bg-yellow">12</small>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Examples</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="invoice.html"><i class="fa fa-angle-double-right"></i> Invoice</a></li>
                                <li><a href="login.html"><i class="fa fa-angle-double-right"></i> Login</a></li>
                                <li><a href="register.html"><i class="fa fa-angle-double-right"></i> Register</a></li>
                                <li><a href="lockscreen.html"><i class="fa fa-angle-double-right"></i> Lockscreen</a></li>
                                <li><a href="404.html"><i class="fa fa-angle-double-right"></i> 404 Error</a></li>
                                <li><a href="500.html"><i class="fa fa-angle-double-right"></i> 500 Error</a></li>                                
                                <li ><a href="blank.html"><i class="fa fa-angle-double-right"></i> Blank Page</a></li>
                            </ul>
                        </li>-->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

