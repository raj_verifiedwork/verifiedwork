<?php
$pageno=2;
$pagename=" Email Templates ";

require_once('genfunctions.php');
require_once('vw_header.php');
require_once('vw_leftmenu.php');

?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
         <?php echo $pagename; ?>
       <!-- <small>it all starts here</small>-->
    </h1>
   <?php
	require_once('breadcrumb.php');
    ?>
</section>

<!-- Main content -->
<section class="content">
<?php
$sql="SELECT * FROM `email_templates`";
$res=mysql_query($sql)or die("ERROR : ".mysql_error());

?>

<div class="box box-primary">
<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Name</th>
                <th>Type</th>
                <th>Subject</th>
                <th>Content</th>
		<th>Action</th>
            </tr>
        </thead>
        <tbody>
	<?php 
	$i=1;
	while($r=mysql_fetch_array($res))
	{
	?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $r['emailtype']; ?></td>
                <td><?php if($r['template_type']==0) echo "HTML"; else echo "TEXT"; ?></td>
                <td><?php echo $r['subject']; ?></td>
                <td><?php echo $r['content']; ?></td>
		<td><?php 
			echo "<a href='create_email_template.php?job=edit&id=".$r['id']."' title='Edit'><i class='glyphicon glyphicon-edit'></i></a> ";
			
			if($r['is_system_template']!=1)	
			  	echo  "&nbsp; <a href='#' title='Delete'><i class='glyphicon glyphicon-trash'></i></a> ";
		    ?>
		</td>
            </tr>
	<?php
		$i++;
	}
	?>
        </tbody>
       <!-- <tfoot>
            <tr>
                <th>Rendering engine</th>
                <th>Browser</th>
                <th>Platform(s)</th>
                <th>Engine version</th>
                <th>CSS grade</th>
            </tr>
        </tfoot>-->
    </table>
</div><!-- /.box-body -->
</div><!-- /.box -->


</section><!-- /.content -->
</aside><!-- /.right-side -->

<?php
	require_once('vw_footer.php');
?>
<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
    });
</script>
