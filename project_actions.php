<?php
session_start();
require_once('genfunctions.php');

if(isset($_REQUEST['type']))
{
	$cur_user_id=get_session('VW_USER_ID');
	$cur_domain_id=get_session('VW_DOMAIN_ID');
	$cur_username=get_session('VW_USER_NAME');
	$cur_email=get_session('VW_USER_EMAIL');

	$created=date('Y-m-d H:i:s');

	$userimgpath="userphotos/";

	if($_REQUEST['type']=="addProject")
	{
		$projectname=safe_sql_nq(request_get('projectname'));

		$color=safe_sql_nq(request_get('projcolor'));

		$created=date('Y-m-d H:i:s');

		$joinedpersons=$cur_user_id;

		if($color=="")
		{
			$color="rgb(221, 221, 221)";
		}

		$sql="INSERT INTO `projects` ( `id` , `domainid` , `userid` , `projectname` , `joinedpersons` , `color`, `status` , `createdby` , `created` , `modifiedby` , `modified` ) VALUES(NULL , '$cur_domain_id', '$cur_user_id', '$projectname', '$joinedpersons', '$color', '0', '$cur_username', '$created', '$cur_username', '$created')";

		$res=mysql_query($sql) or die("ERROR : ".mysql_error());

		$new_pid=mysql_insert_id();

		echo $new_pid;

		$s_up=mysql_query("SELECT * FROM user_projects where userid='$cur_user_id'") or die("ERROR : ".mysql_error());

		if(mysql_num_rows($s_up)>0)
		{
			$s_up_res=mysql_fetch_array($s_up);
	
			$upid=$s_up_res['id'];
			$oldprojectids=$s_up_res['project_ids'];
			$oldprojectnames=$s_up_res['project_names'];
			
			$projstr=explode(',',$oldprojectids);
			array_push($projstr,$new_pid);
			$newprojectids=implode(',',$projstr);

			$projstr1=explode(',',$oldprojectnames);
			array_push($projstr1,$projectname);
			$newprojectnames=implode(',',$projstr1);

		$up_sql="UPDATE `user_projects` SET `userid` = '$cur_user_id', `project_ids` = '$newprojectids', `project_names`='$newprojectnames', `modified` = '$created', `modifiedby` = '$cur_username' WHERE `id`='$upid'";

			$res=mysql_query($up_sql) or die("ERROR : ".mysql_error());

		}else{

			$up_sql="INSERT INTO `user_projects` ( `id` , `userid` , `project_ids` , `project_names`, `created` , `createdby` , `modified` , `modifiedby` ) VALUES ( NULL , '$cur_user_id', '$new_pid', '$projectname', '$created', '$cur_username', '$created', '$cur_username')";
			$res=mysql_query($up_sql) or die("ERROR : ".mysql_error());

		}

		

	}

	
	if($_REQUEST['type']=="updProject")
	{
		$pid=request_get('pid');
		$projcolor=safe_sql_nq(request_get('projcolor'));
		$projname=safe_sql_nq(request_get('projname'));
		
		$created=date('Y-m-d H:i:s');

		$upd_sql="UPDATE `projects` SET `projectname` = '$projname', `color` = '$projcolor', `modifiedby` = '$cur_username', `modified` = '$created' WHERE `id` ='$pid'";

		echo $upd_sql;
		$res=mysql_query($upd_sql) or die("ERROR : ".mysql_error());
	}

	if($_REQUEST['type']=="delProject")
	{
		$pid=request_get('pid');
		
		$s_up=mysql_query("SELECT * FROM user_projects where userid='$cur_user_id'") or die("ERROR : ".mysql_error());

		if(mysql_num_rows($s_up)>0)
		{
			$s_up_res=mysql_fetch_array($s_up);
			$upid=$s_up_res['id'];
			$oldprojectids=$s_up_res['project_ids'];
			$oldprojectnames=$s_up_res['project_names'];
			
			$projstr=explode(',',$oldprojectids);
			$projstr1=explode(',',$oldprojectnames);
		
			foreach (array_keys($projstr, $pid) as $key) {
			    unset($projstr[$key]);
			    unset($projstr1[$key]);
			}

			$newprojectids=implode(',',$projstr);
			$newprojectnames=implode(',',$projstr1);
			
		$up_sql="UPDATE `user_projects` SET `project_ids` = '$newprojectids', `project_names`='$newprojectnames', `modified` = '$created', `modifiedby` = '$cur_username' WHERE `id`='$upid'";
			$res=mysql_query($up_sql) or die("ERROR : ".mysql_error());
		}
	$pid=request_get('pid');
		$s_up=mysql_query("SELECT * FROM projects where id='$pid'") or die("ERROR : ".mysql_error());
			
		if(mysql_num_rows($s_up)>0)
		{
			$s_up_res=mysql_fetch_array($s_up);
			$usid=$s_up_res['userid'];	
			$upid=$s_up_res['id'];
			$oldprojectids=$s_up_res['joinedpersons'];
			$projstr=explode(',',$oldprojectids);

			foreach (array_keys($projstr, $usid) as $key) {
			    unset($projstr[$key]);
			}

			$newprojectids=implode(',',$projstr);
		$up_sql="UPDATE `projects` SET `joinedpersons` = '$newprojectids' WHERE `id`='$upid'";
			$res=mysql_query($up_sql) or die("ERROR : ".mysql_error());
		
		}
	$s_up=mysql_query("SELECT * FROM projects where userid='$cur_user_id' and id='$pid'") or die("ERROR : ".mysql_error());

		if(mysql_num_rows($s_up)>0)
		{
		$sql=mysql_query("DELETE FROM `projects` WHERE id='$pid'") or die("ERROR : ".mysql_error());
		}

		

	}

	if($_REQUEST['type']=="inviteToProject")
	{
		$inviteuserid=request_get('inviteuserid');
		$selectedpid=request_get('selectedpid');

		$tusr_res=mysql_query("select email from users where id='$inviteuserid'") or die("ERR : ".mysql_error());
		$tusr_r=mysql_fetch_array($tusr_res);

		$from_username="";
		if($cur_username!="")
			$from_username=$cur_username;
		else
			$from_username=$cur_email;

		$to_username=$tusr_r["email"];

		$pusr_res=mysql_query("select projectname from projects where id='$selectedpid'") or die("ERR : ".mysql_error());
		$pusr_r=mysql_fetch_array($pusr_res);

		$selectedprojname=$pusr_r['projectname'];

		$created=date('Y-m-d H:i:s');

		$sql="INSERT INTO `invite_project` ( `id` , `subdomainid` , `pid` , `projectname`, `fromuserid` , `from_username`, `touserid` , `to_username`, `status` , `created` , `createdby` , `modified` , `modifiedby` ) VALUES ( NULL , '$cur_domain_id', '$selectedpid', '$selectedprojname', '$cur_user_id', '$from_username', '$inviteuserid', '$to_username', '0', '$created', '$cur_username', '$created', '$cur_username')";

		$res=mysql_query($sql) or die("ERROR : ".mysql_error());

	}

	
	if($_REQUEST['type']=="shareprojectinfo")
	{
		$pid=request_get('pid');

		$sql=mysql_query("select * from projects where id='$pid'") or die("ERROR : ".mysql_error());

		$r=mysql_fetch_array($sql);

		$joinedperson=array_filter(explode(",",$r['joinedpersons']));
		
		$table="<table id='invitedpeoplelist' class='table' width='100%'><tbody>";
		for($i=0;$i<count($joinedperson);$i++)
		{
			$usrid=$joinedperson[$i];

			if($usrid!=" ")
			{
				$ures=mysql_query("select id,fname,email from users where id='$usrid'")or die("ERROR : ".mysql_error());
				$ur=mysql_fetch_array($ures);

				$username="";
				if($usrid==$cur_user_id)
					$username="me";
				else{
					$username=$ur['fname'];
				}

				$table.='<tr>
				<td rowspan="2" width="10%"><img src="assets/img/avatar3.png" width="80%" class="img-circle" alt="User Image"/></td>
						<td width="80%">'.$username.'</td>
						<td rowspan="2" width="10%"> &nbsp; </td>
					</tr>
					<tr>
						<td style="border-top:none;">'.$ur["email"].'</td>
					</tr>';
			}

		}
		$table.="</tbody></table>";

		echo $table;

	}
	
	if($_REQUEST['type']=="responseToProject")
	{
		$invid=request_get('invid');
		$status=request_get('status');

		if($status==1)
		{
		$sql=mysql_query("SELECT * FROM invite_project WHERE id='$invid'") or die("ERROR : ".mysql_error());

		$r=mysql_fetch_array($sql);
		
		$new_pid=$r['pid'];
		$projectname=$r['projectname'];

		$created=date('Y-m-d H:i:s');

		$s_up=mysql_query("SELECT * FROM user_projects where userid='$cur_user_id'") or die("ERROR : ".mysql_error());

		if(mysql_num_rows($s_up)>0)
		{
			$s_up_res=mysql_fetch_array($s_up);
	
			$upid=$s_up_res['id'];
			$oldprojectids=$s_up_res['project_ids'];
			$oldprojectnames=$s_up_res['project_names'];
			
			$projstr=explode(',',$oldprojectids);
			array_push($projstr,$new_pid);
			$newprojectids=implode(',',$projstr);

			$projstr1=explode(',',$oldprojectnames);
			array_push($projstr1,$projectname);
			$newprojectnames=implode(',',$projstr1);

		$up_sql="UPDATE `user_projects` SET `project_ids` = '$newprojectids', `project_names`='$newprojectnames', `modified` = '$created', `modifiedby` = '$cur_username' WHERE `id`='$upid'";

			$res=mysql_query($up_sql) or die("ERROR : ".mysql_error());

		}else{

			$up_sql="INSERT INTO `user_projects` ( `id` , `userid` , `project_ids` , `project_names`, `created` , `createdby` , `modified` , `modifiedby` ) VALUES ( NULL , '$cur_user_id', '$new_pid', '$projectname', '$created', '$cur_username', '$created', '$cur_username')";
			$res=mysql_query($up_sql) or die("ERROR : ".mysql_error());

		}

		$p_up=mysql_query("select id,joinedpersons from projects where id='$new_pid'") or die("ERROR : ".mysql_error());

		if(mysql_num_rows($p_up)>0)
		{
			$p_up_res=mysql_fetch_array($p_up);

			$upid=$p_up_res['id'];
			$olduserids=$p_up_res['joinedpersons'];

			$userstr=explode(',',$olduserids);
			array_push($userstr,$cur_user_id);
			$newuserids=implode(',',$userstr);
			
			$upd_psql="update projects set joinedpersons='$newuserids' where id='$new_pid'";
			$res=mysql_query($upd_psql) or die("ERROR : ".mysql_error());
		}

			$upd_sql="update invite_project set status='1' where id='$invid'";
			$res=mysql_query($upd_sql) or die("ERROR : ".mysql_error());
		}
		else if($status==2){
			$upd_sql="update invite_project set status='2' where id='$invid'";
			$res=mysql_query($upd_sql) or die("ERROR : ".mysql_error());
		}
		
	}

	if($_REQUEST['type']=="getTasksByProjectId")
	{
		$pid=request_get('pid');

		$task_res=mysql_query("SELECT * FROM tasks WHERE pid='$pid'") or die("ERROR : ".mysql_error());

		$tasklist="";

		while($r=mysql_fetch_array($task_res))
		{
		 	$tasklist.="<option value='".$r['id']."'>".$r['taskname']."</div>";
		}

		echo $tasklist;
	}

	if($_REQUEST['type']=="getProjectTasks")
	{
		$pid=request_get('pid');

		$sql=mysql_query("SELECT * FROM `projects` WHERE id='$pid'") or die("ERROR : ".mysql_error());
		
		$res=mysql_fetch_array($sql);

		$projectname=$res['projectname'];
		$pid=$res['id'];

		$joinedpersons=$res['joinedpersons'];

		$projusrs=array_filter(explode(",",$joinedpersons));
		

		$taskList='<div class="box box-primary">
                                <div class="box-header">
                                    <i class="ion ion-clipboard"></i>
                                    <h3 class="box-title">'.$projectname.'</h3>
                                    <div class="box-tools pull-right">
                                        <!--<ul class="pagination pagination-sm inline">
                                            <li><a href="#">&laquo;</a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">&raquo;</a></li>
                                        </ul>-->
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body">
				<ul class="todo-list" id="project_task_list">';
		
		$task_res=mysql_query("SELECT * FROM tasks WHERE pid='$pid' and status=0 ORDER BY displayorder ASC") or die("ERROR : ".mysql_error());

		$i=0;
		while($r=mysql_fetch_array($task_res))
		{
			$taskname=$r['taskname'];
			$est_hrs=$r['est_hrs'];
			$due_date=$r['duedate'];

			$taskid=$r["id"];

			$assigned_to=$r['assigned_to'];
			$imgpath="";
			
			$sql=mysql_query("select id,email,photo,login_with from users where id='$assigned_to' and status='0'") or die("ERROR : ".mysql_error());

			$ur=mysql_fetch_array($sql);
			if($ur['photo']=="")
				$imgpath="assets/img/noavatar.png";
			else{
				if($ur['login_with']==1)
					$imgpath=$ur['photo'];
				else 
					$imgpath=$userimgpath.$ur['photo'];
			}

			$npid=$r['pid'];

			$sql_cmpte_hrs=mysql_query("select * from task_compeletion_hrs where `taskid`='$taskid'") or die("ERROR : ".mysql_error());
			
			$tot_cmp_hrs="0";

			while($t=mysql_fetch_array($sql_cmpte_hrs))
			{
				$hours=$t['hours'];
				$tot_cmp_hrs=$tot_cmp_hrs+$hours;
			}

			$esthrs="";
			$hrs="";
			$hrsTitle="";

			if($r['est_hrs']=="")
			{
				$esthrs="";

				if($tot_cmp_hrs>0)
				{
					$hrs=$tot_cmp_hrs;
					$hrsTitle="Total hours : ".$hrs;
					$esthrs='<small class="label label-primary" title="Hrs"><i class="fa fa-clock-o"></i> <span>'.$hrs.'</span> hrs </small>';
				}

			}else if($r['est_hrs']=="0"){
				$esthrs="";
				
				if($tot_cmp_hrs>0)
				{
					$hrs=$tot_cmp_hrs;
					$hrsTitle="Total hours : ".$hrs;
					$esthrs='<small class="label label-primary" title="Hrs"><i class="fa fa-clock-o"></i> <span>'.$hrs.'</span> hrs </small>';
				}
			}else{
			
				if($tot_cmp_hrs>0)
				{
					$hrs=$tot_cmp_hrs-$r["est_hrs"];
					$hrsTitle="Total hrs (".$hrs.") - Est. hrs (".$r['est_hrs'].") ";
				}else{
					$hrs=$r["est_hrs"];
					$hrsTitle="Est. Hrs : ".$r['est_hrs'];
				}
				$esthrs='<small class="label label-primary" title="'.$hrsTitle.'"><i class="fa fa-clock-o"></i> <span>'.$hrs.'</span> hrs </small>';

			}

			$taskvid="tv-".$r["id"];
			$taskeid="te-".$r["id"];
			$taskaid="ta-".$r["id"];
			
			$taskcmtcntid="tcmt-".$taskid;
	
			$due_date_show="";
			if($due_date=="0000-00-00 00:00:00")
			{
				$due_date=date('d-m-Y',strtotime($created));
				$due_date_show="";
			}
			else{
				$due_date=date('d-m-Y',strtotime($due_date));
				$due_date_show='<small class="label label-success" title="Due Date">'.date('M d',strtotime($due_date)).'</small>';
			}

			$iscompleted="";
			$ischked="";
			if($r["status"]==0)
			{
				$iscompleted="";
				$ischked="";
			
			
			$task_res_cnt=mysql_query("SELECT count(*) as taskcmts FROM task_comments WHERE taskid='$taskid'") or die("ERROR : ".mysql_error());
			$r_cnt=mysql_fetch_array($task_res_cnt);

			if($r_cnt['taskcmts']>0)			
				$cmt_count=$r_cnt['taskcmts'];
			else
				$cmt_count="";

                        $taskList.='<li '.$iscompleted.' id="'.$taskid.'" seqid="'.$i.'" class="taskli1" > 
					<div id="taskdiv-'.$taskid.'">
						<div class="viewtask" id="'.$taskvid.'">   

					<table border="0" width="100%" class="table table-condensed" style="display:inline;">
						<tr>
							<td style="border-top:none;">
								<span class="handle">
									<i class="fa fa-th"></i>
								</span>
							</td>
							<td style="border-top:none;" valign="top">
				<input type="checkbox" id="cbk-'.$taskid.'" value="" '.$ischked.' name="completetask" onclick="javascript:return completetask('.$taskid.','.$npid.');" />
							</td>
							<td width="75%" style="border-top:none;" valign="top">

			<span class="text" > <span class="edittaskshow" id="'.$taskaid.'"  >'.$taskname.' &nbsp;&nbsp; </span> 
			<span style="opacity:0.5; cursor:pointer;" title="Task Comments"  onclick="javascript:return showtaskcomments('.$taskid.','.$npid.');" data-toggle="modal" data-target="#comments-modal"><i class="glyphicon glyphicon-comment"></i>&nbsp;
			<span id="'.$taskcmtcntid.'"> '.$cmt_count.' </span>&nbsp;</span>&nbsp;
			<span style="opacity:0.5; cursor:pointer;" title="Completed Hours" onclick="javascript:return showtaskhrs('.$taskid.','.$npid.');" data-toggle="modal" data-target="#updatehrs-modal"><i class="glyphicon glyphicon-dashboard"></i>&nbsp;</span> &nbsp; 
			</span> 

							</td>
							<td width="10%" style="border-top:none;" valign="top"><span>'.$esthrs.'</span>&nbsp;&nbsp;<span>'.$due_date_show.'</span></td>
							<td width="5%" style="border-top:none;" valign="top">
				<img class="img-circle ap" assignedto="'.$assigned_to.'" pid="'.$npid.'" taskid="'.$taskid.'" alt="User Image" src="'.$imgpath.'" width="60%">
							</td>
							<td width="5%" style="border-top:none;" valign="top">
								<div class="tools">
									<i class="fa fa-trash-o" onclick="deleteTask('.$taskid.',this);"></i>
								</div>
							</td>
						</tr>
					</table>

						</div>
						<div class="edittask" id="'.$taskeid.'">
							<div class="row">
								<div class="col-xs-8">
				<textarea class="form-control" type="text" name="taskname-'.$taskid.'" id="taskname-'.$taskid.'" placeholder="Task name" required >'.$taskname.'</textarea>
								</div>
								<div class="col-xs-2">
				<input class="form-control" type="text" name="esthrs-'.$taskid.'" id="esthrs-'.$taskid.'" value="'.$est_hrs.'" placeholder="Est.Hrs">
								</div>
								<div class="col-xs-2">
				<input class="form-control duedate" type="text" name="duedate-'.$taskid.'" id="duedate-'.$taskid.'" value="'.$due_date.'" placeholder="Due Date">
								</div>
							</div><br>
							<div class="row">
								<div class="col-xs-12">
						<button type="button" id="updTask-'.$taskid.'" class="btn btn-danger pull-left editupd"> Save </button> &nbsp; &nbsp;
							<button type="button" id="cancelEditTask-'.$taskid.'" class="btn btn-default editcancel" > Cancel </button>
								</div>
							</div>
						</div>
					</div>
                                      </li>';
			$i++;
			}
			/*else
			{
				$iscompleted='class="done"';
				$ischked="checked";
			}*/
		}

                $imgpath="";

		$string="";

		for($k=0;$k<count($projusrs);$k++)
		{
			if($projusrs[$k]!=" ")
			{
				$usrid=$projusrs[$k];
				$sql=mysql_query("select id,email,photo,login_with from users where id='$usrid' and status='0'") or die("ERROR : ".mysql_error());

				$r=mysql_fetch_array($sql);
			
				$uid=$r['id'];
				$uemail=$r['email'];

				if($r['photo']=="")
					$imgpath="assets/img/noavatar.png";
				else{
					if($r['login_with']==1)
						$imgpath=$r['photo'];
					else 
						$imgpath=$userimgpath.$r['photo'];
				}
		$a_uemail=split("@",$uemail);
			$string.="<tr><td>
			<span class='asgninfo'><img assignedto='".$uid."' pid='".$npid."' taskid='0' class='img-circle apn' src='".$imgpath."' width='20%'>&nbsp; ".$a_uemail[0]."</span>
				</td></tr>";
			
			}
		}
               
                   $taskList.='</ul>  

				<div id="assignedtopersons" class="custompoptask" style="padding:8px; width: 250px;">
				<span style="padding-bottom:5px;">&nbsp;<b>Assign a person</b><span id="clsap" class="glyphicon glyphicon-remove-circle pull-right"></span>&nbsp;</span>
				<div style="height:250px; overflow-y:auto;"> 
					<table class="table table-bordered hoverTable">
					<tr><td>
			<span class="asgninfo"><img assignedto="0" pid="'.$npid.'" taskid="0" class="img-circle apn" src="assets/img/noavatar.png" width="13%">&nbsp; Unassigned</span>
					</td></tr>'.$string.'</table>
					<input type="hidden" name="cur_taskid" id="cur_taskid" />
					<input type="hidden" name="cur_pid" id="cur_pid" />
				</div>
				</div>


				</div><!-- /.box-body -->

				
                                <div class="box-footer clearfix no-border">
				
				<div id="addTaskContent"> 
					<div class="row">
						<div class="col-xs-8">
							<input class="form-control" type="text" name="taskname" id="taskname" placeholder="Task name" required >
						</div>
						<div class="col-xs-2">
							<input class="form-control esthrs" type="text" maxlength="3" name="esthrs" id="esthrs" placeholder="Est.Hrs">
						</div>
						<div class="col-xs-2">
							<input class="form-control duedate" type="text" name="duedate" id="duedate" placeholder="Due Date">
						</div>
					</div><br>
					<div class="row">
						<div class="col-xs-12">
							<button type="button" id="saveTask" class="btn btn-danger pull-left"><i class="fa fa-plus"></i> Add Task </button> &nbsp; &nbsp;
							<button type="button" id="cancelTask" class="btn btn-default" > Cancel </button>
						</div>
					</div>
				</div>
				<br><br><input type="hidden" name="taskaction" id="taskaction" value="add" />
					<input type="hidden" name="projectid" id="projectid" value="'.$pid.'" />
                                    <a id="addTask" class="pull-left" style="color: #F4543C; cursor:pointer; "><i class="fa fa-plus"></i> Add Task</a>';

$taskList.='<div id="showhide"  class="pull-right" style="display:none">
<a id="showTask" href="#"  onclick="javascript:showcomp_task();" style="cursor:pointer; ">Show</a>
 / <a id="hideTask" href="#"  onclick="javascript:hidecomp_task();" style="cursor:pointer;">Hide</a> 
Completed Task</div>';

                               $taskList.='</div>
                            </div><!-- /.box -->';




//update

$taskList.='<div id="task_comment" ><div class="box box-primary">
<div class="box-header">
            <i class="ion ion-clipboard"></i>
            <h3 class="box-title">Completed Task</h3>
     
        </div><!-- /.box-header -->';
$task_res=mysql_query("SELECT * FROM tasks WHERE pid='$pid' and status=1 ORDER BY displayorder ASC") or die("ERROR : ".mysql_error());

$taskList.='<div class="box-body">
	
	<ul class="todo-list taskui" id="project_task_list" >';

		$i=0;
		while($r=mysql_fetch_array($task_res))
		{
			$taskname=$r['taskname'];
			$est_hrs=$r['est_hrs'];
			$due_date=$r['duedate'];

			$taskid=$r["id"];

			$assigned_to=$r['assigned_to'];
			$imgpath="";
			$sql=mysql_query("select id,email,photo,login_with from users where id='$assigned_to' and status='0'") or die("ERROR : ".mysql_error());

			$ur=mysql_fetch_array($sql);
			if($ur['photo']=="")
				$imgpath="assets/img/noavatar.png";
			else{
				if($ur['login_with']==1)
					$imgpath=$ur['photo'];
				else 
					$imgpath=$userimgpath.$ur['photo'];
			}
			$taskvid="tv-".$r["id"];
			$taskeid="te-".$r["id"];
			$taskaid="ta-".$r["id"];
			/*$npid=$r['pid'];
			
			
			 */
			$iscompleted="";
			$ischked="";
			if($r["status"]==0)
			{
				$iscompleted="";
				$ischked="";
			}
			else
			{
				$iscompleted='class="done"';
				$ischked="checked";
			}
			
			$task_res_cnt=mysql_query("SELECT count(*) as taskcmts FROM task_comments WHERE taskid='$taskid'") or die("ERROR : ".mysql_error());
			$r_cnt=mysql_fetch_array($task_res_cnt);

			if($r_cnt['taskcmts']>0)			
				$cmt_count=$r_cnt['taskcmts'];
			else
				$cmt_count="";

	  $taskList.='<li '.$iscompleted.' id="'.$taskid.'" seqid="'.$i.'"  class="taskli" > 
				
					<div id="taskdiv-'.$taskid.'"  >
						<div class="viewtask" id="'.$taskvid.'">   

					<table border="0" width="100%" class="table table-condensed" style="display:inline;">
						<tr>
							<td style="border-top:none;">
								<span class="handle">
									<i class="fa fa-th"></i>
								</span>
							</td>
							<td style="border-top:none;" valign="top">
				<input type="checkbox" id="cbk-'.$taskid.'" value="" '.$ischked.' name="completetask" onclick="javascript:return completetask('.$taskid.','.$npid.');" />
							</td>
							<td width="75%" style="border-top:none;" valign="top">

			<span class="text edittaskshow" id="'.$taskaid.'" >'.$taskname.' &nbsp;&nbsp; 
			<span style="opacity:0.5; cursor:pointer;" title="Task Comments"  onclick="javascript:return showtaskcomments('.$taskid.','.$npid.');" data-toggle="modal" data-target="#comments-modal"><i class="glyphicon glyphicon-comment"></i>&nbsp;
			<span id="'.$taskcmtcntid.'"> '.$cmt_count.' </span>&nbsp;</span>&nbsp;
			<span style="opacity:0.5; cursor:pointer;" title="Completed Hours" onclick="javascript:return showtaskhrs('.$taskid.','.$npid.');" data-toggle="modal" data-target="#updatehrs-modal"><i class="glyphicon glyphicon-dashboard"></i>&nbsp;</span> &nbsp; 
			</span> 

							</td>
							<td width="10%" style="border-top:none;" valign="top"><span>'.$esthrs.'</span>&nbsp;&nbsp;<span>'.$due_date_show.'</span></td>
							<td width="5%" style="border-top:none;" valign="top">
				<img class="img-circle ap" assignedto="'.$assigned_to.'" pid="'.$npid.'" taskid="'.$taskid.'" alt="User Image" src="'.$imgpath.'" width="60%">
							</td>
							<td width="5%" style="border-top:none;" valign="top">
								<div class="tools">
									<i class="fa fa-trash-o" onclick="deleteTask('.$taskid.',this);"></i>
								</div>
							</td>
						</tr>
					</table>
					
						</div></div>
			</li>';
			$i++;
		}
	

 $taskList.='</ul></div>
</div>
</div>';
		echo $taskList;	
		
	}


}

?>
