<?php
session_start();
$pageno=2;
$pagename=" Todoist Weekly Report ";
require_once('genfunctions.php');
require_once('vw_header.php');
require_once('vw_leftmenu.php');
$vw_userid = get_session('VW_USER_ID');
	$sdate = date("Y-m-d",strtotime('monday this week'));
	$sd_date = $sdate;		
	$day_date = array($sd_date);
	for($i=1; $i<7; $i++)
	{
	$date1 = $sd_date;$date11 = str_replace('-', '/', $date1);
	$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
	array_push($day_date,$tomorrow );
	$sd_date = $tomorrow;
	}
$sw=date('m/d/Y',strtotime($sdate)); 
$ew=date('m/d/Y',strtotime($sd_date)); 
?>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css">



<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
         <?php echo $pagename; ?>
       <!-- <small>it all starts here</small>-->
    </h1>
   <?php
	require_once('breadcrumb.php');
    ?>
</section>

    <br /><br />
  				Week of: &nbsp;&nbsp;<a href="#" id="prev"><img src="img/back.png" height="15px" width="15px"></a><input type="text" class="week-picker week_pick val" id="sel_week"><a href="#" id="next"><img src="img/forward.png" height="15px" width="15px"></a>
	  <br /><br />

<!-- Main content -->
<section class="content">
<?php 
$sql="select * from projects where userid='$vw_userid'";
$res=mysql_query($sql)or die("ERROR : ".mysql_error());
?>
<div class="ajax_table">

<div id="loader" style="height:50px; width:50px; position:absolute; top:50%; left:50%;"><img src="img/loader.gif"></div>
</div>

</section><!-- /.content -->
</aside><!-- /.right-side -->

<?php
	//require_once('vw_footer.php');
?>
<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
    });
</script>
<input type="text" name="proj" class="proj">
<input type="hidden" name="sdate" id="sdate" value="<?php echo $sw; ?>">
<input type="hidden" name="edate" id="edate" value="<?php echo $ew; ?>">
 
        <script type="text/javascript">
	
// Integration with Todoist
	var todoist_token="<?php echo get_session('todoist_token'); ?>";

	if(!todoist_token)
	{
		var un="<?php echo get_session('VW_USER_EMAIL'); ?>";
		var pwd="<?php echo get_session('VW_USER_PWD'); ?>";

		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'http://todoist.com/API/login?email='+un+'&password='+pwd+'&callback=callback_userinfo';

		document.getElementsByTagName('head')[0].appendChild(script);

		function callback_userinfo(result){

			//console.log(result);

			var apitoken=result.api_token;
			var avatar_big=result.avatar_big;
			var avatar_medium=result.avatar_medium;
			var avatar_small=result.avatar_small;
			var todo_email=result.email;
			var full_name=result.full_name;
			var todo_userid=result.id;
			var inbox_project=result.inbox_project;
			var seq_no=result.seq_no;
			var timezone=result.timezone;
			var token=result.token;

			var data={
				type:"todoist_userinfos",
				apitoken:apitoken,
				avatar_big:avatar_big,
				avatar_medium:avatar_medium,
				avatar_small:avatar_small,
				todo_email:todo_email,
				full_name:full_name,
				todo_userid:todo_userid,
				inbox_project:inbox_project,
				seq_no:seq_no,
				token:token
			}
			$.ajax({
				type:"POST",
				url:"user_actions.php",
				data:data,
				success:function(output){
	
				}

			});

		}

	} // end of todoist_token -- if 

	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'http://todoist.com/API/getProjects?token='+todoist_token+'&callback=callback_projects';

	document.getElementsByTagName('head')[0].appendChild(script);

	function callback_projects(result){	

		//console.log(result);

		for(var i=0;i<result.length;i++)
		{
			var npid=result[i].id;
			var projname=result[i].name;
			var projcolor=result[i].color;

		 	$('ul#todoist_projlist').append('<li id="li_'+npid+'" style="margin-left:10px;"><table cellpadding="6px" width="100%"><tr><td valign="top" width="5%"><i class="fa fa-circle" style="color:'+projcolor+'"></i></td><td valign="top" width="90%"><a href="#" onclick="get_project_tasks('+npid+')" id="'+npid+'">'+projname+'</a></td><td valign="top" width="5%">&nbsp;</td></tr></table></li>');
		}
		var myJsonString = JSON.stringify(result);//alert(myJsonString);
		var cd='';
		$(".proj").val(myJsonString);
		$.ajax({
			    type: "POST",		
			    url: "get_todo_wreport.php",
			    data:{ name: myJsonString, type:'project', custom_date:cd},
			    success: function(result) { //alert(result);
		$("#loader").css('display','none');
		$('.ajax_table').html(result);
				}
		});

	}

	function get_project_tasks(pid)
	{

		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'http://todoist.com/API/getUncompletedItems?project_id='+pid+'&token='+todoist_token+'&callback=callback_project_tasks';

		document.getElementsByTagName('head')[0].appendChild(script);

	}

	function callback_project_tasks(result)
	{
		//console.log(result);

		var proj_tasks='<div class="box box-primary"><div class="box-header"><i class="ion ion-clipboard"></i><h3 class="box-title">Tasks</h3><div class="box-tools pull-right"></div></div><!-- /.box-header --><div class="box-body"><ul class="todo-list" id="todoist_project_task_list">';

		for(var i=0;i<result.length;i++)
		{
			var taskid=result[i].id;
			var taskcontent=result[i].content;
			var date_string=result[i].date_string;
			var due_date=result[i].due_date;
			var projectid=result[i].project_id;
			var assigned_to=result[i].responsible_uid;
			var avartar=result[i].responsible_uid;
			var pid=result[i].project_id;
			
			var task_item='<li><div class="viewtask" id="'+taskid+'"><table border="0" width="100%" class="table table-condensed"><tr><td width="5%" style="border-top:none;"><span class="handle"><i class="fa fa-th"></i></span></td><td width="75%" style="border-top:none;" valign="top"><span class="text edittaskshow" id="'+taskid+'" >'+taskcontent+' &nbsp;&nbsp;</span>&nbsp;<span style="opacity:0.5; cursor:pointer;" title="Completed Hours" onclick="javascript:return showtaskhrs_todoist('+taskid+','+projectid+');" data-toggle="modal" data-target="#todoist_updatehrs-modal"><i class="glyphicon glyphicon-dashboard"></i>&nbsp;</span> &nbsp;</span></td><td width="10%" style="border-top:none;" valign="top"><span>'+date_string+'</span>&nbsp;&nbsp;</td><td width="5%" style="border-top:none;" valign="top"> &nbsp; </td><td width="5%" style="border-top:none;" valign="top"> &nbsp; </td></tr></table></div></li>';

			/*<img class="img-circle ap" assignedto="'+assigned_to+'" pid="'+projectid+'" taskid="'+taskid+'" alt="User Image" src="'+assigned_to+'" width="60%"> <div class="tools"><i class="fa fa-trash-o" onclick="deleteTask('+taskid+');"></i></div>*/

			proj_tasks+=task_item;

		}

		proj_tasks+='</ul> </div><!-- /.box-body --> </div><!-- /.box -->';

		$('.content').html(proj_tasks);

	}
	$(document).ready(function(){
	var sdate=$("#sdate").val();
	var edate=$("#edate").val();
	var date = sdate+"-"+edate;
	$(".val").val(date);
	
	$("#next").click(function(){ 
		var x =$(".val").val();
		var proj = $(".proj").val(); 
		$.ajax({
			    type: "POST",		
			    url: "get_date.php",
			    data:{ name: x, type:'next'},
			    dataType: "html",
			    success: function(result) {
				$(".val").val(result);
				var y =$(".val").val();
				 $.ajax({
				    type: "POST",		
				    url: "get_todo_wreport.php",
				    data:{ custom_date: y,project : proj, type:'project'},
				    dataType: "html",
				    success: function(result) {
					$('.ajax_table').html(result)
							}
				});
			   }
		});
	});
	$("#prev").click(function(){ 
		var x =$(".val").val();
		var proj = $(".proj").val(); 
		$.ajax({
			    type: "POST",		
			    url: "get_date.php",
			    data:{ name: x, type:'prev'},
			    dataType: "html",
			    success: function(result) {
				$(".val").val(result);
				var x=$(".val").val();
				 $.ajax({
				    type: "POST",		
				    url: "get_todo_wreport.php",
				    data:{ custom_date: x,project : proj, type:'project'},
				    dataType: "html",
				    success: function(result) {
					$('.ajax_table').html(result)
							}
				});
			    }
		});
	});
});

        </script>
<script type="text/javascript">

$(function() {
    var startDate;
    var endDate;

    var selectCurrentWeek = function() {
        window.setTimeout(function () {
            $('.week-picker').find('.ui-datepicker-current-day a').addClass('ui-state-active')
        }, 1);
    }

    $('.week-picker').datepicker( {
        showOtherMonths: true,
        selectOtherMonths: true,
        onSelect: function(dateText, inst) { 
            var date = $(this).datepicker('getDate');
            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
            var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
            $('#startDate').text($.datepicker.formatDate( dateFormat, startDate, inst.settings ));
            $('#endDate').text($.datepicker.formatDate( dateFormat, endDate, inst.settings ));
var sel_week =$.datepicker.formatDate( dateFormat, startDate, inst.settings ) + "-" + $.datepicker.formatDate( dateFormat, endDate, inst.settings );
	var x= $('#sel_week').val(sel_week);
		
            selectCurrentWeek();
		getweek();
        },
        beforeShowDay: function(date) {
            var cssClass = '';
            if(date >= startDate && date <= endDate)
                cssClass = 'ui-datepicker-current-day';
            return [true, cssClass];
        },
        onChangeMonthYear: function(year, month, inst) {
            selectCurrentWeek();
        }
	
	
    });


    $('.week-picker .ui-datepicker-calendar tr').live('mousemove', function() { $(this).find('td a').addClass('ui-state-hover'); });
    $('.week-picker .ui-datepicker-calendar tr').live('mouseleave', function() { $(this).find('td a').removeClass('ui-state-hover'); });
function getweek()
{
	var x=$(".week_pick").val();
	var proj = $(".proj").val(); 
	$.ajax({
			    type: "POST",		
			    url: "get_todo_wreport.php",
			    data:{ custom_date: x, project : proj, type:'project'},
			    dataType: "html",
			    success: function(result) {
		$('.ajax_table').html(result)
				}
		});
}	
});
</script>
