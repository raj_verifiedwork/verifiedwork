<?php
session_start();
$pageno=1;
$pagename=" Task Assigning ";

require_once('genfunctions.php');
validate_login();

$subdomainid=get_session('VW_DOMAIN_ID');

$cur_user_id=get_session('VW_USER_ID');
$cur_domain_id=get_session('VW_DOMAIN_ID');
$cur_username=get_session('VW_USER_NAME');
$cur_email=get_session('VW_USER_EMAIL');

require_once('vw_header.php');
require_once('vw_leftmenu.php');

?>

<!-- fullCalendar -->
<link href="assets/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
<link href="assets/css/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css" media='print' />

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $pagename; ?>
        <small id="pageloader"><img src="assets/img/ajax-page-loader.gif" /></small>
    </h1>
   <?php
	require_once('breadcrumb.php');
    ?>
</section>

<!-- Main content -->
<section class="content">


<div class="row">
<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header">
            <!--<h4 class="box-title">Project</h4>-->
	    
		<select class="form-control" name="project" id="project" style="margin:8px; width:95%;">
		<option value="0">Select Project</option>
		<?php

		$mgres=mysql_query("SELECT * FROM user_projects WHERE userid='$cur_user_id' ORDER BY id ASC")or die(mysql_error());
		$mgr=mysql_fetch_array($mgres);

		$userprojectids=$mgr['project_ids'];

		$pids=explode(',',$userprojectids);

		for($i=0;$i<count($pids);$i++)
		{
			if($pids[$i]!="")
			{

				$pid=$pids[$i];
				$pres=mysql_query("SELECT projectname FROM projects WHERE id='$pid' ORDER BY id ASC")or die(mysql_error());
				$pr=mysql_fetch_array($pres);
				$pname=$pr['projectname'];

				echo "<option value='$pids[$i]'>".$pname."</option>"; 

			}
	
		}
		?>
		</select>
		<select class="form-control" name="users" id="users" style="margin:8px; width:95%;">
		<option value="<?php echo $cur_user_id; ?>">Select User</option>
		<?php echo get_office_users_list($subdomainid); ?>
		</select>
        </div>
        <div class="box-body">
            <!-- the events -->
            <div id='external-events'>&nbsp;</div>
	    <p><input type='checkbox' id='drop-remove' /><label for='drop-remove'> &nbsp; remove after drop</label></p>
        </div><!-- /.box-body -->
    </div><!-- /. box -->

</div><!-- /.col -->
<div class="col-md-8">
    <div class="box box-primary">                                
        <div class="box-body no-padding">
            <!-- THE CALENDAR -->
            <div id="calendar"></div>
        </div><!-- /.box-body -->
    </div><!-- /. box -->
</div><!-- /.col -->
</div><!-- /.row -->  

</section><!-- /.content -->
</aside><!-- /.right-side -->

<?php
	require_once('vw_footer.php');
?>
<!-- fullCalendar -->
<script src="assets/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<!-- Page specific script -->
<script type="text/javascript">
$(function() {

$("#project").change(function(){

	var pid=$(this).val();

	if(pid!=0)
	{
		var data={
				type:'getProjectTaskForCalender',					
				pid:pid
			};
		$('#pageloader').show();
		$.ajax({
			type:'POST',
			url:'calender_actions.php',
			data:data,
			success:function(output){
				
				$('#external-events').html(output);
				$('#pageloader').hide();
				ini_events($('#external-events div.external-event'));
				//window.location.reload();
			} 
		});

	}else{
		alert("Please select any Project!");
	}

});

var userid="<?php echo $cur_user_id; ?>";

var source='calender_actions.php?type=fetchCalenderEvents&userid='+userid;

$("#users").change(function(){ 

	userid=$(this).val();

	//alert(userid);
	var newsource='calender_actions.php?type=fetchCalenderEvents&userid='+userid;

	$("#calendar").fullCalendar('removeEventSource',source);
	$('#calendar').fullCalendar('refetchEvents');
	$("#calendar").fullCalendar('addEventSource',newsource); 
	$('#calendar').fullCalendar('refetchEvents');

	source = newsource;	

});


if(!jQuery.ui)
{
   // bla bla
	console.log("jQuery.ui Not Loaded");
}

        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
            ele.each(function() {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 1070,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        }
        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();

	$('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {//This is to add icons to the visible buttons
                prev: "<span class='fa fa-caret-left'></span>",
                next: "<span class='fa fa-caret-right'></span>",
                today: 'today',
                month: 'month',
                week: 'week',
                day: 'day'
            },
	    events: source,
            //Random default events
            /*events: [
                {
                    title: 'All Day Event',
                    start: new Date(y, m, 1),
                    backgroundColor: "#f56954", //red 
                    borderColor: "#f56954" //red
                },
                {
                    title: 'Long Event',
                    start: new Date(y, m, d - 5),
                    end: new Date(y, m, d - 2),
                    backgroundColor: "#f39c12", //yellow
                    borderColor: "#f39c12" //yellow
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false,
                    backgroundColor: "#0073b7", //Blue
                    borderColor: "#0073b7" //Blue
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false,
                    backgroundColor: "#00c0ef", //Info (aqua)
                    borderColor: "#00c0ef" //Info (aqua)
                },
                {
                    title: 'Birthday Party',
                    start: new Date(y, m, d + 1, 19, 0),
                    end: new Date(y, m, d + 1, 22, 30),
                    allDay: false,
                    backgroundColor: "#00a65a", //Success (green)
                    borderColor: "#00a65a" //Success (green)
                },
                {
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/',
                    backgroundColor: "#3c8dbc", //Primary (light-blue)
                    borderColor: "#3c8dbc" //Primary (light-blue)
                }
            ],*/
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function(date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                copiedEventObject.backgroundColor = $(this).css("background-color");
                copiedEventObject.borderColor = $(this).css("border-color");

		var taskid=$(this).attr('val');

		var title=copiedEventObject.title;
		var startdate=copiedEventObject.start;
		var allDay=copiedEventObject.allDay;
		var backgroundColor=copiedEventObject.backgroundColor;
		var borderColor=copiedEventObject.borderColor;
	
		var userid=$("#users").val();

		var data={
				type:'addToCalender',
				userid:userid,
				taskid:taskid,				
				title:title,
				startdate:startdate,
				allDay:allDay,
				backgroundColor:backgroundColor,
				borderColor:borderColor
			};
		
		$.ajax({
			type:'POST',
			url:'calender_actions.php',
			data:data,
			success:function(output){
				
			} 
		});

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }

            }
        });

        /* ADDING EVENTS */
        var currColor = "#f56954"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function(e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css("color");
            //Add color effect to button
            colorChooser
                    .css({"background-color": currColor, "border-color": currColor})
                    .html($(this).text()+' <span class="caret"></span>');
        });
        $("#add-new-event").click(function(e) {
            e.preventDefault();
            //Get value and make sure it is not null
            var val = $("#new-event").val();
            if (val.length == 0) {
                return;
            }

            //Create event
            var event = $("<div />");
            event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
            event.html(val);
            $('#external-events').prepend(event);

            //Add draggable funtionality
            ini_events(event);

            //Remove event from text input
            $("#new-event").val("");
        });
    });
</script>
