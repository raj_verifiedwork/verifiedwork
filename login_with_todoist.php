<?php
ob_start();
require_once('genfunctions.php');

setcookie("user_login_failed", 0, time()+3600);

$err="";
$cap = 'notEq';

?>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Verified Work :: Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <!-- bootstrap 3.0.2 -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/custom_styles.css" rel="stylesheet" type="text/css" />
	<!-- jQuery 2.0.2 -->
	<script src="assets/js/jquery.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
	<style>
	body{
		background: url("assets/img/noisebg.png") repeat scroll 0 0 #EEEEEE;
	}
	.form-box .body > .form-group > input, .form-box .footer > .form-group > input{
		border:1px solid #ddd;
	}
	.header1{
		background: #fff;
		border-radius: 4px 4px 0 0;
		padding:10px;
		font-weight:bold;
		display: inline-block;
		width: 100%;
	}

	.form_box_new{
	
		background: linear-gradient(to bottom, rgba(255, 255, 255, 0.15) 0%, rgba(0, 0, 0, 0.15) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
		border-right: 1px solid #989899;
		border-left: 1px solid #989899;
		border-top: 1px solid #989899;
		box-shadow: 0 -6px rgba(0, 0, 0, 0.35) inset, 0 -1px rgba(0, 0, 0, 0.15) inset, 0 5px 4px rgba(0, 0, 0, 0.12), 0 -7px 2px rgba(255, 255, 255, 0.8) inset;
		display: block;
		background: #fff;
		position: relative;
		text-decoration: none;
		text-shadow: 0 1px 0 #FFFFFF;

	}

	</style>
    </head>
    <body>

<?php

if(isset($_POST['createtodoist']))
{
	$subdomain=request_get('subdomainname');
	$useremail=request_get('useremail');
	$regtype=request_get('regtype');

	$ps=request_get('ps');

	$user_name=request_get('user_name');
	$user_img=request_get('user_img');

	$created_ip=$_SERVER['REMOTE_ADDR'];

	$created=date('Y-m-d H:i:s');

	$results = mysql_query("SELECT id FROM subdomain_list WHERE subdomain='$subdomain'");
	
	$username_exist = mysql_num_rows($results);
	
	if($username_exist) {

		$err='<div class="callout callout-danger">'.__USERNAME_EXISTS__.'</div>';
		
	}else{

		$auth_userid=request_get('api_token');

		$signupedby=$regtype."_signup";

		$sql="INSERT INTO `subdomain_list` (`id`, `subdomain`, `email`, `created_ip`, `last_active`, `status`, `referral_url`, `verification_code`, `verified`, `createdby`, `created`, `modifiedby`, `modified`) VALUES (NULL, '$subdomain', '$useremail', '$created_ip', '-', '0', '$signupedby', '$auth_userid', '$created', '$regtype', '$created', '$regtype', '$created')";

		mysql_query($sql) or die(mysql_error());

		$refidnew=mysql_insert_id();

		$user_sql="INSERT INTO `users` (`id`, `subdomainid`, `email`, `password`, `fname`, `lanme`, `nickname`, `show_name_options`, `login_with`, `timezone`, `status`, `last_login`, `mail_preference`, `photo`, `usertype`, `created`, `createdby`, `modified`, `modifiedby`) VALUES (NULL, '$refidnew', '$useremail', '', '$user_name', '', '', '0', '4', 'timezone', '0', '', '0', '$user_img', '1', '$created', 'google', '$created', 'google')";	

		$res=mysql_query($user_sql)or die("ERROR : ".mysql_error());

		/*Start Sending Email*/
		$emres=mysql_query("select * from email_templates where emailtype='__WELCOME_EMAIL__'") or die(mysql_error());
		$emr=mysql_fetch_array($emres);

		$subject=$emr['subject'];

		$email_username = $user_name;

		$message=$emr['content'];

		$messageNew=html_entity_decode(str_replace("%username%",$email_username,$message));

		send_mail_with_smtp($useremail,$subject,$messageNew);

		/*End of Sending Email*/


		session_start();

		set_session('VW_USER_ID',$userid);
		set_session('VW_DOMAIN_ID',$refidnew);
		set_session('VW_USER_EMAIL',$useremail);
		set_session('VW_USER_NAME',$user_name);
		set_session('VW_DOMAIN_URL',$subdomain);
		set_session('VW_USER_LOGIN_WITH',1);

		$newurl="http://".$subdomain.".verifiedwork.com/index.php?email=$useremail&p=$ps";

		header("location:$newurl");
		exit();



	}

}


?>
	<form method="post">

	<?php 
	if(isset($_REQUEST['job']))
	{

		if($_REQUEST['job']=="login")
		{
	?>
	<div class="form-box form_box_new" id="LoginTodist">

	    <div class="header1"><img src="assets/img/vw_logo.png" width="30%" style="float:left;"/> <span style="float:right;">Login with Todoist Account </span> </div>
	    <form method="post">
		<div class="body bg-white">
		    <div class="form-group">
		        <input type="email" name="email" id="u_emailid" class="form-control" placeholder="E-Mail" required />
		    </div>
		    <div class="form-group">
		        <input type="password" name="password" id="u_password" class="form-control" placeholder="Password" required />
		    </div>
	 	</div>
		<div class="footer"> 
			<input type="hidden" name="tp" id="tp" />
                                                              
		    <button type="button" name="login" id="login" class="btn btn-danger">Sign in</button>  
		    
		    <p><a href="login.php">Back to Verified Work Login</a></p>
		   
		</div>
	    </form>

	   <div class="margin text-center">
	       <!-- <span style="color:green; font-weight:bold;">Sign in using social networks</span>
		<a class="btn btn-block btn-social btn-google-plus" href="<?php echo $authUrl; ?>" ><i class="fa fa-google-plus"></i> Sign in with Google </a> 
		<a class="btn btn-block btn-social btn-foursquare" id="loginwith_todoist" ><i class="fa fa-foursquare"></i>Sign in with Todoist.com</a>-->
	    </div><br>
	</div>

	<?php
		}else if($_REQUEST['job']=="signup"){
	?>

	<div class="form-box form_box_new" id="SignupTodist">

	    <div class="header1"><img src="assets/img/vw_logo.png" width="30%" style="float:left;"/> <span style="float:right;">Signin with Todoist Account </span> </div>
	    <form method="post">
		<div class="body bg-white">
		    <div class="form-group">
		        <input type="email" name="email" id="u_email" class="form-control" placeholder="E-Mail" required />
		    </div>
		    <div class="form-group">
		        <input type="password" name="password" id="u_pwd" class="form-control" placeholder="Password" required />
		    </div>
	 	</div>
		<div class="footer">                                                               
		    <button type="button" name="signup" id="signup_todoist" class="btn btn-danger">Sign in</button>  
		    
		    <p><a href="login.php">Back to Verified Work Login</a></p> 
		   
		</div>
	    </form>

	   <div class="margin text-center">
	       <!-- <span style="color:green; font-weight:bold;">Sign in using social networks</span>
		<a class="btn btn-block btn-social btn-google-plus" href="<?php echo $authUrl; ?>" ><i class="fa fa-google-plus"></i> Sign in with Google </a> 
		<a class="btn btn-block btn-social btn-foursquare" id="loginwith_todoist" ><i class="fa fa-foursquare"></i>Sign in with Todoist.com</a>-->
	    </div><br>
	</div>

	
	<div class="form-box form_box_new" id="CreateAccountWithTodoist">
	    <div class="header1"><img src="assets/img/vw_logo.png" width="30%" style="float:left;"/> <span style="float:right;">Register New Account </span> </div>
            <form method="post">
                <div class="body bg-white">
			<b>Welcome <span id="userfullname">&nbsp;</span> !</b>
                    <div class="form-group">
			<div class="input-group">
			<span class="input-group-addon">http://</span>
			<input class="form-control" type="text" name="subdomainname" id="subdomainname" placeholder="Your Login Page" maxlength="24" required>
			<span class="input-group-addon">.verifiedwork.com <span id="user-result"></span> </span>
			</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
			Your E-Mail : <span id="todoist_useremail"></span>
			<input class="form-control" type="hidden" name="useremail" id="useremail" placeholder="Your E-Mail" >
			</div>
                    </div>
		    <div class="form-group">
                        <span>
				<input type="checkbox" onClick="EnableSubmit(this)" /> &nbsp;
				I agree to Verified Work terms.
			</span>    
                    </div>

                </div>
                <div class="footer">                    
		    <input class="form-control" type="hidden" name="regtype" value="todoist" >
		    <input class="form-control" type="hidden" name="userid" >
		    <input type="hidden" name="user_name" id="u_fullname" />
		    <input type="hidden" name="api_token" id="api_token" />
		    <input type="hidden" name="user_img" id="user_img" />
		    <input type="hidden" name="todo_userid" id="todo_userid" />
		    <input type="hidden" name="ps" id="ps"/>

                    <button type="submit" name="createtodoist" id="Accept" class="btn btn-danger">Create</button>
			<br><br>
                    <a href="login.php" id="si" class="text-center">I already have an account</a>
                </div>
            </form>

            <div class="margin text-center">
              <!--  <span>Register using social networks</span>-->
                <br/>
		<!-- <a class="btn bg-red btn-circle" href="<?php echo $authUrl; ?>"><i class="fa fa-google-plus"></i></a> 
		<a class="btn btn-block btn-social btn-google-plus" href="<?php echo $authUrl; ?>" ><i class="fa fa-google-plus"></i> Sign in with Google </a> -->
                <!--  <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
                <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button> -->
		
            </div>
        </div>
		
	
	<?php
		}

	}

	?>

	</form>


       <div class="pagecenter_div">
	
		   <div class="col-md-12">
                           
                        </div><!-- /.col -->
	</div>
  
        <!-- Bootstrap -->
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>        
	<script type="text/javascript">
	
	EnableSubmit = function(val)
	{
	    var sbmt = document.getElementById("Accept");

	    if (val.checked == true)
	    {
		sbmt.disabled = false;
	    }
	    else
	    {
		sbmt.disabled = true;
	    }
	}    

	function callback_userinfo_reg(result){

		console.log(result);

		if(result!="LOGIN_ERROR")
		{
			var apitoken=result.api_token;
			var avatar_big=result.avatar_big;
			var avatar_medium=result.avatar_medium;
			var avatar_small=result.avatar_small;
			var todo_email=result.email;
			var full_name=result.full_name;
			var todo_userid=result.id;
			var inbox_project=result.inbox_project;
			var seq_no=result.seq_no;
			var timezone=result.timezone;
			var token=result.token;

			$("#CreateAccountWithTodoist").show();
			$("#SignupTodist").hide();

			document.getElementById("userfullname").innerHTML=full_name;
			document.getElementById("todoist_useremail").innerHTML=todo_email;

			document.getElementById("u_fullname").value=full_name;
			document.getElementById("api_token").value=apitoken;
			document.getElementById("user_img").value=avatar_small;
			document.getElementById("todo_userid").value=todo_userid;
			document.getElementById("useremail").value=todo_email;
			
			
		}else{
			alert("Login Failed! Try again with Valid Username and Password");
			return false;
		}

	}

	function callback_userinfo_login(result){

		console.log(result);

		if(result!="LOGIN_ERROR")
		{
			var apitoken=result.api_token;
			var avatar_big=result.avatar_big;
			var avatar_medium=result.avatar_medium;
			var avatar_small=result.avatar_small;
			var todo_email=result.email;
			var full_name=result.full_name;
			var todo_userid=result.id;
			var inbox_project=result.inbox_project;
			var seq_no=result.seq_no;
			var timezone=result.timezone;
			var token=result.token;
			var p=document.getElementById('tp').value;

			$("#CreateAccountWithTodoist").show();
			$("#SignupTodist").hide();

			var data={
				type:"todoist_login",
				apitoken:apitoken,
				avatar_big:avatar_big,
				avatar_medium:avatar_medium,
				avatar_small:avatar_small,
				todo_email:todo_email,
				full_name:full_name,
				todo_userid:todo_userid,
				inbox_project:inbox_project,
				seq_no:seq_no,
				token:token,
				p:p
			}
			$.ajax({
				type:"POST",
				url:"user_actions.php",
				data:data,
				success:function(output){
					//alert(output);
					window.location.href=output;
				}

			});
		}else{
			alert("Login Failed! Try again with Valid Username and Password");
			return false;
		}

	}

	$(document).ready(function(){

		
	<?php
		if($_REQUEST['job']=="signup"){
	?>
			document.getElementById("Accept").disabled = true;
	<?php
		}
	?>

		$("#CreateAccountWithTodoist").hide();
		$("#SignupTodist").show();

		$("#signup_todoist").click(function(){
	
			var un=$("#u_email").val();
			var pwd=$("#u_pwd").val();

			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = 'http://todoist.com/API/login?email='+un+'&password='+pwd+'&callback=callback_userinfo_reg';

			document.getElementsByTagName('head')[0].appendChild(script);

			document.getElementById('ps').value=pwd;

		});

		$("#login").click(function(){
	
			var un=$("#u_emailid").val();
			var pwd=$("#u_password").val();

			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = 'http://todoist.com/API/login?email='+un+'&password='+pwd+'&callback=callback_userinfo_login';

			document.getElementsByTagName('head')[0].appendChild(script);

			document.getElementById('tp').value=pwd;


		});


		$("#subdomainname").keyup(function (e) {
			//removes spaces from username
			$(this).val($(this).val().replace(/\s/g, ''));
		});

		$("#subdomainname").blur(function (e) {

			//removes spaces from username
			$(this).val($(this).val().replace(/\s/g, ''));

			var subdomainname = $(this).val();
			if(subdomainname.length < 2){$("#user-result").html('');return;}

			if(subdomainname.length >= 2){
				$("#user-result").html('<img src="assets/img/ajax-loader.gif" width="18px" >');
				$.post('check_domain.php', {'subdomainname':subdomainname }, function(data) {
		  			$("#user-result").html(data);
				});
			}

		});

	});

	</script>
    </body>
</html>
<?php
ob_end_flush();
?>
