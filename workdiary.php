<?php
session_start();
$pageno=1;
$pagename=" Work Diary ";

require_once('genfunctions.php');
validate_login();

$subdomainid=get_session('VW_DOMAIN_ID');

require_once('vw_header.php');
require_once('vw_leftmenu.php');

?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $pagename; ?>
        <small id="pageloader"><img src="assets/img/ajax-page-loader.gif" /></small>
    </h1>
   <?php
	require_once('breadcrumb.php');
    ?>
</section>
<style>
.cusBorder{
/*	border:1px solid #ccc;*/

}
address {
    border: 1px solid #ccc;
}
.invoice{
	width:99%;
}

.logActive{
	border:1px solid #3c763d; 
	background-color:#3c763d;
	margin-right:1px;
	cursor:pointer;
}

.logInactive{
	border:1px solid #ccc;
	margin-right:1px;
	cursor:pointer;
}

.emptyLogs{
	border:1px dashed #ccc; 
	width:100%; 
	height:100%;
}

</style>
<!-- Main content -->
<section class="content invoice">

<?php
	//echo "<pre>"; echo var_dump($_SESSION); echo "</pre>";

	$userid=get_session('VW_USER_ID');

	$screenshotpath="/var/www/vwwork/screenshots/1/2014/11/05/";
echo $sdate = date("D,M d,Y",strtotime('monday this week'));	
echo "<br>";


 $sdate = date("Y-m-d",strtotime('monday this week'));	
 $edate = date('Y-m-d',strtotime($sdate . "+5 days"));	
	$sd_date = $sdate;		
	$day_date = array($sd_date);
	
		for($i=1; $i<6; $i++)
		{
			$date1 = $sd_date.",";
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			array_push($day_date,$tomorrow );
			$sd_date = $tomorrow;
			$data[]=$tomorrow;
		}
$mydata = implode(",", $day_date);

$sql="select * from projects where userid=$userid and status=0 ";
	$res=mysql_query($sql) or die("Projects : ".mysql_error());
echo "<form method='post' action=''>";

echo "<div class='callout callout-info ' >

		<table>";
while($r=mysql_fetch_array($res))
	{
	
	$projectid=$r['id'];
	$sql1="select count(status) as c,projectid from time_tracking  where projectid='$projectid' and status=1 and FIND_IN_SET(log_date,'$mydata')"; 
	$res1=mysql_query($sql1) or die("Projects : ".mysql_error());
	while($r1=mysql_fetch_array($res1))
	{
		echo "<tr><td id='pid' >".$r['projectname']."</td><td>&nbsp;&nbsp;&nbsp;".$r1['c']." hrs  of 48 hrs this Week</td>"; 
		echo "<td><a href='workdiary.php?pid=".$projectid."' ><button type='button' id='$projectid' name='view' class='btn btn-default btn-flat' style='color:#00ACD6'>View Work Diary</button></a>"; 
		echo "</td></tr>";
	}  
}

echo "</table></div>";

echo "</form>";
echo '<div class="input-group">
                   <!--Date range button: &nbsp;&nbsp;&nbsp; <input id="daterange-btn" name="date"  data-weekpicker="weekpicker" />-->
		<h4>Date range button :&nbsp;
		<a href="#" id="prev"><img src="img/back.png" height="15px" width="15px"></a>
		<input type="text" id="datepicker" required="required" name="date" class="val" value="'.$sdate.'"  />
		<a href="#" id="next"><img src="img/forward.png" height="15px" width="15px"></a>
		</h4>
                </div>';

	$sql="select * from projects where userid=$userid and status=0 ";
	$res=mysql_query($sql) or die("Projects : ".mysql_error());

	
	$projectid=0;
	$current_date=date('2014-11-05');

//, strtotime('-1 day')

	/*echo "<select id='projects' name='projects'>";
	echo "<option value='0'>Select Project</option>";
	while($r=mysql_fetch_array($res))
	{
		echo "<option value='".$r['id']."'>".$r['projectname']."</option>";
		$projectid=$r['id'];
	}
	echo "</select>";*/

 $projectid=request_get('pid');
$userid=request_get('uid');
$projectname="";
echo "<input type='hidden' value='$projectid' id='getid' />";
$sql2="select * from projects where id='$projectid'";
$res=mysql_query($sql2) or die("Projects : ".mysql_error());
echo "<select id='users' name='users' onchange='changeuser(this.value);'>";
	echo "<option value='0' >Select users</option>";
while($r=mysql_fetch_array($res))
	{
echo $users=explode(",",$r['joinedpersons']);
	foreach($users as $val){
	$sql3="select * from users where id='$val'";
	$res1=mysql_query($sql3) or die("Projects : ".mysql_error());
	$r1=mysql_fetch_array($res1);
	
if($r['id']==$projectid)
{
	$projectname=$r['projectname'];
}
		echo "<option value='".$r1['id']."'>".$r1['fname']."</option>";
		}	
	}
	echo "</select>";

	//echo $projectid;

	echo '<div class="row">
			<div class="col-xs-12">
			    <h2 class="page-header">
				<i class="fa fa-globe"></i>'.$projectname.'
				<small class="pull-right">Date: 2/10/2014</small>
			    </h2>                            
			</div><!-- /.col -->
		</div>
		<div class="row invoice-info">
			<div class="col-xs-12 table-responsive">
				<table class="table table-bordered">';

				for($i=9;$i<24;$i++)
				{
					$st=$i.":00:00";
					$ed=($i+1).":00:00";

					$start_time=date('H:i:s',strtotime($st));
					$end_time=date('H:i:s',strtotime($ed));

					$start_time1=date('h a',strtotime($st));
					$end_time1=date('h:i a',strtotime($ed));

					$timeduration=$start_time1." - ".$end_time1;
					
					echo '<tr>';
						echo '<td width="10%"><b>'.$start_time1.'</b></td>';

						echo '<td width="14%">';
						$min1st=$i.":00:00";
						$min1ed=$i.":10:00";
							echo generateColumn($userid,$projectid,$current_date,$min1st,$min1ed);
						echo '</td>';

						echo '<td width="14%">';
						$min2st=$i.":10:00";
						$min2ed=$i.":20:00";
							echo generateColumn($userid,$projectid,$current_date,$min2st,$min2ed);
						echo '</td>

						<td width="14%">';
						$min3st=$i.":20:00";
						$min3ed=$i.":30:00";
							echo generateColumn($userid,$projectid,$current_date,$min3st,$min3ed);
						echo '</td>

						<td width="14%">';
						$min4st=$i.":30:00";
						$min4ed=$i.":40:00";
							echo generateColumn($userid,$projectid,$current_date,$min4st,$min4ed);
						echo '</td>

						<td width="14%">';
						$min5st=$i.":40:00";
						$min5ed=$i.":50:00";
							echo generateColumn($userid,$projectid,$current_date,$min5st,$min5ed);
						echo '</td>

						<td width="14%">';
						$min6st=$i.":50:00";
						$min6ed=$i.":59:59";
							echo generateColumn($userid,$projectid,$current_date,$min6st,$min6ed);
						echo '</td>';

					echo '</tr>';
				} // end of for loop
				echo '</table>

			</div>
		</div>';



function generateColumn($userid,$projectid,$current_date,$min1st,$min1ed)
{
	
	$string="";

	$sql="select * from time_tracking where userid='$userid' and projectid='$projectid' and log_date='$current_date' and log_starttime>='$min1st' and log_starttime<='$min1ed'";

	$res=mysql_query($sql) or die("TIME TRACK ERR : ".mysql_error());
	if(mysql_num_rows($res)>0)
	{
		$r=mysql_fetch_assoc($res);
		/*echo $r['status'];
		echo $r['minutes'];
		echo $r['total_key_strokes'];
		echo $r['total_mouse_clicks'];*/
	
		$activityLevel=$r['minutes'];

		$loginfo=$r['log_minutes_info'];

		$arr=json_decode($loginfo,true);

		$screenpath="screenshots/1/2014/11/05/2014-11-05_13_10_01.png";

		$showpopuptable="<table width=100% class=table>
				<tr><td colspan=2><img src=$screenpath width=100% height=200px /></td></tr>
				<tr><td>Snapshot Taken</td><td>Snapshot time</td></tr>
				<tr><td>Memo</td><td>".$r['memo']."</td></tr>
				<tr><td>Active Window</td><td>Active Window text</td></tr>
				<tr><td>Total Keystrokes</td><td>".$r['total_key_strokes']."</td></tr>
				<tr><td>Total Mouse clicks</td><td>".$r['total_mouse_clicks']."</td></tr>
			</table>";

		$string.="<table width='100%'>
				<tr>
				
					<td colspan='2'>
					<div class='showpopup' data-container='body' data-toggle='popover' data-content='$showpopuptable'>
					<img src='$screenpath' width='100%' height='110px' />
					</td>
				</tr>
				<tr>
					<td>time</td>
					<td align='right'>";
				for($m=0;$m<count($arr);$m++)
				{
			$tooltip="Activity Level : ".$activityLevel.", Events : ".$arr[$m]['keycount']." Keyboard, ".$arr[$m]['mousecount']." Mouse";
					if($arr[$m]['keycount']==0 && $arr[$m]['mousecount']==0)
					{
						$string.= "<span class='logInactive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}else{
						$string.= "<span class='logActive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}
				}
					$string.= "</td>
				</tr>
		</table>";
	}else{
		$string.= "<div class='emptyLogs'><br><br><br><br><br><br><br></div>";
	}

	return $string;
}
?>

</section><!-- /.content -->
</aside><!-- /.right-side -->

<?php
	require_once('vw_footer.php');
?>
<script type="text/javascript">
function changeuser(value){
	var pid=document.getElementById('getid').value;
	window.location.href="workdiary.php?pid="+pid+"&uid="+value;	
	
	}

$(document).ready(function(){

	
	$('.showpopup').popover({
		placement:'auto left',
		trigger: 'hover',
		html: true,
		template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
	});

$("#next").click(function(){ 
	var x =$(".val").val();
	$.ajax({
		    type: "POST",		
		    url: "get_date.php",
		    data:{ name: x, type:'next'},
		    dataType: "html",
		    success: function(result) {
			$(".val").val(result);

		   }
	});

});
$("#prev").click(function(){ 
	var x =$(".val").val();
	$.ajax({
		    type: "POST",		
		    url: "get_date.php",
		    data:{ name: x, type:'prev'},
		    dataType: "html",
		    success: function(result) {
			$(".val").val(result);
		    }
	});
});
 
	$('#datepicker').datepicker({
			format: 'mm/dd/yyyy',
			"setDate": new Date(), 
		});

		$('#datepicker').on('changeDate', function(ev){
			$(this).datepicker('hide');
		});

		$(".cus_today").click(function(){
			$('#datepicker').datepicker('setValue', today);
			$('#datepicker').datepicker('hide');
		});
		$(".cus_tomorrow").click(function(){	
			$('#datepicker').datepicker('setValue', tomorrow);
			$('#datepicker').datepicker('hide');
			
		});
		$(".cus_nextweek").click(function(){
			$('#datepicker').datepicker('setValue', nextweek);
			$('#datepicker').datepicker('hide');
		});
		$('.datepicker').on('mousemove', 'tr', function () {
			$(this).css("background","#EEEEEE");        
		 });
		$('.datepicker').on('mouseleave', 'tr', function () {
			$(this).css("background","#FFFFFF");       
		});
		


});

</script>
