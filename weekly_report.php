<?php
session_start();
$pageno=2;
$pagename=" Weekly Report ";

require_once('genfunctions.php');
require_once('vw_header.php');
require_once('vw_leftmenu.php');
$vw_userid = get_session('VW_USER_ID');
	$sdate = date("Y-m-d",strtotime('monday this week'));
	$sd_date = $sdate;		
	$day_date = array($sd_date);
	for($i=1; $i<7; $i++)
	{
	$date1 = $sd_date;$date11 = str_replace('-', '/', $date1);
	$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
	array_push($day_date,$tomorrow );
	$sd_date = $tomorrow;
	}
$sw=date('m/d/Y',strtotime($sdate)); 
$ew=date('m/d/Y',strtotime($sd_date)); 

?>
 	 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css"> 

      
<script type="text/javascript">

$(function() {
    var startDate;
    var endDate;

    var selectCurrentWeek = function() {
        window.setTimeout(function () {
            $('.week-picker').find('.ui-datepicker-current-day a').addClass('ui-state-active')
        }, 1);
    }

    $('.week-picker').datepicker( {
        showOtherMonths: true,
        selectOtherMonths: true,
        onSelect: function(dateText, inst) { 
            var date = $(this).datepicker('getDate');
            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
            var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
            $('#startDate').text($.datepicker.formatDate( dateFormat, startDate, inst.settings ));
            $('#endDate').text($.datepicker.formatDate( dateFormat, endDate, inst.settings ));
var sel_week =$.datepicker.formatDate( dateFormat, startDate, inst.settings ) + "-" + $.datepicker.formatDate( dateFormat, endDate, inst.settings );
	var x= $('#sel_week').val(sel_week);
		
            selectCurrentWeek();
		getweek();
        },
        beforeShowDay: function(date) {
            var cssClass = '';
            if(date >= startDate && date <= endDate)
                cssClass = 'ui-datepicker-current-day';
            return [true, cssClass];
        },
        onChangeMonthYear: function(year, month, inst) {
            selectCurrentWeek();
        }
	
	
    });


    $('.week-picker .ui-datepicker-calendar tr').live('mousemove', function() { $(this).find('td a').addClass('ui-state-hover'); });
    $('.week-picker .ui-datepicker-calendar tr').live('mouseleave', function() { $(this).find('td a').removeClass('ui-state-hover'); });
function getweek()
{
	var x=$(".week_pick").val();
	$.ajax({
			    type: "POST",		
			    url: "get_weekreport.php",
			    data:{ name: x, type:'select'},
			    dataType: "html",
			    success: function(result) {
		$('.ajax_table').html(result)
				}
		});
}	
});
</script>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
         <?php echo $pagename; ?>
       <!-- <small>it all starts here</small>-->
    </h1>
   <?php
	require_once('breadcrumb.php');
    ?>
</section>
<br /><br /> 
Week of: &nbsp;&nbsp;<a href="#" id="prev"><img src="img/back.png" height="15px" width="15px"></a><input type="text" class="week-picker week_pick val" id="sel_week"><a href="#" id="next"><img src="img/forward.png" height="15px" width="15px"></a>
    <br /><br />
<!-- <a href="export_vw_wreport.php?ept=excel"><button value="Download">Download</button></a> -->
<!-- Main content -->
<section class="content">
<?php
$sql="select * from projects where userid='$vw_userid'";
$res=mysql_query($sql)or die("ERROR : ".mysql_error());
?>
<div class="ajax_table">
<div class="box box-primary">
<div class="box-body table-responsive" id="table-responsive">
    <table id="example1" class="table table-bordered table-striped ">
        <thead>
            <tr>
                <th>Client</th>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
		<th>Thu</th>
		 <th>Fri</th>
		<th>Sat</th>
		<th>Sun</th>
		<th>Total Hours</th>
            </tr>
        </thead>
        <tbody>
	<?php 
	$i=1;
	while($r=mysql_fetch_array($res))
	{
	$proj_name = $r['projectname'];
	$pid = $r['id'];
	
	$sql1 = mysql_query("SELECT SUM(hours) as tot_hours FROM `task_compeletion_hrs` where DATE(workdate) >= '".$sdate. "'  AND  DATE(workdate) <= '".$sd_date."' and userid='$vw_userid' ");

	$row = mysql_fetch_array($sql1);	
	$hours = $row['tot_hours'];
	  echo "<tr>";
               echo "<td>".$proj_name."</td>";
               		 foreach($day_date as $weekdate){
			$sql11 = mysql_query("SELECT SUM(hours) as tot_hours FROM `task_compeletion_hrs` WHERE DATE(workdate) = '$weekdate' and userid='$vw_userid' "); 	
			$rows1= mysql_fetch_array($sql11);	
			echo "<td>".$rows1['tot_hours']."</td>";
             		}
                echo "<td>".$hours."</td>";
           echo "</tr>";
		$i++;
	}
	?>
        </tbody>
     
    </table>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div>
<input type="hidden" name="sdate" id="sdate" value="<?php echo $sw; ?>">
<input type="hidden" name="edate" id="edate" value="<?php echo $ew; ?>">
</section><!-- /.content -->
</aside><!-- /.right-side -->

<?php
	//require_once('vw_footer.php');
?>
<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
    });
</script>
<script>
$(document).ready(function(){
	var sdate=$("#sdate").val();
	var edate=$("#edate").val();
	var date = sdate+"-"+edate;
	$(".val").val(date);
	$("#next").click(function(){ 
		var x =$(".val").val();
		$.ajax({
			    type: "POST",		
			    url: "get_date.php",
			    data:{ name: x, type:'next'},
			    dataType: "html",
			    success: function(result) {
				$(".val").val(result);
				var y =$(".val").val();
				 $.ajax({
				    type: "POST",		
				    url: "get_weekreport.php",
				    data:{ name: y, type:'select'},
				    dataType: "html",
				    success: function(result) {
					$('.ajax_table').html(result)
							}
				});
			   }
		});
	});
	$("#prev").click(function(){ 
		var x =$(".val").val();
		$.ajax({
			    type: "POST",		
			    url: "get_date.php",
			    data:{ name: x, type:'prev'},
			    dataType: "html",
			    success: function(result) {
				$(".val").val(result);
				var x=$(".val").val();
				 $.ajax({
				    type: "POST",		
				    url: "get_weekreport.php",
				    data:{ name: x, type:'select'},
				    dataType: "html",
				    success: function(result) {
					$('.ajax_table').html(result)
							}
				});
			    }
		});
	});
});
</script>
 	
