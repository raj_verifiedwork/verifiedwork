 <!-- Share Options MESSAGE MODAL -->
        <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Share Options</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
				<select id="invitepeople" style="width:80%;" name="invitepeople" placeholder="Invite Members" >
				<?php echo get_office_users_list($subdomainid); ?>				
				</select>
				<input class="form-control" type="hidden" name="selectedpid" id="selectedpid">
				<button class="btn btn-danger btn-sm" id="saveInvite" type="button">Invite</button>
				<br><br>
				<div id="project_details">&nbsp;</div>
				<p><h4>Collaborate with others</h4>
				The people you invite will be able to add, delete and complete tasks from Project.
				<br>
				You will get notified when changes happen.</p>
                        </div>
                        <div class="modal-footer clearfix">

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>

                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<link href="assets/js/uploadify/uploadifive.css" rel="stylesheet" type="text/css" />
<style>
.uploadifive-button {
	float: left;
	margin-right: 10px;
}
#queue1 {
	border: 1px solid #E5E5E5;
	height: 60px;
	overflow: auto;
	margin-bottom: 10px;
	padding: 0 3px 3px;
	width: 100%;
	float: left;
	text-align: left;
}

</style>

<!-- COMMENTS MODAL -->
        <div class="modal fade" id="comments-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Comments</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body" style="height:220px; overflow-y:scroll; padding: 5px;">
				<div id="comments_details">&nbsp;</div>
                        </div>
                        <div class="modal-footer clearfix">
			    <input type="hidden" id="taskid" name="taskid" />
			    <input type="hidden" name="projid" id="projid" />
			    <textarea class="form-control" name="taskcomment" id="taskcomment" placeholder="Comment" rows="2"></textarea>

		<br>		<div id="queue1">Drag & Drop Files Here</div>
				<input id="file_upload_cmt" name="file_upload_cmt" type="file" multiple="true">
				<div id="output1" style="display:none;"></div>
				<input type="hidden" name="upimgids1" id="upimgids1">
			
			    <br>
			    <button type="button" id="addComment" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-plus"></i> Add Comment</button>
                            <button type="button" id="colsecmt" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>

                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!-- TASK COMPLETE MODAL -->
        <div class="modal fade" id="complete-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-tasks"></i> Complete Task </h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body" style="padding: 10px;">
				<div id="complete_details">&nbsp;</div>
				<input type="text" class="form-control esthrs" name="complete_hrs" maxlength="3" id="complete_hrs" placeholder="Complete Hrs"/>
                        </div>
                        <div class="modal-footer clearfix">
			    <input type="hidden" id="taskid" name="taskid" />
			    <input type="hidden" name="projid" id="projid" />
			    <button type="button" id="cmptask" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-plus"></i> Complete Task</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>

                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!-- TASK updatehrs MODAL -->
        <div class="modal fade" id="updatehrs-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-tasks"></i> Task worked hours</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">

				<div style="height:220px; overflow-y:scroll; padding: 5px;" >
					<div id="updatehrs_details">&nbsp;</div>
				</div>
				<br>
				<div class="form-group">
					<input type="text" class="form-control esthrs" name="hours_worked" maxlength="3" id="hours_worked" placeholder="Hrs worked" />
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="workdate" id="workdate" placeholder="Work date" />
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="complete_hrs_desc" id="complete_hrs_desc" placeholder="Description"/>
				</div>
                        </div>
                        <div class="modal-footer clearfix">
			    <input type="hidden" name="ctaskid" id="ctaskid" />
			    <input type="hidden" name="cprojid" id="cprojid" />

			    <button type="button" id="cmphrs" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-plus"></i> Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>

                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->



<!-- TASK updatehrs MODAL -->
        <div class="modal fade" id="todoist_updatehrs-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-tasks"></i> Task worked hours</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">

				<div style="height:220px; overflow-y:scroll; padding: 5px;" >
					<div id="todoist_updatehrs_details">&nbsp;</div>
				</div>
				<br>
				<div class="row">
					<div class="col-xs-3">
						<select id="cusers_ddl" class="form-control" name="cusers_ddl">
						<option value="-1">User</option>
						<?php
						$sql="SELECT id,email FROM `users` where `subdomainid`='$subdomainid' and status='0'";
						$res=mysql_query($sql)or die("ERROR : ".mysql_error());
						$i=1;
						$cur_uid=get_session('VW_USER_ID');
						while($r=mysql_fetch_array($res))
						{
							if($r['id']==$cur_uid)
								echo "<option value='".$r['id']."' selected >".$r['email']."</option>";
							else
								echo "<option value='".$r['id']."' >".$r['email']."</option>";
						}

						?>
						</select>
					</div>
					<div class="col-xs-3">
						<input type="text" class="form-control" name="workdate" id="tworkdate" value="<?php echo date('d-m-Y'); ?>" placeholder="Work date" />
					</div>
					<div class="col-xs-3">
						<select id="chrs_ddl" class="form-control" name="chrs_ddl">
						<option value="-1">Hours</option>
						<?php 
							for($i=0;$i<24;$i++)
							{
								echo "<option value='".$i."'>".$i."</option>";
							}
						?>
						</select>
					</div>
					<div class="col-xs-3">
						<select id="cmins_ddl" class="form-control" name="cmins_ddl">
							<option value="-1">Minutes</option>
							<option value="0.00">00</option>
							<option value="0.17">10</option>
							<option value="0.33">20</option>
							<option value="0.50">30</option>
							<option value="0.67">40</option>
							<option value="0.83">50</option>
						</select>
					</div>
				</div><br>
				<div class="row">
					<div class="col-xs-12">
						<input type="text" class="form-control" name="complete_hrs_desc" id="tcomplete_hrs_desc" placeholder="Description"/>
					</div>
				</div>
                        </div>
                        <div class="modal-footer clearfix">
			    <input type="hidden" name="ctaskid" id="tctaskid" />
			    <input type="hidden" name="cprojid" id="tcprojid" />

			    <button type="button" id="todoist_cmphrs" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-plus"></i> Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>

                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->



</div><!-- ./wrapper -->
<?php      

$timestamp=date('Y-m-d H:i:s');

?>
<style>
.mycustombtns{
	display:block !important;
}

.custompoptask{
   background-clip: padding-box;
    background-color: #FFFFFF;
    border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.176);
    display: block;
    float: left;
    font-size: 14px;
    left: 0;
    list-style: none outside none;
    margin: 2px 0 0;
    min-width: 160px;
    padding: 5px 0;
    position: absolute;
    z-index: 10;
    cursor:pointer;
}

.hoverTable{
	width:100%; 
	border-collapse:collapse; 
	padding:8px;
	border-top:1px solid #ddd;
}
.hoverTable td{ 
	padding:7px; border:#ddd 1px solid;
}
/* Define the default color for all the table rows */
.hoverTable tr{
	background: #fff;
}
/* Define the hover highlight color for the table row */
.hoverTable tr:hover {
  background-color: #000;
  color:#fff;
}

.task:hover{
   background-color:#f5f5f5;
}

</style>
	
        <!-- jQuery 2.0.2 -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- jQuery UI 1.10.3 -->
        <script src="assets/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
	
	<!-- iCheck -->
        <script src="assets/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="assets/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="assets/js/AdminLTE/demo.js" type="text/javascript"></script>
	
	<script src="assets/js/plugins/select2/select2.min.js" ></script>

	<!-- date-range-picker -->
        <script src="assets/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

	<script src="assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<script src="assets/js/uploadify/jquery.uploadifive.min.js"></script>
	<script src="assets/js/jquery.autosize.js"></script>
	
	<script src="assets/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

	<script type="text/javascript"> 

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
		dd='0'+dd
	} 

	if(mm<10) {
		mm='0'+mm
	} 

	today = dd+'-'+mm+'-'+yyyy;

	var currentDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
	var day = currentDate.getDate()
	var month = currentDate.getMonth() + 1
	var year = currentDate.getFullYear()
	var tomorrow= day + "-" + month + "-" + year;

	var firstDay = new Date();
	var nextWeek = new Date(firstDay.getTime() + 7 * 24 * 60 * 60 * 1000);

	var nextweek= nextWeek.getDate()+ "-" + (nextWeek.getMonth() + 1) + "-" + nextWeek.getFullYear();

	function deleteprojfun(pid){

		var res=confirm("Are you sure do you want to delete this?");

		if(res==true)
		{
			$('#pageloader').show();
			var data={
				type:'delProject',
				pid:pid	
			};

			$.ajax({
				type:'POST',
				url:'project_actions.php',
				data:data,
				success:function(output){
					$('#pageloader').hide();
				}
			});

			$("#li_"+pid).remove();
		}
		else{

		}
	}

	function project_invites(invid,action)
	{
		var data={
				type:'responseToProject',					
				invid:invid,
				status:action
			};
		$('#pageloader').show();
		$.ajax({
			type:'POST',
			url:'project_actions.php',
			data:data,
			success:function(output){
				$('#pageloader').hide();
				window.location.reload();
			} 
		});
	}

	function shareproject(selectedpid)
	{
		document.getElementById('selectedpid').value=selectedpid;

		var data={
				type:'shareprojectinfo',					
				pid:selectedpid
			};

		$('#pageloader').show();
		$.ajax({
			type:'POST',
			url:'project_actions.php',
			data:data,
			success:function(output){

				$('#project_details').html(output);
				$('#pageloader').hide();
			} 
		});
	}

	function showtaskcomments(taskid,pid)
	{
		document.getElementById('taskid').value=taskid;
		document.getElementById('projid').value=pid;		

		var data={
				type:'taskcomments',					
				taskid:taskid
			};

		$('#pageloader').show();
		$.ajax({
			type:'POST',
			url:'task_actions.php',
			data:data,
			success:function(output){

				$('#comments_details').html(output);
				$('#pageloader').hide();
			} 
		});
	}

	function showtaskhrs(taskid,pid)
	{
		document.getElementById('ctaskid').value=taskid;
		document.getElementById('cprojid').value=pid;		

		var data={
				type:'taskcompletionhrsHTML',					
				taskid:taskid
			};

		$('#pageloader').show();
		$.ajax({
			type:'POST',
			url:'task_actions.php',
			data:data,
			success:function(output){

				$('#updatehrs_details').html(output);
				$('#pageloader').hide();
			} 
		});
	}

	function showtaskhrs_todoist(taskid,pid)
	{
		document.getElementById('tctaskid').value=taskid;
		document.getElementById('tcprojid').value=pid;		

		var data={
				type:'todoist_taskcompletionhrsHTML',					
				taskid:taskid
			};

		$('#pageloader').show();
		$.ajax({
			type:'POST',
			url:'task_actions.php',
			data:data,
			success:function(output){

				$('#todoist_updatehrs_details').html(output);
				$('#pageloader').hide();
			} 
		});
	}

	function completetask(taskid,pid)
	{
		document.getElementById('taskid').value=taskid;
		document.getElementById('projid').value=pid;		

		var status=0;

		if($("#taskdiv-"+taskid).parent().hasClass("done")==true)
			status=0;
		else
			status=1;

		
		var data={
				type:'updTaskCompleteHrs',
				taskid:taskid,
				projid:pid,
				status:status
			};

		$.ajax({
			type:'POST',
			url:'task_actions.php',
			data:data,
			success:function(output){

				$("#taskdiv-"+taskid).parent().toggleClass("done");

				if(status==1)
					$('#cbk-'+taskid).prop('checked', true);
				else
					$('#cbk-'+taskid).prop('checked', false);

			}
		});
			

	}

	function deleteTask(id)
	{

		var res=confirm("Are you sure do you want to delete this?");

		if(res==true)
		{
			var data={
					type:'deletetask',					
					taskid:id
				};

			$('#pageloader').show();

			$('#taskdiv-'+id).parent().hide();

			$.ajax({
				type:'POST',
				url:'task_actions.php',
				data:data,
				success:function(output){
					//$('#complete_details').html(output);
					$('#pageloader').hide();
				} 
			});
		}

	}

	function deleteTaskCmtHrs(id,element)
	{
		var res=confirm("Are you sure do you want to delete this?");

		if(res==true)
		{
			var data={
					type:'deleteTaskCmtHrs',					
					rid:id
				};

			$('#pageloader').show();

			$(element).closest('tr').remove();

			$.ajax({
				type:'POST',
				url:'task_actions.php',
				data:data,
				success:function(output){

					$('#pageloader').hide();

				} 
			});
		}
	}

	$(document).ready(function(){

		
		$(".custom-select").each(function(){
			$(this).wrap("<span class='select-wrapper'></span>");
			$(this).after("<span class='holder'></span>");
		});

		$(".custom-select").change(function(){
			var selectedOption = $(this).find(":selected").text();
			$(this).next(".holder").text(selectedOption);
		}).trigger('change');

		$(".esthrs").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			     // Allow: Ctrl+A
			    (e.keyCode == 65 && e.ctrlKey === true) || 
			     // Allow: home, end, left, right
			    (e.keyCode >= 35 && e.keyCode <= 39)) {
				 // let it happen, don't do anything
				 return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			    e.preventDefault();
			}
		    });
		

		$('#file_upload_cmt').uploadifive({
			'auto'             : true,
			'formData'         : {
							   'timestamp' : '<?php echo $timestamp;?>',
							   'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'
				     },
			'queueID'          : 'queue1',
			'uploadScript'     : 'uploadifive.php?job=addToCommentsTable&tabname=task_comments',
			'queueSizeLimit'   : 5,
			'onProgress'   	   : function(file, e) {
				    if (e.lengthComputable) {
					var percent = Math.round((e.loaded / e.total) * 100);
				    }
				    file.queueItem.find('.fileinfo').html(' - ' + percent + '%');
				    file.queueItem.find('.progress-bar').css('width', percent + '%');
			}, 
			'onUploadComplete' : function(file, data) {

					$('#output1').append(data+',');

					document.getElementById('upimgids1').value = $('#output1').html();

					//file.queueItem.find('.close').html(data);
		
			}

		});

		
		$("#addComment").click(function(){

			var taskcomment=$("#taskcomment").val();
			var attachments=$("#upimgids1").val();
			var taskid=$("#taskid").val();
			var projid=$("#projid").val();
		
			if(taskcomment!="")
			{
				var data={
						type:'addTaskComment',					
						taskcomment:taskcomment,
						taskid:taskid,
						projid:projid,
						attachments:attachments
					};

				$.ajax({
					type:'POST',
					url:'task_actions.php',
					data:data,
					success:function(output){
						$("#taskcomment").val("");
						document.getElementById('upimgids1').value="";
						$("#queue1").val("");

						$('#file_upload_cmt').uploadifive('clearQueue');

					 	$('#output1').html("&nbsp;");

						var cur_cnt=$("#tcmt-"+taskid).text();
						cur_cnt++;
						$("#tcmt-"+taskid).text(cur_cnt);
						//$('#comments_details').html(output);
					}
				});
			}else{
				alert("Please fill task comments");
				return false;
			}
			
		});

		$("#colsecmt").click(function(){
			
			$('#file_upload_cmt').uploadifive('clearQueue');
			document.getElementById('upimgids1').value="";
			$('#output1').html("&nbsp;");

		});

		$("#cmphrs").click(function(){

			var complete_hrs=$("#hours_worked").val();
			var taskid=$("#ctaskid").val();
			var projid=$("#cprojid").val();
			var workdate=$("#workdate").val();
			var complete_hrs_desc=$("#complete_hrs_desc").val();
			
			if(complete_hrs!="")
			{
				var data={
						type:'updTaskCompleteHrs',					
						complete_hrs:complete_hrs,
						taskid:taskid,
						projid:projid,
						workdate:workdate,
						complete_hrs_desc:complete_hrs_desc
					};

				$.ajax({
					type:'POST',
					url:'task_actions.php',
					data:data,
					success:function(output){

						$("#hours_worked").val("");
						$("#workdate").val("");
						$("#complete_hrs_desc").val("");

					}
				});
			}else{
				alert("Please fill task completion hrs");
				return false;
			}
			
			
		});


		$("#todoist_cmphrs").click(function(){

			var hrs=$("#chrs_ddl").val();
			var mins=$("#cmins_ddl").val();
	
			var complete_hrs = parseFloat(hrs)+parseFloat(mins);
			
			var userid=$('#cusers_ddl').val();
			var taskid=$("#tctaskid").val();
			var projid=$("#tcprojid").val();
			var workdate=$("#tworkdate").val();
			var complete_hrs_desc=$("#tcomplete_hrs_desc").val();
			
			if(complete_hrs!="")
			{
				var data={
						type:'updTaskCompleteHrs',					
						complete_hrs:complete_hrs,
						userid:userid,
						taskid:taskid,
						projid:projid,
						workdate:workdate,
						complete_hrs_desc:complete_hrs_desc
					};

				$.ajax({
					type:'POST',
					url:'task_actions.php',
					data:data,
					success:function(output){

						$("#hours_worked").val("");
						$("#workdate").val("");
						$("#tcomplete_hrs_desc").val("");

					}
				});
			}else{
				alert("Please fill task completion hrs");
				return false;
			}
			
			
		});

		/*$("#cmptask").click(function(){

			var complete_hrs=$("#complete_hrs").val();
			var taskid=$("#taskid").val();
			var projid=$("#projid").val();
			
			var status=0;

			if($("#taskdiv-"+taskid).parent().hasClass("done")==true)
				status=0;
			else
				status=1;

			if(complete_hrs!="")
			{
				var data={
						type:'updTaskCompleteHrs',					
						complete_hrs:complete_hrs,
						taskid:taskid,
						projid:projid,
						status:status
					};

				$.ajax({
					type:'POST',
					url:'task_actions.php',
					data:data,
					success:function(output){
						$("#complete_hrs").val("");

						$("#taskdiv-"+taskid).parent().toggleClass("done");

						if(status==1)
							$('#cbk-'+taskid).prop('checked', true);
						else
							$('#cbk-'+taskid).prop('checked', false);

						//$('#comments_details').html(output);

					}
				});
			}
			
		});*/

		$('#workdate').datepicker({
			format: 'dd-mm-yyyy',
			"setDate": new Date(),
		        "autoclose": true
		});

		$('#workdate').on('changeDate', function(ev){
	  	  	$(this).datepicker('hide');
		  	
		});

		$(".cus_today").click(function(){
			$('#workdate').datepicker('setValue', today);
			$('#workdate').datepicker('hide');	
		});

		$(".cus_tomorrow").click(function(){	
			$('#workdate').datepicker('setValue', tomorrow);
			$('#workdate').datepicker('hide');
		});

		$(".cus_nextweek").click(function(){
			$('#workdate').datepicker('setValue', nextweek);
			$('#workdate').datepicker('hide');
		});
		
		// todoist task completions

		$("#thours_worked").timepicker({
                   
                });

		$('#tworkdate').datepicker({
			format: 'dd-mm-yyyy',
			"setDate": new Date(),
		        "autoclose": true
		});

		$('#tworkdate').on('changeDate', function(ev){
	  	  	$(this).datepicker('hide');
		  	
		});

		$(".cus_today").click(function(){
			$('#tworkdate').datepicker('setValue', today);
			$('#tworkdate').datepicker('hide');	
		});

		$(".cus_tomorrow").click(function(){	
			$('#tworkdate').datepicker('setValue', tomorrow);
			$('#tworkdate').datepicker('hide');
		});

		$(".cus_nextweek").click(function(){
			$('#tworkdate').datepicker('setValue', nextweek);
			$('#tworkdate').datepicker('hide');
		});

		$('#pageloader').hide();

		$('#addProject').click(function(){

			var defcolor="rgb(221, 221, 221)";

			$("#addprojnew").remove();
			$('#projlist').find(' > li:nth-last-child(1)').before('<li id="addprojnew" style="padding:8px;"><div class="input-group input-group-sm"><div class="input-group-btn"><button type="button" id="color-chooser-btn" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-circle"></span></button><ul class="dropdown-menu" id="color-chooser"><li><a class="text-green" href="#"><i class="fa fa-square"></i> Green</a></li><li><a class="text-blue" href="#"><i class="fa fa-square"></i> Blue</a></li> <li><a class="text-navy" href="#"><i class="fa fa-square"></i> Navy</a></li><li><a class="text-yellow" href="#"><i class="fa fa-square"></i> Yellow</a></li><li><a class="text-orange" href="#"><i class="fa fa-square"></i> Orange</a></li><li><a class="text-aqua" href="#"><i class="fa fa-square"></i> Aqua</a></li><li><a class="text-red" href="#"><i class="fa fa-square"></i> Red</a></li><li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i> Fuchsia</a></li><li><a class="text-purple" href="#"><i class="fa fa-square"></i> Purple</a></li></ul></div><input type="text" name="updprojname" placeholder="Projet name" value="" id="projname" class="form-control"><input type="hidden" name="projcolor" id="projcolor" value="'+defcolor+'"></div> <br><button type="button" id="saveproj" class="btn btn-danger">Add Project</button>&nbsp;&nbsp;<button type="button" id="cancelproj" class="btn btn-default">Cancel</button></div></li>');
			
			$("#projname").focus();

			var colorChooser = $("#color-chooser-btn");
			$("#color-chooser > li > a").click(function(e) {
			    e.preventDefault();
			    //Save color
			    currColor = $(this).css("color");
			  
			    $('#projcolor').val(currColor);

			    //Add color effect to button
			    colorChooser
				    .css({"background-color": currColor, "border-color": currColor})
				    .html('<span class="fa fa-circle"></span>');
			});

			//$(".addprojnew").show();
	
		});

		$(document).on('click', '.project_autogrow', function(){

			//console.log($('.project_autogrow'));

			var pname=$(this).attr('pname');
			var pid=$(this).attr('pid');

			var projclr=$('#projclr-'+pid).attr('val');

			var defcolor="rgb(221, 221, 221)";

			$("#editproj").remove();			

$('#projlist').find(' > li:nth-last-child(1)').before('<li id="editproj" style="padding:8px;"><div class="input-group input-group-sm"><div class="input-group-btn"><button type="button" id="color-chooser-btn" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span id="projclr-'+pid+'" val="'+projclr+'" style="color:'+projclr+'"class="fa fa-circle"></span></button><ul class="dropdown-menu" id="color-chooser"><li><a class="text-green" href="#"><i class="fa fa-square"></i> Green</a></li><li><a class="text-blue" href="#"><i class="fa fa-square"></i> Blue</a></li> <li><a class="text-navy" href="#"><i class="fa fa-square"></i> Navy</a></li><li><a class="text-yellow" href="#"><i class="fa fa-square"></i> Yellow</a></li><li><a class="text-orange" href="#"><i class="fa fa-square"></i> Orange</a></li><li><a class="text-aqua" href="#"><i class="fa fa-square"></i> Aqua</a></li><li><a class="text-red" href="#"><i class="fa fa-square"></i> Red</a></li><li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i> Fuchsia</a></li><li><a class="text-purple" href="#"><i class="fa fa-square"></i> Purple</a></li></ul></div><input type="text" name="updprojname" placeholder="Projet name" value="'+pname+'" id="updprojname" class="form-control"><input type="hidden" name="projid" id="projid" value="'+pid+'"><input type="hidden" name="projcolor" id="projcolor" value="'+defcolor+'"></div><br><button type="button" id="updproj" class="btn btn-danger">Save</button>&nbsp;&nbsp;<button type="button" id="cancelupdproj" class="btn btn-default">Cancel</button></li>');

			$("#updprojname").focus();

			 var colorChooser = $("#color-chooser-btn");
			$("#color-chooser > li > a").click(function(e) {
			    e.preventDefault();
			    //Save color
			    currColor = $(this).css("color");
			  
			    $('#projcolor').val(currColor);

			    //Add color effect to button
			    colorChooser
				    .css({"background-color": currColor, "border-color": currColor})
				    .html('<span class="fa fa-circle"></span>');
			});
			//$(".addprojnew").show();

	
		});

		 var colorChooser = $("#color-chooser-btn");
			$("#color-chooser > li > a").click(function(e) {
			    e.preventDefault();
			    //Save color
			    currColor = $(this).css("color");
			    //Add color effect to button
			    colorChooser
				    .css({"background-color": currColor, "border-color": currColor})
				    .html($(this).text()+' <span class="caret"></span>');
			});

		$('#projlist').on('click', '#cancelupdproj', function(){
			$('#updprojname').val("");
			$("#editproj").remove();
		});

		$('#projlist').on('click', '#updproj', function(){

			var projid=$('#projid').val();
			var projcolor=$('#projcolor').val();
			var updprojname=$('#updprojname').val();
			
			var data={
					type:'updProject',					
					pid:projid,
					projcolor:projcolor,
					projname:updprojname
				};
			
			var alinkval="";			
			$.each($(".project_autogrow"), function() {

				if($(this).hasClass("activeBtn")==true)
				{
					alinkval=$(this).attr("pid");

					if(alinkval==projid)
					{
						$(this).attr("pname", updprojname);
					}
				}
				
			});

			$('#spid-'+projid).html(updprojname);
			$('#projclr-'+projid).css({"color": projcolor});

			$('#pageloader').show();
			$.ajax({
				type:'POST',
				url:'project_actions.php',
				data:data,
				success:function(output){
					
					$('#updprojname').val("");
					$("#editproj").remove();
					$('#pageloader').hide();
				}
			});
			
		});

		$('#projlist').keypress(function(e) {

		    if(e.which == 13) {

			var projectaction=$("#projectaction").val();
		
			if(projectaction=="add")
			{
				saveProject();
			}			
			else if(projectaction=="edit")
			{
				
				$("#updproj").trigger("click");

			}
		   }

		});


		$('#projlist').on('click', '#saveproj', function(){

			saveProject();

		});

		saveProject = function()
		{
			var projname=$('#projname').val();
			var projcolor=$('#projcolor').val();

			if(projname!="")
			{
				$('#pageloader').show();
				var data={
						type:'addProject',					
						projectname:projname,
						projcolor:projcolor
					};

				$.ajax({
					type:'POST',
					url:'project_actions.php',
					data:data,
					success:function(output){
						var npid=output.trim();

						$('#projlist').find(' > li:nth-last-child(1)').before('<li id="li_'+npid+'" style="margin-left:10px;"><table cellpadding="3px" width="100%"><tr><td valign="top" width="5%"><i class="fa fa-circle" style="color:'+projcolor+'"></i></td><td valign="top" width="90%"><a href="#" id="pid-'+npid+'">'+projname+'</a></td><td valign="top" width="5%"><span style="cursor:pointer;" class="pull-right"><div class="btn-group"><button class="btn btn-default btn-flat dropdown-toggle" style="background-color: #F9F9F9; border: none;" data-toggle="dropdown" type="button"><span class="fa fa-ellipsis-h"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a style="margin-left:0px;" id="editproject" val="pid-'+npid+'" pname="'+projname+'" pid="'+npid+'" class="project_autogrow" href="#">Edit Project</a></li><li><a style="margin-left:0px;" id="shareproject" onclick="javascript:return shareproject('+npid+');" data-toggle="modal" data-target="#compose-modal" href="#">Share Project</a></li><li style="margin-left:0px;" class="divider"></li><li><a style="margin-left:0px;" href="#" onclick="javascript:deleteprojfun('+npid+');">Delete Project</a></li></ul></div></span></td></tr></table></li>');

						$('#projname').val("");
						$("#addprojnew").remove();
						$("#projectaction").val("add");

						var data={
								type:'getProjectTasks',			
								pid:npid
							};

						$.ajax({
							type:'POST',
							url:'project_actions.php',
							data:data,
							success:function(output){
								$('#pageloader').hide();
								$('.content').html(output);
								$("#addTaskContent").hide();
								$("#assignedtopersons").hide();
								$("#projectaction").val("add");
							}
				
						});
					},
					error:function(err){
						console.log(err);
					}
					
				
				});

			}else{
				alert("Please fill Project Name!");
				
			}
	
		};

		$('#projlist').on('click', '#cancelproj', function(){
			$('#projname').val("");
			$("#addprojnew").remove();
			$("#projectaction").val("add");			
		});

		$(document).on('click', '.edittaskshow span', function(e){
			e.stopPropagation();
			e.preventDefault();
		});

		


		//$("#projlist li a").click(function(){

		$(document).on('click', '#projlist li a', function(){

		 	pid = $(this).attr('id');
			//console.log(pid);
			if(pid=="addProject")
			{
				$("#projectaction").val("add");

			}else if(pid=="editproject")
			{
				$("#projectaction").val("edit");
				$(this).addClass("activeBtn");
			}
			else if(pid=="shareproject")
			{
			
			}
			else
			{
				$('#pageloader').show();

				var npid=pid.split("-"); 
				
				var data={
						type:'getProjectTasks',					
						pid:npid[1]
					};

				$.ajax({
					type:'POST',
					url:'project_actions.php',
					data:data,
					success:function(output){
						$('.content').html(output);
						
						todolist_sorting(pid);

						$('.viewtask').show();
						$('.edittask').hide();
			
						$("#assignedtopersons").hide();

						$('.duedate').datepicker({
							format: 'dd-mm-yyyy'
						});

						$('.duedate').on('changeDate', function(ev){
					  	  	$(this).datepicker('hide');
						  	
						});

						$(".cus_today").click(function(){
							$('.duedate').datepicker('setValue', today);
							$('.duedate').datepicker('hide');	
						});

						$(".cus_tomorrow").click(function(){	
							$('.duedate').datepicker('setValue', tomorrow);
							$('.duedate').datepicker('hide');
						});

						$(".cus_nextweek").click(function(){
							$('.duedate').datepicker('setValue', nextweek);
							$('.duedate').datepicker('hide');
						});
						
						$("#addTaskContent").hide();
						$('#pageloader').hide();
					}
				
				});
			}

		});

		$("#assignedtopersons").hide();

                $('.duedate').datepicker({
				format: 'dd-mm-yyyy'
			});
		
		$('.duedate').on('changeDate', function(ev){
	  	  $(this).datepicker('hide');
		});

		$(".cus_today").click(function(){
			$('.duedate').datepicker('setValue', today);
			$('.duedate').datepicker('hide');	
		});

		$(".cus_tomorrow").click(function(){	
			$('.duedate').datepicker('setValue', tomorrow);
			$('.duedate').datepicker('hide');
		});

		$(".cus_nextweek").click(function(){
			$('.duedate').datepicker('setValue', nextweek);
			$('.duedate').datepicker('hide');
		});
	
		$("#invitepeople").select2();

		$("#saveInvite").click(function(){
	
			var invitepeople=$("#invitepeople").val();
			var selectedpid=$("#selectedpid").val();

			var invemail=$('#invitepeople option:selected').html();

			$('#pageloader').show();

			var data={
				type:'inviteToProject',
				inviteuserid:invitepeople,
				selectedpid:selectedpid
			};

			$.ajax({
				url: "project_actions.php",
				type: 'POST',
				data: data,
				success:function(output){
					$('#pageloader').hide();

			$('#invitedpeoplelist tr:last').after('<tr><td rowspan="2" width="10%"><img src="assets/img/avatar3.png" width="80%" class="img-circle" alt="User Image"></td><td width="80%"> </td><td rowspan="2" width="10%">&nbsp;</td></tr><tr><td style="border-top:none;">'+invemail+'</td></tr>');

				}
			});			
				
		});

		$('.content').on('click', '#addTask', function(){
		
			$("#addTaskContent").show();
		
			$(".esthrs").keydown(function (e) {
				// Allow: backspace, delete, tab, escape, enter and .
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				     // Allow: Ctrl+A
				    (e.keyCode == 65 && e.ctrlKey === true) || 
				     // Allow: home, end, left, right
				    (e.keyCode >= 35 && e.keyCode <= 39)) {
					 // let it happen, don't do anything
					 return;
				}
				// Ensure that it is a number and stop the keypress
				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				    e.preventDefault();
				}
			    });

			$('.duedate').datepicker({
				format: 'dd-mm-yyyy'
			});
		
			$('.duedate').on('changeDate', function(ev){
		  	  	$(this).datepicker('hide');
			});

			$(".cus_today").click(function(){
				$('.duedate').datepicker('setValue', today);
				$('.duedate').datepicker('hide');	
			});

			$(".cus_tomorrow").click(function(){	
				$('.duedate').datepicker('setValue', tomorrow);
				$('.duedate').datepicker('hide');
			});

			$(".cus_nextweek").click(function(){
				$('.duedate').datepicker('setValue', nextweek);
				$('.duedate').datepicker('hide');
			});

		});

		$('.content').keypress(function(e) {
		    if(e.which == 13) {

			var taskaction=$("#taskaction").val();
		
			if(taskaction=="add")			
				saveTasks();
			else if(taskaction=="edit")
			{
				var curEditBtn="";			
				$.each($('.content .editupd'), function() {

					if($(this).hasClass("activeBtn")==true)
					{
						curEditBtn=this.id;
					}
					
				});

				$("#"+curEditBtn).trigger("click");

			}

		    }
		});
		
		$('.content').on('click', '#saveTask', function(){
			saveTasks();
		});

		saveTasks = function()
		{
			var taskname=$("#taskname").val();
			var esthrs=$("#esthrs").val();
			var duedate=$("#duedate").val();
			var pid=$("#projectid").val();

			if(taskname!="")
			{
				var data={
						type: 'addNewTask',
						taskname: taskname,
						esthrs: esthrs,	
						duedate: duedate,			
						pid: pid
					};

				$.ajax({
					type:'POST',
					url:'task_actions.php',
					data:data,
					success:function(output){
						//$('.content').html(output);
						//$('<li>', {html: output}).appendTo('ul#project_task_list');

						var outputCopy=output;

						$('textarea').autosize();

						var cur_size=$("ul#project_task_list li").size();
						
						var el = $(output).attr("seqid",cur_size);
						var lastLi = null;
						$("ul#project_task_list li").each(function(){
						    var currId = parseInt($('ul#project_task_list li').attr("seqid"));
						    lastLi = $(this);
						    if (currId > cur_size){
							el.insertBefore($(this));
							lastLi=null;
							return false;
						    }
						});

						if (lastLi){
						    el.insertAfter(lastLi);
						}else{
							$("ul#project_task_list").append(outputCopy); 
							$("ul#project_task_list li").first().attr("seqid","0");
							//console.log(outputCopy);	
						}

						$("#addTaskContent").show();
						$('#pageloader').hide();

						$('.viewtask').show();
						$('.edittask').hide();

						$("#taskname").val("");
						$("#esthrs").val("");
						$("#duedate").val("");
						$("#taskaction").val("add");

						todolist_sorting(pid);

						$('.duedate').datepicker({
							format: 'dd-mm-yyyy'
						});
		
						$('.duedate').on('changeDate', function(ev){
					  	  	$(this).datepicker('hide');
						});
						
						$(".cus_today").click(function(){
							$('.duedate').datepicker('setValue', today);
							$('.duedate').datepicker('hide');	
						});

						$(".cus_tomorrow").click(function(){	
							$('.duedate').datepicker('setValue', tomorrow);
							$('.duedate').datepicker('hide');
						});

						$(".cus_nextweek").click(function(){
							$('.duedate').datepicker('setValue', nextweek);
							$('.duedate').datepicker('hide');
						});
					}
				
				});

			}else{
				alert("Please Fill Task Name!");
			}
			


		};
		
		$('textarea').autosize();

		$('.content').on('click', '#cancelTask', function(){
		
			$("#taskname").val("");
			$("#esthrs").val("");
			$("#duedate").val("");

			$("#addTaskContent").hide();

		});

		$(document).on('click', '.edittask input', function(e){
			$(this).focus();
		});

		$(document).on('click', '.edittask textarea', function(e){
			$(this).focus();
		});

		$('.content').on('click', '.edittaskshow', function(){
		
		//$(document).on('click', '.edittaskshow', function(){
			
			var editidstr=this.id;
			//console.log(editidstr);

			var eid=editidstr.split("-");

			$("#te-"+eid[1]).show().focus();
			$("#tv-"+eid[1]).hide();

			$('.edittask').not("#te-"+eid[1]).hide();
			$('.viewtask').not("#tv-"+eid[1]).show();

			$('#updTask-'+eid[1]).addClass("activeBtn");

			$("#taskaction").val("edit");

			$("#taskname-"+eid[1]).focus();

			$('textarea').autosize();

			var pid=0;

			todolist_sorting(pid);

			$('.duedate').datepicker({
				format: 'dd-mm-yyyy'
			});

			$('.duedate').on('changeDate', function(ev){
		  	  	$(this).datepicker('hide');
			});

			$(".cus_today").click(function(){
				$('.duedate').datepicker('setValue', today);
				$('.duedate').datepicker('hide');	
			});

			$(".cus_tomorrow").click(function(){	
				$('.duedate').datepicker('setValue', tomorrow);
				$('.duedate').datepicker('hide');
			});

			$(".cus_nextweek").click(function(){
				$('.duedate').datepicker('setValue', nextweek);
				$('.duedate').datepicker('hide');
			});

		});

		$('.content').on('click', '.editcancel', function(){
		
			var editidstr=this.id;
			//console.log(editidstr);

			var eid=editidstr.split("-");

			$("#te-"+eid[1]).hide();
			$("#tv-"+eid[1]).show();
			
			$('#updTask-'+eid[1]).removeClass("activeBtn");

			$("#taskaction").val("add");

		});

		$('.content').on('click', '.editupd', function(){
		
			var editidstr=this.id;
			//console.log(editidstr);

			var eid=editidstr.split("-");

			$("#te-"+eid[1]).hide();
			$("#tv-"+eid[1]).show();

			var taskid = eid[1];
		
			var taskname=$("#taskname-"+eid[1]).val();
			var esthrs=$("#esthrs-"+eid[1]).val();
			var duedate=$("#duedate-"+eid[1]).val();
			var pid=$("#projectid").val();
			
			if(taskname!="")
			{

			$('#pageloader').show();
				var data={
						type: 'updTask',
						taskid:taskid,
						taskname: taskname,
						esthrs: esthrs,	
						duedate: duedate,			
						pid: pid
					};

				$.ajax({
					type:'POST',
					url:'task_actions.php',
					data:data,
					success:function(output){
						$('#taskdiv-'+taskid).html(output);
						//$('<li>', {html: output}).appendTo('ul#project_task_list');
						$("#addTaskContent").hide();
						$('#pageloader').hide();

						$('.viewtask').show();
						$('.edittask').hide();

						todolist_sorting(pid);

						$('textarea').autosize();

						$("#taskname").val("");
						$("#esthrs").val("");
						$("#duedate").val("");
						$('#pageloader').hide();
						$("#taskaction").val("add");

						$('.duedate').datepicker({
							format: 'dd-mm-yyyy'
						});
		
						$('.duedate').on('changeDate', function(ev){
					  	  	$(this).datepicker('hide');
						});

						$(".cus_today").click(function(){
							$('.duedate').datepicker('setValue', today);
							$('.duedate').datepicker('hide');	
						});

						$(".cus_tomorrow").click(function(){	
							$('.duedate').datepicker('setValue', tomorrow);
							$('.duedate').datepicker('hide');
						});

						$(".cus_nextweek").click(function(){
							$('.duedate').datepicker('setValue', nextweek);
							$('.duedate').datepicker('hide');
						});
					}
				
				});

			}else{
				alert("Please Fill Task Name!");
			}

		});


		$('.taskdiv').on('click', '.editupd', function(){

		$('#pageloader').show();
			var editidstr=this.id;
			//console.log(editidstr);

			var eid=editidstr.split("-");

			$("#te-"+eid[1]).hide();
			$("#tv-"+eid[1]).show();

			var taskid = eid[1];
		
			var taskname=$("#taskname-"+eid[1]).val();
			var esthrs=$("#esthrs-"+eid[1]).val();
			var duedate=$("#duedate-"+eid[1]).val();
			var pid=$("#projectid").val();
			
			if(taskname!="")
			{
				var data={
						type: 'updTask',
						taskid:taskid,
						taskname: taskname,
						esthrs: esthrs,	
						duedate: duedate,			
						pid: pid
					};

				$.ajax({
					type:'POST',
					url:'task_actions.php',
					data:data,
					success:function(output){
						$('#taskdiv-'+taskid).html(output);
						//$('<li>', {html: output}).appendTo('ul#project_task_list');
						$("#addTaskContent").hide();
						$('#pageloader').hide();

						$('.viewtask').show();
						$('.edittask').hide();

						todolist_sorting(pid);

						$("#taskname").val("");
						$("#esthrs").val("");
						$("#duedate").val("");
						$('#pageloader').hide();

						$('.duedate').datepicker({
							format: 'dd-mm-yyyy'
						});
		
						$('.duedate').on('changeDate', function(ev){
					  	  	$(this).datepicker('hide');
						});

						$(".cus_today").click(function(){
							$('.duedate').datepicker('setValue', today);
							$('.duedate').datepicker('hide');	
						});

						$(".cus_tomorrow").click(function(){	
							$('.duedate').datepicker('setValue', tomorrow);
							$('.duedate').datepicker('hide');
						});

						$(".cus_nextweek").click(function(){
							$('.duedate').datepicker('setValue', nextweek);
							$('.duedate').datepicker('hide');
						});
					}
				
				});

			}else{
				alert("Please Fill Task Name!");
			}

		});

		
		$('#comments_details').on('click', '.deletecomment', function(){ 

			var taskid=document.getElementById('taskid').value;

			var res=confirm("Are you sure do you want to delete this?");
	
			if(res==true)
			{			
				var sid=$(this).attr('id');

				var data={
						type: 'delComment',			
						sid: sid
					};

				$.ajax({
					type:'POST',
					url:'task_actions.php',
					data:data,
					success:function(output){
						var cur_cnt=$("#tcmt-"+taskid).text();
						cur_cnt--;
						$("#tcmt-"+taskid).text(cur_cnt);
					}
				});

				var tr = $(this).closest("tr");
				tr.add(tr.next()).add(tr.next().next()).remove();
			}

		});

		var todolist_sorting = function(pid)
		{
			
			//jQuery UI sortable for the todo list
			$(".todo-list").sortable({
				placeholder: "sort-highlight",
				handle: ".handle",
				forcePlaceholderSize: true,
				zIndex: 999999,
				receive: function( event, ui ) {
					//var sid = ui.item.attr('id').match(/seq_([0-9]+)/).pop();
				     	var index = ui.item.index();
					//console.log(" SID : "+sid+" Index : "+index);
				},
			   	update : function (event, ui) {

					var newOrder = $('#project_task_list').sortable('toArray').toString();

					var projectid=$("#projectid").val();
					
					var data={
						type:"sortorderTask",
						pid:projectid,
						displayorder:newOrder
					}
					$.ajax({
						type:"POST",
						url:"task_actions.php",
						data:data,
						success:function(output){
							
						}

					});
			    	}
			}).disableSelection();

		}

		$(document).on('click', '.ap', function(e){
			
			var cur_taskid=$(this).attr('taskid');
			var cur_pid=$(this).attr('pid');
	
			document.getElementById('cur_taskid').value=cur_taskid;
			document.getElementById('cur_pid').value=cur_pid;

			 $("#assignedtopersons").toggle('fast');
			 $("#assignedtopersons").offset({left:e.pageX-230,top:e.pageY+18});

		 });

		$(document).on('click', '.apn', function(e){
			
			var cur_taskid=$(this).attr('taskid');
			var cur_pid=$(this).attr('pid');
	
			document.getElementById('cur_taskid').value=cur_taskid;
			document.getElementById('cur_pid').value=cur_pid;
			
			$("#assignedtopersons").toggle('fast');
			$("#assignedtopersons").offset({left:e.pageX-230,top:e.pageY+18});

		 });

		$(document).on('click', '.asgninfo', function(e){
		
			var ctskid=$("#cur_taskid").val();
			var cpid=$("#cur_pid").val();

			var cur_usrimg=$(this).find('img').attr('src');
			var cur_usrasgnto=$(this).find('img').attr('assignedto');

			$(".ap[taskid~='"+ctskid+"']").attr('src',cur_usrimg);
			$(".ap[taskid~='"+ctskid+"']").attr('assignedto',cur_usrasgnto);
	
			//console.log(" Task Id : "+ctskid+" ; Proj Id : "+cpid+" ; Assigned To : "+cur_usrasgnto);			
	
			$("#assignedtopersons").toggle('fast');

			$('#pageloader').show();
			var data={
				type:"taskAssignTo",
				taskid:ctskid,
				pid:cpid,
				assignedto:cur_usrasgnto
			}
			$.ajax({
				type:"POST",
				url:"task_actions.php",
				data:data,
				success:function(output){
					$('#pageloader').hide();
				}

			});
			

		 });

		$(document).on('click', '#clsap', function(e){
		
			 $("#assignedtopersons").toggle('fast');

		 });

	});

// Integration with Todoist

	var todoist_token="<?php echo get_session('todoist_token'); ?>";

	if(!todoist_token)
	{
		var un="<?php echo get_session('VW_USER_EMAIL'); ?>";
		var pwd="<?php echo get_session('VW_USER_PWD'); ?>";

		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'http://todoist.com/API/login?email='+un+'&password='+pwd+'&callback=callback_userinfo';

		document.getElementsByTagName('head')[0].appendChild(script);

		function callback_userinfo(result){

			//console.log(result);

			var apitoken=result.api_token;
			var avatar_big=result.avatar_big;
			var avatar_medium=result.avatar_medium;
			var avatar_small=result.avatar_small;
			var todo_email=result.email;
			var full_name=result.full_name;
			var todo_userid=result.id;
			var inbox_project=result.inbox_project;
			var seq_no=result.seq_no;
			var timezone=result.timezone;
			var token=result.token;

			var data={
				type:"todoist_userinfos",
				apitoken:apitoken,
				avatar_big:avatar_big,
				avatar_medium:avatar_medium,
				avatar_small:avatar_small,
				todo_email:todo_email,
				full_name:full_name,
				todo_userid:todo_userid,
				inbox_project:inbox_project,
				seq_no:seq_no,
				token:token
			}
			$.ajax({
				type:"POST",
				url:"user_actions.php",
				data:data,
				success:function(output){
	
				}

			});

		}

	} // end of todoist_token -- if 

	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'http://todoist.com/API/getProjects?token='+todoist_token+'&callback=callback_projects';

	document.getElementsByTagName('head')[0].appendChild(script);

	function callback_projects(result){	

		//console.log(result);

		for(var i=0;i<result.length;i++)
		{
			var npid=result[i].id;
			var projname=result[i].name;
			var projcolor=result[i].color;

		 	$('ul#todoist_projlist').append('<li id="li_'+npid+'" style="margin-left:10px;"><table cellpadding="6px" width="100%"><tr><td valign="top" width="5%"><i class="fa fa-circle" style="color:'+projcolor+'"></i></td><td valign="top" width="90%"><a href="#" onclick="get_project_tasks('+npid+')" id="'+npid+'">'+projname+'</a></td><td valign="top" width="5%">&nbsp;</td></tr></table></li>');
			$('select#project').append('<option value="'+npid+'">'+projname+'</option>');


		}
		var myJsonString = JSON.stringify(result);
		var cd='';
		$("#proj").val(myJsonString);
		//$("#loader").css('display','block');
		$.ajax({
			    type: "POST",		
			    url: "get_todo_wreport.php",
			    data:{ name: myJsonString, type:'project', custom_date:cd},
			    success: function(result) { //alert(result);
		$("#loader").css('display','none');
		$('.ajax_table').html(result);
				}
		});

	}

	function get_project_tasks(pid)
	{

		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'http://todoist.com/API/getUncompletedItems?project_id='+pid+'&token='+todoist_token+'&callback=callback_project_tasks';

		document.getElementsByTagName('head')[0].appendChild(script);

	}

	function callback_project_tasks(result)
	{
		//console.log(result);

		var proj_tasks='<div class="box box-primary"><div class="box-header"><i class="ion ion-clipboard"></i><h3 class="box-title">Tasks</h3><div class="box-tools pull-right"></div></div><!-- /.box-header --><div class="box-body"><ul class="todo-list" id="todoist_project_task_list">';

		for(var i=0;i<result.length;i++)
		{
			var taskid=result[i].id;
			var taskcontent=result[i].content;
			var date_string=result[i].date_string;
			var due_date=result[i].due_date;
			var projectid=result[i].project_id;
			var assigned_to=result[i].responsible_uid;
			var avartar=result[i].responsible_uid;
			var pid=result[i].project_id;
			
			var task_item='<li><div class="viewtask" id="'+taskid+'"><table border="0" width="100%" class="table table-condensed"><tr><td width="5%" style="border-top:none;"><span class="handle"><i class="fa fa-th"></i></span></td><td width="75%" style="border-top:none;" valign="top"><span class="text edittaskshow" id="'+taskid+'" >'+taskcontent+' &nbsp;&nbsp;</span>&nbsp;<span style="opacity:0.5; cursor:pointer;" title="Completed Hours" onclick="javascript:return showtaskhrs_todoist('+taskid+','+projectid+');" data-toggle="modal" data-target="#todoist_updatehrs-modal"><i class="glyphicon glyphicon-dashboard"></i>&nbsp;</span> &nbsp;</span></td><td width="10%" style="border-top:none;" valign="top"><span>'+date_string+'</span>&nbsp;&nbsp;</td><td width="5%" style="border-top:none;" valign="top"> &nbsp; </td><td width="5%" style="border-top:none;" valign="top"> &nbsp; </td></tr></table></div></li>';

			/*<img class="img-circle ap" assignedto="'+assigned_to+'" pid="'+projectid+'" taskid="'+taskid+'" alt="User Image" src="'+assigned_to+'" width="60%"> <div class="tools"><i class="fa fa-trash-o" onclick="deleteTask('+taskid+');"></i></div>*/

			proj_tasks+=task_item;

		}

		proj_tasks+='</ul> </div><!-- /.box-body --> </div><!-- /.box -->';

		$('.content').html(proj_tasks);

	}
		
	</script>
    </body>
</html>
<?php

ob_end_flush();
?>
