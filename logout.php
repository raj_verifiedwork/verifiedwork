<?php
session_start();
$_SESSION = array();

foreach(array_keys($_SESSION) as $k) unset($_SESSION[$k]);

if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

unset($_SESSION['token']);
//$gClient->revokeToken();

session_destroy();
header("location: login.php");
exit;

?>
