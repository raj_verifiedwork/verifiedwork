<?php
ob_start();
include('genfunctions.php');

if(isset($_POST['go']))
{
	$refid=request_get('refid');
	$ref_email=request_get('ref_email');
	$passwordInput=request_get('passwordInput');
	$confirmPasswordInput=request_get('confirmPasswordInput');
	$subdomainname=request_get('subdomainname');

	$created=date('Y-m-d H:i:s');

	$user_sql="INSERT INTO `users` (`id`, `subdomainid`, `email`, `password`, `fname`, `lanme`, `nickname`, `show_name_options`, `login_with`, `timezone`, `status`, `last_login`, `mail_preference`, `photo`, `usertype`, `created`, `createdby`, `modified`, `modifiedby`) VALUES (NULL, '$refid', '$ref_email', '$passwordInput', '', '', '', '0', '0', 'timezone', '0', '', '0', '', '1', '$created', 'self', '$created', 'self')";	

	$res=mysql_query($user_sql)or die("ERROR : ".mysql_error());

	$userid=mysql_insert_id();

	/*Start Sending Email*/
	$emres=mysql_query("select * from email_templates where emailtype='__WELCOME_EMAIL__'") or die(mysql_error());
	$emr=mysql_fetch_array($emres);

	$subject=$emr['subject'];

	$email_username = $ref_email;

	$message=$emr['content'];

	$messageNew=html_entity_decode(str_replace("%username%",$email_username,$message));

	send_mail_with_smtp($useremail,$subject,$messageNew);
	/*End of Sending Email*/

	session_start();

	set_session('VW_USER_ID',$userid);
	set_session('VW_DOMAIN_ID',$refid);
	set_session('VW_USER_EMAIL',$ref_email);
	set_session('VW_USER_NAME',$ref_email);
	set_session('VW_DOMAIN_URL','test');
	set_session('VW_USER_LOGIN_WITH',0);

	$newurl="http://".$subdomainname.".verifiedwork.com/index.php?email=$ref_email";

	header("location:$newurl");
	exit();
	
}

?>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Verified Work :: Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/custom_styles.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
	
	<style>
	body{
		background: url("assets/img/noisebg.png") repeat scroll 0 0 #EEEEEE;
	}
	.form-box .body > .form-group > input, .form-box .footer > .form-group > input{
		border:1px solid #ddd;
	}
	.header1{
		background: #fff;
		border-radius: 4px 4px 0 0;
		padding:10px;
		font-weight:bold;
		display: inline-block;
		width: 100%;
	}

	.form_box_new{
	
		background: linear-gradient(to bottom, rgba(255, 255, 255, 0.15) 0%, rgba(0, 0, 0, 0.15) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
		border-right: 1px solid #989899;
		border-left: 1px solid #989899;
		border-top: 1px solid #989899;
		box-shadow: 0 -6px rgba(0, 0, 0, 0.35) inset, 0 -1px rgba(0, 0, 0, 0.15) inset, 0 5px 4px rgba(0, 0, 0, 0.12), 0 -7px 2px rgba(255, 255, 255, 0.8) inset;
		display: block;
		background: #fff;
		position: relative;
		text-decoration: none;
		text-shadow: 0 1px 0 #FFFFFF;

	}

	</style>

    </head>
    <body>
	<div style='position:absolute;z-index:-999;left:0;top:0;width:100%;height:100%'>
	  <img src='assets/img/coming_soon_bg.jpg' style='width:100%;height:100%' alt='VerifiedWork' />
	</div>

<form method="post">

	<div class="form-box form_box_new" id="CreateAccount">

<?php

$msg="";

// retrieve token
if (isset($_GET["token"]) && preg_match('/^[0-9A-F]{40}$/i', $_GET["token"])) {

	$token = $_GET["token"];
	$refid = $_GET["refid"];

	$sql="select * from `subdomain_list` where id='$refid' and status='2'";

	$res=mysql_query($sql)or die("ERR : ".mysql_error());

	$row=mysql_fetch_array($res);

	$created=$row['created'];
	$verified=date('Y-m-d H:i:s');

	$ref_email=$row['email'];

	$subdomainname=$row['subdomain'];

	$hourdiff = round((strtotime($verified) - strtotime($created))/3600,1);

	if($hourdiff<12)
	{
		
		if($token==$row['verification_code'])	
		{
			$upd_sql="update `subdomain_list` set status='0' where id='$refid' and status='2'";

			$res=mysql_query($upd_sql)or die("ERR : ".mysql_error());

			if($res){
				
			?>
	    <div class="header1"><img src="assets/img/vw_logo.png" width="30%" style="float:left;"/> <span style="float:right;">Set Password </span> </div>
            <form method="post">
                <div class="body bg-white">
			<input class="form-control" type="hidden" id="refid" name="refid" value="<?php echo $refid; ?>">
			<input class="form-control" type="hidden" id="ref_email" name="ref_email" value="<?php echo $ref_email; ?>">
		    <div class="form-group">
                        <?php echo '<div class="callout callout-info">'.__VERIFICATION_ACTIVATED__.'</div>'; ?>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
			<span class="input-group-addon">
			<i class="glyphicon glyphicon-lock"></i>
			</span>
			<input class="form-control" type="password" id="passwordInput" name="passwordInput" placeholder="Your Password" required >
			</div>
                    </div>
		    <div class="form-group">
                        <div class="input-group">
			<span class="input-group-addon">
			<i class="glyphicon glyphicon-lock"></i>
			</span>
			<input class="form-control" type="password" id="confirmPasswordInput" name="confirmPasswordInput" placeholder="Confirm Password" required >
			</div>
                    </div>
		    <div class="form-group">
                        <div class="" id="passwordStrength"></div>
                    </div>

                </div>
                <div class="footer">                    
		     <input class="form-control" type="hidden" name="regtype" value="0" >
		    <input type="hidden" name="subdomainname" value="<?php echo $subdomainname; ?>" />
                    <button type="submit" name="go" class="btn btn-danger">Go</button>

                </div>
            </form>

		<?php	}
			else{

				echo '<div class="callout callout-danger">'.__VERIFICATION_ERROR_ADMIN__.'</div>';

			}
		}else{
			echo '<div class="callout callout-danger">'.__VERIFICATION_ERROR__.'</div>';
		}
		
	}
	else{
		echo '<div class="callout callout-danger">'.__VERIFICATION_ERROR__.'</div>';

	}

}
else {
	echo '<div class="callout callout-danger">'.__VERIFICATION_ERROR__.'</div>';
}

//echo $msg;

?><br><br>
  </div>

</form>					
				

       <div class="pagecenter_div">
	
		   <div class="col-md-12">
                           
                        </div><!-- /.col -->
	</div>

        <!-- jQuery 2.0.2 -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>        
	<script type="text/javascript">
	$(document).ready(function(){

		$("#loginnow").click(function(){
			window.location.href="login.php";
		});

		$(".close").click(function(){
			window.location.href="login.php";
		});

		$(window).resize(function(){
		   var width = $(window).width();
		   if(width >= 320 && width <= 481){
		       $('#mydiv').removeClass('col-xs-5 col-xs-offset-5').addClass('col-xs-11 col-xs-offset-1');
		   }
		   else{
		       $('#mydiv').removeClass('col-xs-11 col-xs-offset-1').addClass('col-xs-5 col-xs-offset-5');
		   }
		}).resize();

		$('#passwordInput, #confirmPasswordInput').on('keyup', function(e) {

			if($('#passwordInput').val() == '' && $('#confirmPasswordInput').val() == '')
			{
			    $('#passwordStrength').removeClass().html('');

			    return false;
			}

			if($('#passwordInput').val() != '' && $('#confirmPasswordInput').val() != '' && $('#passwordInput').val() != $('#confirmPasswordInput').val())
			{
				$('#passwordStrength').removeClass().addClass('alert alert-error').html('Passwords do not match!');
				return false;
			}

			// Must have capital letter, numbers and lowercase letters
			var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");

			// Must have either capitals and lowercase letters or lowercase and numbers
			var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");

			// Must be at least 6 characters long
			var okRegex = new RegExp("(?=.{6,}).*", "g");

			if (okRegex.test($(this).val()) === false) {
			    // If ok regex doesn't match the password
				$('#passwordStrength').removeClass().addClass('alert alert-danger').html('Password must be 6 characters long.');

			} else if (strongRegex.test($(this).val())) {
			    // If reg ex matches strong password
			    $('#passwordStrength').removeClass().addClass('alert alert-success').html('Good Password!');
			} else if (mediumRegex.test($(this).val())) {
			    // If medium password matches the reg ex
			 $('#passwordStrength').removeClass().addClass('alert alert-info').html('Make your password stronger with more capital letters, more numbers and special characters!');
			} else {
			    // If password is ok
			    $('#passwordStrength').removeClass().addClass('alert alert-error').html('Weak Password, try using numbers and capital letters.');
			}

			return true;
		});
	

	});
	</script>
    </body>
</html>
<?php
ob_end_flush();
?>
