<?php

define("__COMPANY_NAME__", "Verified Work");

define("__VERIFICATION_ALERT__", "<h4>Verify your E-Mail</h4><p>Verification email send to your email id! <br>Please verify now!</p>");
define("__VERIFICATION_ERROR__", "<h4>Verification</h4><p>Your Token Expired or Already used!</p>");
define("__VERIFICATION_ERROR_ADMIN__", "<h4>Verification</h4><p>Error while activation contact admin regarding this! Thank you.</p>");
define("__VERIFICATION_ACTIVATED__", "<h4>Verification</h4><p>Your Account is Activated Successfully.<br>Set your password to proceed! </p>");
define("__USERNAME_EXISTS__", "Requested Account already exists!");

define("__INVITE_TO_PROJECTS__", "%sendusername% invited you to %projectname%");


?>
