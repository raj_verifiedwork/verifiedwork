<?php
session_start();
$vw_userid = get_session('VW_USER_ID');
$pageno=2;
$pagename=" Weekly Report ";

require_once('genfunctions.php');
require_once('vw_header.php');
require_once('vw_leftmenu.php');
	$sdate = date("Y-m-d",strtotime('monday this week'));
	$sd_date = $sdate;		
	$day_date = array($sd_date);
	for($i=1; $i<7; $i++)
	{
	$date1 = $sd_date;$date11 = str_replace('-', '/', $date1);
	$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
	array_push($day_date,$tomorrow );
	$sd_date = $tomorrow;
	}

?>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css">


<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
         <?php echo $pagename; ?>
       <!-- <small>it all starts here</small>-->
    </h1>
   <?php
	require_once('breadcrumb.php');
    ?>
</section>

    <br /><br />
  				<div class="form-group">
                                        <div class="input-group">
                                           Date range button: &nbsp;&nbsp;&nbsp; <input type="text" class="btn btn-default pull-right week_pick" id="daterange-btn">
                                        </div>
                                    </div><!-- /.form group -->
	  <br /><br />

<!-- Main content -->
<section class="content">
<?php 
$sql="select * from projects where userid='$vw_userid'";
$res=mysql_query($sql)or die("ERROR : ".mysql_error());
?>

<div class="box box-primary">
<div class="box-body table-responsive" id="table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Client</th>
                <th>Agency</th>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
		<th>Thu</th>
		 <th>Fri</th>
		<th>Sat</th>
		<th>Sun</th>
		<th>Total Hours</th>
            </tr>
        </thead>
        <tbody>
	<?php 
	$i=1;
	while($r=mysql_fetch_array($res))
	{
	$proj_name = $r['projectname'];
	$pid = $r['id'];
	
	$sql1 = mysql_query("SELECT SUM(hours) as tot_hours FROM `task_compeletion_hrs` where workdate BETWEEN '".$sdate. "'  AND '".$sd_date."' and userid='$vw_userid' and pid='$pid'");
	$row = mysql_fetch_array($sql1);	
	$hours = $row['tot_hours'];
	  echo "<tr>";
               echo "<td>".$proj_name."</td>";
                echo "<td>GR Info</td>";
               		 foreach($day_date as $weekdate){
			$sql1 = mysql_query("SELECT * FROM `task_compeletion_hrs` WHERE DATE(workdate) = '$weekdate' and userid='$vw_userid' and pid='$pid'"); 	
			$rows1= mysql_fetch_array($sql1);	
			echo "<td>".$rows1['hours']."</td>";
             		}
                echo "<td>".$hours."</td>";
           echo "</tr>";
		$i++;
	}
	?>
        </tbody>
     
    </table>
</div><!-- /.box-body -->
</div><!-- /.box -->


</section><!-- /.content -->
</aside><!-- /.right-side -->

<?php
	require_once('vw_footer.php');
?>
<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
    });
</script>

 
        <script type="text/javascript">
		Date.format = 'dd/mm/yyyy';
            $(function() {
               
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                'Last 7 Days': [moment().subtract('days', 6), moment()],
                                'Last 30 Days': [moment().subtract('days', 29), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                            },
                            startDate: moment().subtract('days', 29),
                            endDate: moment()
				
                        },
                function(start, end) {
                  // $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	       $('#reportrange span').html(start.format('dd/MM/yy') + ' - ' + end.format('dd/MM/yy'));
		getweek(start, end);
                }
		); 
		 function getweek(start, end)
		{
		var f=start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY');
		
		var x= $("#daterange-btn").val(); 
			$.ajax({
			    type: "POST",		
			    url: "get_weekreport.php",
			    data:{ name: x, type:'select'},
			    dataType: "html",
			    success: function(result) { 
		$('#table-responsive').html(result)
				}
		});
}	          
            });
        </script>
