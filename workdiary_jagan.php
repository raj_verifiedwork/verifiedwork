<?php
session_start();
$pageno=1;
$pagename=" Work Diary ";

require_once('genfunctions.php');
validate_login();

$subdomainid=get_session('VW_DOMAIN_ID');

require_once('vw_header.php');
require_once('vw_leftmenu.php');

?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $pagename; ?>
        <small id="pageloader"><img src="assets/img/ajax-page-loader.gif" /></small>
    </h1>
   <?php
	require_once('breadcrumb.php');
    ?>
</section>
<style>
.cusBorder{
/*	border:1px solid #ccc;*/

}
address {
    border: 1px solid #ccc;
}
.invoice{
	width:99%;
}

.logActive{
	border:1px solid #3c763d; 
	background-color:#3c763d;
	margin-right:1px;
	cursor:pointer;
}

.logInactive{
	border:1px solid #ccc;
	margin-right:1px;
	cursor:pointer;
}

.emptyLogs{
	border:1px dashed #ccc; 
	width:100%; 
	height:100%;
}

</style>
<!-- Main content -->
<section class="content invoice">

<?php
	//echo "<pre>"; echo var_dump($_SESSION); echo "</pre>";

	$userid=get_session('VW_USER_ID');

	$screenshotpath="/var/www/html/verifiedworkteam/vwteam/screenshots/";

	$sql="select * from projects where userid=$userid and status=0 ";
	$res=mysql_query($sql) or die("Projects : ".mysql_error());

	$projectid=0;
	$current_date=date('Y-m-d');

//, strtotime('-1 day')

	echo "<select id='projects' name='projects'>";
	echo "<option value='0'>Select Project</option>";
	while($r=mysql_fetch_array($res))
	{
		echo "<option value='".$r['id']."'>".$r['projectname']."</option>";
		$projectid=$r['id'];
	}
	echo "</select>";

	//echo $projectid;

	echo '<div class="row">
			<div class="col-xs-12">
			    <h2 class="page-header">
				<i class="fa fa-globe"></i> AdminLTE, Inc.
				<small class="pull-right">Date: 2/10/2014</small>
			    </h2>                            
			</div><!-- /.col -->
		</div>
		<div class="row invoice-info">
			<div class="col-xs-12 table-responsive">
				<table class="table table-bordered">';

				for($i=9;$i<24;$i++)
				{
					$st=$i.":00:00";
					$ed=($i+1).":00:00";

					$start_time=date('H:i:s',strtotime($st));
					$end_time=date('H:i:s',strtotime($ed));

					$start_time1=date('h a',strtotime($st));
					$end_time1=date('h:i a',strtotime($ed));

					$timeduration=$start_time1." - ".$end_time1;
					
					echo '<tr>';
					echo '<td width="10%"><b>'.$start_time1.'</b></td>';

				  		echo '<td width="14%">';

						$min1st=$i.":00:00";
						$min1ed=$i.":10:00";

	$sql="select * from time_tracking where userid='$userid' and projectid='$projectid' and log_date='$current_date' and log_starttime>='$min1st' and log_starttime<='$min1ed'";

	$res=mysql_query($sql) or die("TIME TRACK ERR : ".mysql_error());
	if(mysql_num_rows($res)>0)
	{
		$r=mysql_fetch_assoc($res);
		/*echo $r['status'];
		echo $r['minutes'];
		echo $r['total_key_strokes'];
		echo $r['total_mouse_clicks'];*/
	
		$activityLevel=$r['minutes'];

		$loginfo=$r['log_minutes_info'];

		$arr=json_decode($loginfo,true);
	
		echo "<table width='100%'>
				<tr>
					<td colspan='2'><img src='timetracking/4.png' width='100%' height='110px' /></td>
				</tr>
				<tr>
					<td>time</td>
					<td align='right'>";
				for($m=0;$m<count($arr);$m++)
				{
			$tooltip="Activity Level : ".$activityLevel.", Events : ".$arr[$m]['keycount']." Keyboard, ".$arr[$m]['mousecount']." Mouse";
					if($arr[$m]['keycount']==0 && $arr[$m]['mousecount']==0)
					{
						echo "<span class='logInactive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}else{
						echo "<span class='logActive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}
				}
					echo "</td>
				</tr>
		</table>";
	}else{
		echo "<div class='emptyLogs'><br><br><br><br><br><br><br></div>";
	}
		echo '</td>';
		echo '<td width="14%">';
			$min2st=$i.":10:00";
			$min2ed=$i.":20:00";

	$sql="select * from time_tracking where userid='$userid' and projectid='$projectid' and log_date='$current_date' and log_starttime>='$min2st' and log_starttime<='$min2ed'";

	$res=mysql_query($sql) or die("TIME TRACK ERR : ".mysql_error());
	if(mysql_num_rows($res)>0)
	{
		$r=mysql_fetch_assoc($res);
		/*echo $r['status'];
		echo $r['minutes'];
		echo $r['total_key_strokes'];
		echo $r['total_mouse_clicks'];*/
	
		$activityLevel=$r['minutes'];

		$loginfo=$r['log_minutes_info'];

		$arr=json_decode($loginfo,true);
	
		echo "<table width='100%'>
				<tr>
					<td colspan='2'><img src='timetracking/4.png' width='100%' height='110px' /></td>
				</tr>
				<tr>
					<td>time</td>
					<td align='right'>";
				for($m=0;$m<count($arr);$m++)
				{
			$tooltip="Activity Level : ".$activityLevel.", Events : ".$arr[$m]['keycount']." Keyboard, ".$arr[$m]['mousecount']." Mouse";
					if($arr[$m]['keycount']==0 && $arr[$m]['mousecount']==0)
					{
						echo "<span class='logInactive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}else{
						echo "<span class='logActive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}
				}
					echo "</td>
				</tr>
		</table>";
	}else{
		echo "<div class='emptyLogs'><br><br><br><br><br><br><br></div>";
	}

		echo '</td>
			<td width="14%">';

			$min3st=$i.":20:00";
			$min3ed=$i.":30:00";

	$sql="select * from time_tracking where userid='$userid' and projectid='$projectid' and log_date='$current_date' and log_starttime>='$min3st' and log_starttime<='$min3ed'";

	$res=mysql_query($sql) or die("TIME TRACK ERR : ".mysql_error());
	if(mysql_num_rows($res)>0)
	{
		$r=mysql_fetch_assoc($res);
		/*echo $r['status'];
		echo $r['minutes'];
		echo $r['total_key_strokes'];
		echo $r['total_mouse_clicks'];*/
	
		$activityLevel=$r['minutes'];

		$loginfo=$r['log_minutes_info'];

		$arr=json_decode($loginfo,true);
	
		echo "<table width='100%'>
				<tr>
					<td colspan='2'><img src='timetracking/4.png' width='100%' height='110px' /></td>
				</tr>
				<tr>
					<td>time</td>
					<td align='right'>";
				for($m=0;$m<count($arr);$m++)
				{
			$tooltip="Activity Level : ".$activityLevel.", Events : ".$arr[$m]['keycount']." Keyboard, ".$arr[$m]['mousecount']." Mouse";
					if($arr[$m]['keycount']==0 && $arr[$m]['mousecount']==0)
					{
						echo "<span class='logInactive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}else{
						echo "<span class='logActive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}
				}
					echo "</td>
				</tr>
		</table>";
	}else{
		echo "<div class='emptyLogs'><br><br><br><br><br><br><br></div>";
	}
	

						echo '</td>
							<td width="14%">';
			$min4st=$i.":30:00";
			$min4ed=$i.":40:00";

	$sql="select * from time_tracking where userid='$userid' and projectid='$projectid' and log_date='$current_date' and log_starttime>='$min4st' and log_starttime<='$min4ed'";

	$res=mysql_query($sql) or die("TIME TRACK ERR : ".mysql_error());
	if(mysql_num_rows($res)>0)
	{
		$r=mysql_fetch_assoc($res);
		/*echo $r['status'];
		echo $r['minutes'];
		echo $r['total_key_strokes'];
		echo $r['total_mouse_clicks'];*/
	
		$activityLevel=$r['minutes'];

		$loginfo=$r['log_minutes_info'];

		$arr=json_decode($loginfo,true);
	
		echo "<table width='100%'>
				<tr>
					<td colspan='2'><img src='timetracking/4.png' width='100%' height='110px' /></td>
				</tr>
				<tr>
					<td>time</td>
					<td align='right'>";
				for($m=0;$m<count($arr);$m++)
				{
			$tooltip="Activity Level : ".$activityLevel.", Events : ".$arr[$m]['keycount']." Keyboard, ".$arr[$m]['mousecount']." Mouse";
					if($arr[$m]['keycount']==0 && $arr[$m]['mousecount']==0)
					{
						echo "<span class='logInactive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}else{
						echo "<span class='logActive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}
				}
					echo "</td>
				</tr>
		</table>";
	}else{
		echo "<div class='emptyLogs'><br><br><br><br><br><br><br></div>";
	}
	
						echo '</td>
							<td width="14%">';

			$min5st=$i.":40:00";
			$min5ed=$i.":50:00";

	$sql="select * from time_tracking where userid='$userid' and projectid='$projectid' and log_date='$current_date' and log_starttime>='$min5st' and log_starttime<='$min5ed'";

	$res=mysql_query($sql) or die("TIME TRACK ERR : ".mysql_error());
	if(mysql_num_rows($res)>0)
	{
		$r=mysql_fetch_assoc($res);
		/*echo $r['status'];
		echo $r['minutes'];
		echo $r['total_key_strokes'];
		echo $r['total_mouse_clicks'];*/
	
		$activityLevel=$r['minutes'];

		$loginfo=$r['log_minutes_info'];

		$arr=json_decode($loginfo,true);
	
		echo "<table width='100%'>
				<tr>
					<td colspan='2'><img src='timetracking/4.png' width='100%' height='110px' /></td>
				</tr>
				<tr>
					<td>time</td>
					<td align='right'>";
				for($m=0;$m<count($arr);$m++)
				{
			$tooltip="Activity Level : ".$activityLevel.", Events : ".$arr[$m]['keycount']." Keyboard, ".$arr[$m]['mousecount']." Mouse";
					if($arr[$m]['keycount']==0 && $arr[$m]['mousecount']==0)
					{
						echo "<span class='logInactive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}else{
						echo "<span class='logActive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}
				}
					echo "</td>
				</tr>
		</table>";
	}else{
		echo "<div class='emptyLogs'><br><br><br><br><br><br><br></div>";
	}
	
			echo '</td>
				<td width="14%">';

			$min6st=$i.":50:00";
			$min6ed=$i.":59:59";

	$sql="select * from time_tracking where userid='$userid' and projectid='$projectid' and log_date='$current_date' and log_starttime>='$min6st' and log_starttime<='$min6ed'";

	$res=mysql_query($sql) or die("TIME TRACK ERR : ".mysql_error());
	if(mysql_num_rows($res)>0)
	{
		$r=mysql_fetch_assoc($res);
		/*echo $r['status'];
		echo $r['minutes'];
		echo $r['total_key_strokes'];
		echo $r['total_mouse_clicks'];*/
	
		$activityLevel=$r['minutes'];

		$loginfo=$r['log_minutes_info'];

		$arr=json_decode($loginfo,true);

	$showpopuptable="<table width=100% class=table>
				<tr><td colspan=2><img src=timetracking/4.png width=100% height=200px /></td></tr>
				<tr><td>Snapshot Taken</td><td>Snapshot time</td></tr>
				<tr><td>Memo</td><td>".$r['memo']."</td></tr>
				<tr><td>Active Window</td><td>Active Window text</td></tr>
				<tr><td>Total Keystrokes</td><td>".$r['total_key_strokes']."</td></tr>
				<tr><td>Total Mouse clicks</td><td>".$r['total_mouse_clicks']."</td></tr>
			</table>";

		echo "<table width='100%'>
				<tr>
					<td colspan='2'>

<div class='showpopup' data-container='body' data-toggle='popover' data-content='$showpopuptable'>
<img src='timetracking/4.png' width='100%' height='110px' />
</div>
					</td>
				</tr>
				<tr>
					<td>time</td>
					<td align='right'>";
				for($m=0;$m<count($arr);$m++)
				{
			$tooltip="Activity Level : ".$activityLevel.", Events : ".$arr[$m]['keycount']." Keyboard, ".$arr[$m]['mousecount']." Mouse";
					if($arr[$m]['keycount']==0 && $arr[$m]['mousecount']==0)
					{
						echo "<span class='logInactive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}else{
						echo "<span class='logActive' title='".$tooltip."' >&nbsp;&nbsp;</span>";
					}
				}
					echo "</td>
				</tr>
		</table>";
	}else{
		echo "<div class='emptyLogs'><br><br><br><br><br><br><br></div>";
	}
						echo '</td>';

					echo '</tr>';
				} // end of for loop
				echo '</table>

			</div>
		</div>';

?>

</section><!-- /.content -->
</aside><!-- /.right-side -->

<?php
	require_once('vw_footer.php');
?>
<script type="text/javascript">

$(document).ready(function(){

	$('.showpopup').popover({
		placement:'auto left',
		trigger: 'hover',
		html: true,
		template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
	});

});

</script>
