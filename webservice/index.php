<?php
require_once('dbcon.php');
require_once('genfunctions.php');

if(isset($_REQUEST['type']))
{

	if($_REQUEST['type']=="login")
	{
		$un=request_get('un');
		$pwd=request_get('pwd');

		header('content-type: application/json; charset=utf-8');

		$sql="SELECT * FROM `live_domain_users` WHERE email='".$un."' AND password='".$pwd."'";

		$res=mysql_query($sql) or die("SELECT USER ERR : ".mysql_error());

		if(mysql_num_rows($res)>0)
		{
			$completeUserInfo=array();

			$ur=mysql_fetch_assoc($res);

			$user=array();

			$user['uid']=safe_sql_nq($ur['id']);
			$user['email']=safe_sql_nq($ur['email']);
			$user['fullname']=safe_sql_nq($ur['fname'])." ".safe_sql_nq($ur['lname']);
			$user['subdomain']=safe_sql_nq($ur['subdomain']);
			$user['subdomainid']=safe_sql_nq($ur['subdomainid']);
			
			$user['photo']=safe_sql_nq($ur['photo']);

			if($user['photo']=="")
			{
				$user['photo']=safe_sql_nq("assets/images/noavatar.png");
			}

			$completeUserInfo['Users']=$user;

			$userid=$ur['id'];

			$usr_proj_sql="SELECT * FROM `user_projects` WHERE userid='$userid'";

			$usr_proj_res=mysql_query($usr_proj_sql) or die("SELECT USER PROJECT ERR : ".mysql_error());

			$upr=mysql_fetch_assoc($usr_proj_res);

			$uprids=explode(",",$upr['project_ids']);

			$projects=array();

			$tasks=array();

			$j=0;

			for($i=0;$i<count($uprids);$i++)
			{
				if($uprids[$i]!="")
				{
					$pid=$uprids[$i];

					$upr_sql="SELECT * FROM `projects` WHERE id='$pid'";

					$usr_proj_res1=mysql_query($upr_sql) or die("PROJECTS ERR : ".mysql_error());

					$upr1=mysql_fetch_assoc($usr_proj_res1);

					$projects[$i]['userid']=safe_sql_nq($userid);
					$projects[$i]['project_id']=safe_sql_nq($upr1['id']);
					$projects[$i]['project_name']=safe_sql_nq($upr1['projectname']);
					$projects[$i]['project_joinedpersons']=safe_sql_nq($upr1['joinedpersons']);
					$projects[$i]['project_status']=safe_sql_nq($upr1['status']);

					$projectid=$upr1['id'];

					$uptr_sql="SELECT * FROM `tasks` WHERE pid='$projectid' AND assigned_to='$userid'";

					$usr_task_res=mysql_query($uptr_sql) or die("TASKS ERR : ".mysql_error());

					while($uptr=mysql_fetch_assoc($usr_task_res))
					{
						$tasks[$j]['userid']=safe_sql_nq($userid);
						$tasks[$j]['projectid']=safe_sql_nq($projectid);
						$tasks[$j]['taskid']=safe_sql_nq($uptr['id']);
						$tasks[$j]['taskname']=safe_sql_nq($uptr['taskname']);
						$tasks[$j]['duedate']=date('d-m-Y H:i:s', strtotime($uptr['duedate']));
						$tasks[$j]['est_hrs']=safe_sql_nq($uptr['est_hrs']);
						$tasks[$j]['assigned_to']=safe_sql_nq($uptr['assigned_to']);
						$tasks[$j]['status']=safe_sql_nq($uptr['status']);
		
						$j++;
					}

				}
			}

			$completeUserInfo['Projects']=$projects;
			$completeUserInfo['Tasks']=$tasks;

			$params = array("status"=>"Ok","userinfo"=>$completeUserInfo);
	
			$json = json_encode($params,true);

			echo isset($_GET['callback']) ? "{$_GET['callback']}($json)" : $json;
		}
		else{
	
			$params = array("status"=>"Err","msg"=>"INVALID USER/PASSWORD - USER NOT FOUND");

			//echo $_GET['callback'] . '('.json_encode($params,true).')';
	
			$json = json_encode($params,true);

			echo isset($_GET['callback']) ? "{$_GET['callback']}($json)" : $json;
		}

	}

}

?>
