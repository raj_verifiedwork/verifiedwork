<?php
session_start();
$pageno=4;
$pagename=" Task Completion Hours ";

require_once('genfunctions.php');
validate_login();

$subdomainid=get_session('VW_DOMAIN_ID');

$cur_user_id=get_session('VW_USER_ID');
$cur_domain_id=get_session('VW_DOMAIN_ID');
$cur_username=get_session('VW_USER_NAME');
$cur_email=get_session('VW_USER_EMAIL');

require_once('vw_header.php');
require_once('vw_leftmenu.php');

?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $pagename; ?>
        <small id="pageloader"><img src="assets/img/ajax-page-loader.gif" /></small>
    </h1>
   <?php
	require_once('breadcrumb.php');
    ?>
</section>

<!-- Main content -->
<section class="content">

<div class="row">
<!-- left column -->
<div class="col-md-7">
    <!-- general form elements -->
    <div class="box box-primary">
       <!-- <div class="box-header">
            <h3 class="box-title">Quick Example</h3>
        </div> /.box-header -->
        <!-- form start -->
        <form role="form" method="post" >
            <div class="box-body">
                  <div class="form-group">
                    <select class="form-control" id="project_name" name="project_name" >
                        <option value="0">Select Project</option>
			<?php

			$mgres=mysql_query("SELECT * FROM user_projects WHERE userid='$cur_user_id' ORDER BY id ASC")or die(mysql_error());
			$mgr=mysql_fetch_array($mgres);

			$userprojectids=$mgr['project_ids'];

			$pids=explode(',',$userprojectids);

			for($i=0;$i<count($pids);$i++)
			{
				if($pids[$i]!="")
				{

					$pid=$pids[$i];
					$pres=mysql_query("SELECT projectname FROM projects WHERE id='$pid' ORDER BY id ASC")or die(mysql_error());
					$pr=mysql_fetch_array($pres);
					$pname=$pr['projectname'];

					echo "<option value='$pids[$i]'>".$pname."</option>"; 

				}
	
			}
			?>
                    </select>
                </div>
		<div class="form-group">
                    <select class="form-control" id="task_name" name="task_name" >
                        <option value="0">Select Task</option>
                    </select>
                </div>
		<div class="form-group">
                    <input type="text" class="form-control" id="hours" name="hours" placeholder="Hours" maxlength="5" required >
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
		<button type="submit" name="submit" class="btn btn-primary ">Submit</button>
            </div>
        </form>
    </div><!-- /.box -->
</div>
<div class="col-md-5">


</div>

</div>


</section><!-- /.content -->
</aside><!-- /.right-side -->

<?php
	require_once('vw_footer.php');

	if(isset($_POST['submit']))
	{
		$pid=request_get('project_name');		
		$taskid=request_get('task_name');
		$hours=request_get('hours');

		$created=date('Y-m-d H:i:s');
		$cur_username=get_session('VW_USER_NAME');

		$sql="INSERT INTO `task_compeletion_hrs` ( `id` , `pid` , `taskid` , `hours` , `created` , `createdby` , `modified` , `modifiedby` ) VALUES ( NULL , '$pid', '$taskid', '$hours', '$created', '$cur_username', '$created', '$cur_username' )";

		$res=mysql_query($sql) or die("ERROR : ".mysql_error());

	}

?>

<!-- Page specific script -->
<script type="text/javascript">

$(function() {

	$("#hours").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		     // Allow: Ctrl+A
		    (e.keyCode == 65 && e.ctrlKey === true) || 
		     // Allow: home, end, left, right
		    (e.keyCode >= 35 && e.keyCode <= 39)) {
			 // let it happen, don't do anything
			 return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		    e.preventDefault();
		}
	});
		

	$("#project_name").change(function(){

		var pid=$(this).val();

		if(pid!=0)
		{
			var data={
					type:'getTasksByProjectId',					
					pid:pid
				};
			$('#pageloader').show();
			$.ajax({
				type:'POST',
				url:'project_actions.php',
				data:data,
				success:function(output){
				
					$('#task_name').html(output);
					$('#pageloader').hide();
					//window.location.reload();
				} 
			});

		}else{
			alert("Please select any Project!");
		}

	});

});

</script>
