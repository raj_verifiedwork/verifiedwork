<?php

function fmt_db_date_time($time_var=0)
{
	if ($time_var==0) {
		$time_var = time();
	}
	return date('Y-m-d H:i:s',$time_var);
}

function request_get($param, $type = 1, $default_value = "") {
	if($type == 1){
		if(isset($_REQUEST[$param]) && trim($_REQUEST[$param]) != "") 
			return htmlentities(addslashes(trim($_REQUEST[$param])));
	}
	if($type == 2){
		return $_REQUEST[$param];
	}
	if($default_value !== "")
		return $default_value;
	
	return "";
}

function get_session($var)
{
	if (isset($_SESSION[$var])) {
		return $_SESSION[$var];
	}
	else {
		return '';
	}
}

function set_session($var,$value)
{
	$_SESSION[$var] = $value;
}

function unset_session($var)
{
	if(isset($_SESSION[$var])) {
		unset($_SESSION[$var]);
	}
}

function safe_sql_nq( $val )
{
	  $val = addslashes( htmlentities( $val, ENT_QUOTES, 'UTF-8') );
	  return $val;
}

function display_time_diff_format($var_date,$span_flag=0)
{
	$str_datetime = "";
	if ($var_date > 100) {
		$dateDiff = time() - $var_date;
		$fullDays = floor($dateDiff/(60*60*24));
		$fullHours = floor(($dateDiff-($fullDays*60*60*24))/(60*60));
		$fullMinutes = floor(($dateDiff-($fullDays*60*60*24)-($fullHours*60*60))/60);
		$fullSeconds = $dateDiff%60;
		
		if($fullDays == 0 && $fullHours == 0 && $fullMinutes == 0) {
			$str_datetime = $fullSeconds ." seconds ago";
		}
		else if($fullDays == 0 && $fullHours == 0) {
			$str_datetime = $fullMinutes ." minutes ago";
		}
		else if($fullDays == 0 ) {
			$str_datetime = $fullHours ." hours ago";
		}
		else if( $fullDays<30 ) {
			$str_datetime = $fullDays ." days ago";
		}
		else {
			$str_datetime = date("M j, Y H:i A", $var_date);
		}
		
		if ($span_flag == true)
		{
			$str_datetime_span_disp = date("M j, Y H:i A", $var_date);
			$temp = "<span title='$str_datetime_span_disp'>$str_datetime</span>";
			$str_datetime = $temp;
		}
		return $str_datetime;
	}
	else {
		return "--";
	}
}

?>
