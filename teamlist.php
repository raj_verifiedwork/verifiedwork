<?php
session_start();
$pageno=5;
$pagename=" Team List ";

require_once('genfunctions.php');
validate_login();

$subdomainid=get_session('VW_DOMAIN_ID');
$userid=get_session('VW_USER_ID');

require_once('vw_header.php');
require_once('vw_leftmenu.php');

if(isset($_POST['createnewuser']))
{
	$doid=get_session('VW_DOMAIN_ID');
	$ref_email=request_get('useremail');

	$created=date('Y-m-d H:i:s');

	$token = sha1(uniqid($subdomainid, true));

	$user_sql="INSERT INTO `users` (`id`, `subdomainid`, `email`, `password`, `fname`, `lanme`, `nickname`, `show_name_options`, `login_with`, `timezone`, `status`, `last_login`, `mail_preference`, `photo`, `usertype`, `user_verification_code`, `created`, `createdby`, `modified`, `modifiedby`) VALUES (NULL, '$doid', '$ref_email', '', '', '', '', '0', '0', 'timezone', '2', '', '0', '', '0', '$token', '$created', 'admin', '$created', 'admin')";	

	$res=mysql_query($user_sql)or die("ERROR : ".mysql_error());

	$usrefid=mysql_insert_id();

	$emres=mysql_query("select * from email_templates where emailtype='__INVITE_TEAM_MEMBER__'") or die(mysql_error());
	$emr=mysql_fetch_array($emres);

	$subject=$emr['subject'];

	$url = "http://verifiedwork.com/team_activate.php?token=$token&doid=$doid&uid=$usrefid";

	$message=$emr['content'];

	$messageNew=str_replace("%url%",$url,$message);

	send_mail_with_smtp($useremail,$subject,$messageNew);
	
}

?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $pagename; ?>
        <small id="pageloader"><img src="assets/img/ajax-page-loader.gif" /></small>
    </h1>
   <?php
	require_once('breadcrumb.php');
    ?>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
<!-- left column -->
<div class="col-md-8">
<?php
$sql="SELECT * FROM `users` where `subdomainid`='$subdomainid'";
$res=mysql_query($sql)or die("ERROR : ".mysql_error());
	$sql1="SELECT * FROM `users` where `id`='$userid'";
	$res1=mysql_query($sql1)or die("ERROR : ".mysql_error());
	$r1=mysql_fetch_array($res1);
	$admin=$r1['usertype'];
?>
<div class="box box-primary">
<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>S.No</th>
                <th>E-mail</th>
		<?php 
		 if($admin==1) { ?>
		<th>Action</th>
		 <?php } ?> 
            </tr>
        </thead>
        <tbody>
	<?php 
	$i=1;
	while($r=mysql_fetch_array($res))
	{
		$usertype="";
		if($r['usertype']==0)
			$usertype='<small class="label label-info">User</small>';
		else if($r['usertype']==1)
			$usertype='<small class="label label-primary">Admin</small>';

		$userstatus="";
		if($r['status']==0)
			$userstatus='<small class="badge pull-right bg-green">Active</small>';
		else if($r['status']==1)
			$userstatus='<small class="badge pull-right bg-red">In-Active</small>';
		else if($r['status']==2)
			$userstatus='<small class="badge pull-right bg-navy">Pending</small>';
		else if($r['status']==3)
			$userstatus='<small class="badge pull-right bg-yellow">Blocked</small>';
		else
			$userstatus='<small class="badge pull-right bg-navy">Deleted</small>';

		$uid=$r['id'];
	?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $r['email']; echo "&nbsp;&nbsp;".$usertype; echo "&nbsp;&nbsp;".$userstatus; ?></td>
		<?php 
	 	if($admin==1) { ?>
		 <td>
			<?php 
				if($r['usertype']==0)
					echo " <a href='#' title='Delete' onclick='javascript: deleteuser(".$uid.");'><i class='glyphicon glyphicon-trash'></i></a> "; 
				else if($r['usertype']==1)
					echo " &nbsp; ";
		 	?>
		</td> <?php } ?>
            </tr>
	<?php
		$i++;
	}
	?>
        </tbody>
       <!-- <tfoot>
            <tr>
                <th>Rendering engine</th>
                <th>Browser</th>
                <th>Platform(s)</th>
                <th>Engine version</th>
                <th>CSS grade</th>
            </tr>
        </tfoot>-->
    </table>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div>
<!-- right column -->
<div class="col-md-4">
    <!-- general form elements -->
    <div class="box box-primary">
	<div class="box-header">
            <h3 class="box-title">Invite New User</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address &nbsp; <span id="user-result"></span></label>
                    <input type="email" name="useremail" id="useremail" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required>
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" name="createnewuser" class="btn btn-primary">Invite</button>
            </div>
        </form>
    </div><!-- /.box -->
</div>
</div>
<?php
	echo "<pre style='font-size:10px;'>"; echo var_dump($_SESSION); echo "</pre>";
?>
</section><!-- /.content -->
</aside><!-- /.right-side -->

<?php
	require_once('vw_footer.php');
?>
<script>

function deleteuser(uid){

	var res=confirm("Are you sure do you want to delete this?");

	if(res==true)
	{
		$('#pageloader').show();
		var data={
			type:'delTeamUser',
			uid:uid	
		};

		$.ajax({
			type:'POST',
			url:'user_actions.php',
			data:data,
			success:function(output){
				$('#pageloader').hide();
				window.location.reload();
			}
		});

		$("#li_"+pid).remove();
	}
	
}

function ValidateEmail(email) {

        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);

};

$(document).ready(function(){

	$("#useremail").keyup(function (e) {
		//removes spaces from username
		$(this).val($(this).val().replace(/\s/g, ''));
	});

	$("#useremail").blur(function (e) {

		//removes spaces from username
		$(this).val($(this).val().replace(/\s/g, ''));

		var useremail = $(this).val();
		if(useremail.length < 2){$("#user-result").html('');return;}

		if (!ValidateEmail(useremail)) {
			$("#user-result").html('<img src="assets/img/not-available.png" width="18px" > <span style="color:red;">Invalid Email ID!');
		}
		else {
			
			if(useremail.length >= 2){
				$("#user-result").html('<img src="assets/img/ajax-page-loader.gif" width="18px" >');
				$.post('user_actions.php', { 'type':'checkuserexist', 'useremail':useremail }, function(data) {
					$("#user-result").html(data);
				});
			}

		}

	});

});
</script>
