<?php
########## Google Settings.. Client ID, Client Secret from https://cloud.google.com/console #############
$google_client_id 	= '285619102547-k7b1iuqn6trh3eqojve1mknjkjq7uv9g.apps.googleusercontent.com';
$google_client_secret 	= 'LGxx4Zvq94OwpcRMYE8_Fh8f';
$google_redirect_url 	= 'http://verifiedwork.com/login.php?signup=gmail'; //path to your script
$google_developer_key 	= '285619102547-k7b1iuqn6trh3eqojve1mknjkjq7uv9g@developer.gserviceaccount.com';

//include google api files
require_once 'auth/google/Google_Client.php';
require_once 'auth/google/contrib/Google_Oauth2Service.php';

//start session
session_start();

$gClient = new Google_Client();
$gClient->setApplicationName('Login to verifiedwork.com');
$gClient->setClientId($google_client_id);
$gClient->setClientSecret($google_client_secret);
$gClient->setRedirectUri($google_redirect_url);
$gClient->setDeveloperKey($google_developer_key);

$google_oauthV2 = new Google_Oauth2Service($gClient);

$authUrl="";

//If user wish to log out, we just unset Session variable
if (isset($_REQUEST['reset'])) 
{
  unset($_SESSION['google_token']);
  $gClient->revokeToken();
  header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL)); //redirect user back to page
}


//If code is empty, redirect user to google authentication page for code.
//Code is required to aquire Access Token from google
//Once we have access token, assign token to session variable
//and we can redirect user back to page and login.
if (isset($_GET['code'])) 
{ 
	$gClient->authenticate($_GET['code']);
	$_SESSION['google_token'] = $gClient->getAccessToken();
	header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
	return;
}

if (isset($_SESSION['google_token'])) 
{ 
	$gClient->setAccessToken($_SESSION['google_token']);
}

if ($gClient->getAccessToken()) 
{
	  //For logged in user, get details from google using access token
	  $user 		= $google_oauthV2->userinfo->get();
	  $user_id 		= $user['id'];
	  $user_name 		= filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
	  $email 		= filter_var($user['email'], FILTER_SANITIZE_EMAIL);
	 // $profile_url 	= filter_var($user['link'], FILTER_VALIDATE_URL);
	  $profile_image_url 	= filter_var($user['picture'], FILTER_VALIDATE_URL);
	  $personMarkup 	= "$email<div><img src='$profile_image_url?sz=50'></div>";
	  $_SESSION['google_token'] 	= $gClient->getAccessToken();

	/*echo "<pre>";
	var_dump($user);
	echo "</pre>";*/

	//echo "session active";

}
else 
{
	//For Guest user, get google login url
	$authUrl = $gClient->createAuthUrl();
}
//echo $authUrl;
?>
