<?php
session_start();
require_once('genfunctions.php');

if(isset($_REQUEST['type']))
{
	$cur_user_id=get_session('VW_USER_ID');
	$cur_domain_id=get_session('VW_DOMAIN_ID');
	$cur_username=get_session('VW_USER_NAME');
	$cur_email=get_session('VW_USER_EMAIL');

	//check if its ajax request, exit script if its not
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
		die();
	}

	if($_REQUEST['type']=="checkuserexist")
	{
		//trim and lowercase username
		$useremail =  request_get('useremail'); 
	
		//sanitize username
		$useremail = filter_var($useremail, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH);
	
		//check username in db
		$results = mysql_query("SELECT id FROM users WHERE email='$useremail' and subdomainid='$cur_domain_id'");
	
		//return total count
		$username_exist = mysql_num_rows($results); //total records
	
		//if value is more than 0, username is not available
		if($username_exist) {
			die('<img src="assets/img/not-available.png" width="18px" />');
		}else{
			die('<img src="assets/img/available.png" width="18px" />');
		}
	}

	if($_REQUEST['type']=="delTeamUser")
	{
		$userid=request_get('uid');

		$sql=mysql_query("DELETE FROM `users` WHERE id='$userid'") or die("ERROR : ".mysql_error());
	}

	if($_REQUEST['type']=="todoist_userinfos")
	{
		$apitoken=request_get('apitoken');
		$avatar_big=request_get('avatar_big');
		$avatar_medium=request_get('avatar_medium');
		$avatar_small=request_get('avatar_small');
		$todo_email=request_get('todo_email');
		$full_name=request_get('full_name');
		$todo_userid=request_get('todo_userid');
		$inbox_project=request_get('inbox_project');
		$seq_no=request_get('seq_no');
		$token=request_get('token');

		set_session('apitoken',$apitoken);
		set_session('avatar_big',$avatar_big);
		set_session('avatar_medium',$avatar_medium);
		set_session('avatar_small',$avatar_small);
		set_session('todo_email',$todo_email);
		set_session('full_name',$full_name);
		set_session('todo_userid',$todo_userid);
		set_session('inbox_project',$inbox_project);
		set_session('seq_no',$seq_no);
		set_session('todoist_token',$token);

	}

	if($_REQUEST['type']=="todoist_login")
	{
		$email=request_get('todo_email');

		$sql="SELECT * FROM `live_domain_users` where email='$email' and status='0' and login_with='4'";

		$ures=mysql_query($sql) or die("ERROR : ".mysql_error());
	
		if(mysql_num_rows($ures)>0)
		{
			//echo "login success";

			if(mysql_num_rows($ures)==1)
			{
				$apitoken=request_get('apitoken');
				$avatar_big=request_get('avatar_big');
				$avatar_medium=request_get('avatar_medium');
				$avatar_small=request_get('avatar_small');
				$todo_email=request_get('todo_email');
				$full_name=request_get('full_name');
				$todo_userid=request_get('todo_userid');
				$inbox_project=request_get('inbox_project');
				$seq_no=request_get('seq_no');
				$token=request_get('token');
				$p=request_get('p');

				set_session('apitoken',$apitoken);
				set_session('avatar_big',$avatar_big);
				set_session('avatar_medium',$avatar_medium);
				set_session('avatar_small',$avatar_small);
				set_session('todo_email',$todo_email);
				set_session('full_name',$full_name);
				set_session('todo_userid',$todo_userid);
				set_session('inbox_project',$inbox_project);
				set_session('seq_no',$seq_no);
				set_session('todoist_token',$token);

				$sr=mysql_fetch_array($ures);

				set_session('VW_USER_ID',$sr['id']);
				set_session('VW_DOMAIN_ID',$sr['domainid']);
				set_session('VW_USER_EMAIL',$sr['email']);
				set_session('VW_USER_NAME',$sr['email']);
				set_session('VW_USER_PWD',$sr['password']);
				set_session('VW_DOMAIN_URL',$sr['subdomain']);
				set_session('VW_USER_LOGIN_WITH',$sr['login_with']);

				$subdomainname=$sr['subdomain'];

				$newurl="http://".$subdomainname.".verifiedwork.com/index.php?email=$email&p=$p";

				echo $newurl;
				
			}
		}
	}

}
?>
