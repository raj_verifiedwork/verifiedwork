<?php
require_once('dbcon.php');
require_once('genfunctions.php');

if(isset($_REQUEST['type']))
{

	if($_REQUEST['type']=="start")
	{
		$handle = fopen ("/path/to/file.txt", "w+");
		fclose($handle);
	}


	if($_REQUEST['type']=="logtime")
	{
		$uid=request_get('uid');
		$pid=request_get('pid');
		$tid=request_get('tid');
		$logtimedetails=$_REQUEST['logtimedetails'];
		$memo=request_get('memo');
		$screenshot=request_get('screenshot');
		$activewindow=request_get('activewindow');

		/*echo " userid : ".$uid." <br>";
		echo " project : ".$pid." <br>";
		echo " task : ".$tid." <br>";
		echo " Log Time : ";
		echo var_dump($logtimedetails);
		echo " <br>";
		echo " memo : ".$memo." <br>";*/

		//$logLastTenMinutes=array("status"=>1,"hours"=>$completeUserInfo);

		$logtimes=json_decode($logtimedetails,true);

		$minActive=0;
		$status=0;

		$total_key_strokes=0;
		$total_mouse_clicks=0;

		$log_date="";
		$log_starttime="";
		$log_endtime="";

		for($i=0;$i<count($logtimes);$i++)
		{
			if($logtimes[$i]['keycount']!="0" || $logtimes[$i]['mousecount']!="0")
			{
				$minActive++;
				$total_key_strokes+=$logtimes[$i]['keycount'];
				$total_mouse_clicks+=$logtimes[$i]['mousecount'];
			}
		}

		$first_logtimes = reset($logtimes); // get the first element in array
    		$last_logtimes = end($logtimes); // get the last element in array

		$log_date=date('Y-m-d', strtotime($first_logtimes['datetime']));

		$log_starttime=date('H:i:s', strtotime($first_logtimes['datetime']));
		$log_endtime=date('H:i:s', strtotime($last_logtimes['datetime']));

		if($minActive>0)
			$status=1;
		else
			$status=0;

		$created=date('Y-m-d H:i:s');

		$sql="INSERT INTO `time_tracking` (`id`, `userid`, `projectid`, `taskid`, `memo`, `status`, `minutes`, `log_date`, `log_starttime`, `log_endtime`, `total_key_strokes`, `total_mouse_clicks`, `log_minutes_info`, `active_window`, `screenshot`, `createdby`, `created`, `modifiedby`, `modified`) VALUES (NULL, '$uid', '$pid', '$tid', '$memo', '$status', '$minActive', '$log_date', '$log_starttime', '$log_endtime', '$total_key_strokes', '$total_mouse_clicks', '$logtimedetails', '$activewindow', '$screenshot', '$uid', '$created', '$uid', '$created')";

		$res=mysql_query($sql) or die("TIME TRACKING ERR : ".mysql_error());

		if($res)
		{
			//$params = array("status"=>$status,"minutes"=>$minActive, "logLastTenMinutes"=>$logtimedetails, "log_date"=>$log_date, "log_starttime"=>$log_starttime, "log_endtime"=>$log_endtime,"first_logtimes"=>$first_logtimes, "last_logtimes"=>$last_logtimes, "total_mouse_clicks"=>$total_mouse_clicks, "total_key_strokes"=>$total_key_strokes);
	
			$params = array("status"=>"OK","msg"=>"TIME TRACKED SUCCESSFULLY");
			$json = json_encode($params,true);

			echo isset($_GET['callback']) ? "{$_GET['callback']}($json)" : $json;
		}else{

			$params = array("status"=>"Err","msg"=>"TIME NOT TRACKED");
	
			$json = json_encode($params,true);

			echo isset($_GET['callback']) ? "{$_GET['callback']}($json)" : $json;
		}

	}
}

?>
