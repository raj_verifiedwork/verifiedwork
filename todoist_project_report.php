<?php
$pageno=2;
$pagename=" Todoist Project Report ";

require_once('genfunctions.php');
require_once('vw_header.php');
require_once('vw_leftmenu.php');
	$sdate = date("Y-m-d",strtotime('monday this week'));
	$sd_date = $sdate;		
	$day_date = array($sd_date);
	for($i=1; $i<7; $i++)
	{
	$date1 = $sd_date;$date11 = str_replace('-', '/', $date1);
	$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
	array_push($day_date,$tomorrow );
	$sd_date = $tomorrow;
	}

?>
<style>
.input-group{
margin-left:3%;
}
.but{
margin-left:3%;
}
</style>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css">


<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
         <?php echo $pagename; ?>
       <!-- <small>it all starts here</small>-->
    </h1>
   <?php
	require_once('breadcrumb.php');
    ?>
</section>

    <br /><br />
  				
	<div class="form-group">
	<div class="input-group">
	Project : <select class="form-control proj_id" id="project">
	
	</select>
	</div></div>
	<div class="form-group">
                <div class="input-group">
                   Date range button: &nbsp;&nbsp;&nbsp; <input type="text" class="form-control week_pick" id="daterange-btn">
                </div>
            </div><!-- /.form group -->
	<button class="btn btn-success btn-sm but" id="ok">Success</button>
	  <br /><br />

<!-- Main content -->
<section class="content">
<?php 
$sql="select * from projects ORDER BY id DESC LIMIT 1";
$res=mysql_query($sql)or die("ERROR : ".mysql_error());
?>
<div class="atable">
<div class="box box-primary">
<div class="box-body table-responsive" id="table-responsive">
    <table id="example1" class="table table-bordered table-striped ">
        <thead>
            <tr>
                <th>Client</th>
                 <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
		<th>Thu</th>
		 <th>Fri</th>
		<th>Sat</th>
		<th>Sun</th>
		<th>Total Hours</th> 
            </tr>
        </thead>
        <tbody>
	<?php 
	$i=1;
	$r=mysql_fetch_array($res);
	$userid = $r['joinedpersons'];
	$pid = $r['id'];
		$uid = explode(",",$userid);	
		foreach($uid as $user_id){
		if(trim($user_id) != ''){
		$res=mysql_query("select * from users where id = '$user_id'")or die("ERROR : ".mysql_error());
		$row=mysql_fetch_array($res);
		$username = $row['fname'];
	  echo "<tr>";
               echo "<td>".$username."</td>";
               		 foreach($day_date as $weekdate){
			$sql1 = mysql_query("SELECT * FROM `task_compeletion_hrs` WHERE DATE(workdate) = '$weekdate' and userid='$user_id' and pid='$pid'"); 	
			$rows1= mysql_fetch_array($sql1);	
			echo "<td>".$rows1['hours']."</td>";
             		}
			$sql1 = mysql_query("SELECT SUM(hours) as tot_hours FROM `task_compeletion_hrs` where workdate BETWEEN '".$sdate. "'  AND '".$sd_date."' and userid='$user_id' and pid='$pid'");
			$row = mysql_fetch_array($sql1);	
			$hours = $row['tot_hours'];
              		  echo "<td>".$hours."</td>";
           echo "</tr>";
		$i++;
		}}
	?>
        </tbody>
     
    </table>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div>

</section><!-- /.content -->
</aside><!-- /.right-side -->

<?php
	require_once('vw_footer.php');
?>
<link href="assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
    });
</script>
<input type="text" name="proj" id="proj">
  <script type="text/javascript">
		Date.format = 'dd/mm/yyyy';
            $(function() {
               
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                'Last 7 Days': [moment().subtract('days', 6), moment()],
                                'Last 30 Days': [moment().subtract('days', 29), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                            },
                            startDate: moment().subtract('days', 29),
                            endDate: moment()
				
                        },
                function(start, end) {
                  // $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	       $('#reportrange span').html(start.format('dd/MM/yy') + ' - ' + end.format('dd/MM/yy'));
		
                }
		); 
		
            });
        </script>
      <script>
	$(document).ready(function(){
	$("#ok").click(function(){
		
		var x= $(".proj_id").val(); //alert(x);
		var y= $("#daterange-btn").val(); 
			$.ajax({
			    type: "POST",		
			    url: "get_todo_preport.php",
			    data:{ pid: x,date: y, type:'select'},
			    dataType: "html",
			    success: function(result) { //alert(result);
		$('.atable').html(result)
				}
		});
		
	});
	});
	</script>
