<?php
$pageno=2.1;
$pagename="Create Email Template ";

require_once('genfunctions.php');
require_once('vw_header.php');
require_once('vw_leftmenu.php');

$emailtype="";
$template_type="";
$subject="";
$content="";
$isystempl="0";

$action="";
$id="0";

if(isset($_REQUEST['job'])=="edit")
{
	$action="edit";

	$id=request_get('id');

	$sql="SELECT * FROM `email_templates` where id='$id'";
	$res=mysql_query($sql)or die("ERROR : ".mysql_error());

	$r=mysql_fetch_array($res);

	$emailtype=$r['emailtype'];
	$template_type=$r['template_type'];
	$subject=$r['subject'];
	$content=$r['content'];
	$isystempl=$r['is_system_template'];


}else{
	$action="add";
}

if(isset($_POST['submit']))
{
	$emailtype=request_get('emailtype');
	$template_type=request_get('template_type');
	$subject=safe_sql_nq(request_get('subject'));
	$content=safe_sql_nq(request_get('content'));

	$isystempl=0;

	if(isset($_POST['isystempl']))
	{
		$isystempl=1;
	}else{
		$isystempl=0;
	}

	$created=date('Y-m-d H:i:s');

	$sql="INSERT INTO `email_templates` (`id`, `emailtype`, `template_type`, `fromid`, `fromname`, `to`, `cc`, `bcc`, `subject`, `content`, `attachments`, `is_system_template`, `created`, `createdby`, `modified`, `modifiedby`) VALUES (NULL, '$emailtype', '$template_type', '', '', '', '', '', '$subject', '$content', '0', '$isystempl', '$created', 'createdby', '$created', 'modifiedby')";

	$res=mysql_query($sql) or die("ERROR : ".mysql_error());
	
}

if(isset($_POST['update']))
{
	$updid=request_get('updid');
	$emailtype=request_get('emailtype');
	$template_type=request_get('template_type');
	$subject=safe_sql_nq(request_get('subject'));
	$content=safe_sql_nq(request_get('content'));

	$isystempl=0;

	if(isset($_POST['isystempl']))
	{
		$isystempl=1;
	}else{
		$isystempl=0;
	}

	$modified=date('Y-m-d H:i:s');

	$sql="UPDATE `email_templates` SET `emailtype` = '$emailtype', `template_type` = '$template_type', `subject` = '$subject', `content` = '$content', `is_system_template`='$isystempl', `modified`='$modified', `modifiedby` = 'modifiedby' WHERE `id` = '$updid'";

	$res=mysql_query($sql) or die("ERROR : ".mysql_error());

	header("location:email_templates.php");
	exit;
	
}

//echo " template_type : ".$template_type;

?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       <?php echo $pagename; ?>
      <!--  <small>it all starts here</small>-->
    </h1>
    <?php
	require_once('breadcrumb.php');
    ?>
</section>
<!-- Main content -->
<section class="content">

<div class="row">
<!-- left column -->
<div class="col-md-7">
    <!-- general form elements -->
    <div class="box box-primary">
       <!-- <div class="box-header">
            <h3 class="box-title">Quick Example</h3>
        </div> /.box-header -->
        <!-- form start -->
        <form role="form" method="post" >
            <div class="box-body">
                <div class="form-group">
                    <label for="EmailType">Email Type</label>
  <input type="text" class="form-control" name="emailtype" id="EmailType" placeholder="Enter Email Type" value="<?php echo $emailtype; ?>" <?php if($isystempl==1){ echo "readonly"; } ?> >
                </div>
                <div class="form-group">
                    <label for="template_type">Template Type</label>
                    <select class="form-control" id="temptype" name="template_type">
                        <option value="0" <?php if($template_type=="0") echo "selected"; ?> >HTML FORMAT</option>
                        <option value="1" <?php if($template_type=="1") echo "selected"; ?> >TEXT FORMAT</option>
                    </select>
                </div>
		<div class="form-group">
                    <label for="subject">Subject</label>
                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject" value="<?php echo $subject; ?>">
                </div>
		<div class="form-group" id="textformat">
                    <label for="content">Content</label>
                    <textarea class="form-control" placeholder="Enter Content" name="content" rows="3" style="width: 100%; height: 200px;"><?php echo $content; ?></textarea>
                </div>
		<div class="form-group" id="htmlformat">
                    <label for="content">Content</label>
    			<textarea class="textarea" placeholder="Enter Content" name="content" style="width: 100%; height: 200px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $content; ?></textarea>
                </div>
<?php 
if($action=="edit")
{
	$res=mysql_query("select attachments from email_templates where id='".$id."'") or die(mysql_error());	
	$ra=mysql_fetch_array($res);
	$att=explode(",",$ra['attachments']);
?>
		<div class="form-group">
			<label for="attachment">Attachments</label>
			<?php 
			foreach($att as $at)
			{
				if(!empty($at)){
	echo "<div style='line-height:25px;' id='att_$id'><a href='download.php?type=download&filename=".$at."'><i class='glyphicon glyphicon-paperclip'></i>&nbsp;".basename($at)."</a> &nbsp;";
	?>

	<a href="#" onclick="deleteAttachment(<?php echo $id; ?>, '<?php echo $at; ?>')"><i class="glyphicon glyphicon-trash"></i></a></div>

	<?php 			}
			}
			?><br>
			<div id="queue">Drag & Drop Files Here</div>
			<input id="file_upload" name="file_upload" type="file" multiple="true">
			<div id="output" style="display:none;"></div>
			<input type="hidden" name="upimgids" id="upimgids">
		</div><br>
<?php 
}
?><br>
		<div class="form-group">
			<label for="subject">Is System Templete</label>
			<input type="checkbox" class="form-control" id="isystempl" name="isystempl" <?php if($isystempl==1) echo "checked"; ?> >
		</div>
            </div><!-- /.box-body -->

            <div class="box-footer">
		<?php if($action=="edit"){ ?>
			<input type="hidden" name="updid" value="<?php echo $id; ?>" />
               		<button type="submit" name="update" class="btn btn-primary ">Update</button>
		<?php }else{ ?>
			<button type="submit" name="submit" class="btn btn-primary ">Submit</button>
		<?php } ?>
            </div>
        </form>
    </div><!-- /.box -->
</div>
<div class="col-md-5">

	<div class="box box-primary">
		 <div class="box-body">
			   URL  - %url%    
		 </div><!-- /.box-body -->
	</div>

</div>

</div>

</section><!-- /.content -->
</aside><!-- /.right-side -->
<?php
	require_once('vw_footer.php');

	$timestamp=date('Y-m-d H:i:s');
?>
<link href="assets/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
<script src="assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<link href="assets/js/uploadify/uploadifive.css" rel="stylesheet" type="text/css" />
<style>
.uploadifive-button {
	float: left;
	margin-right: 10px;
}
#queue {
	border: 1px solid #E5E5E5;
	height: 200px;
	overflow: auto;
	margin-bottom: 10px;
	padding: 0 3px 3px;
	width: 100%;
}

</style>
<script src="assets/js/uploadify/jquery.uploadifive.min.js"></script>
<script>

function deleteAttachment(id,filename)
{
	var res=confirm("Are you sure do you want to delete this?");

	if(res==true)
	{
		var data={
			type:'deleteEmailAttachment',
			id:id,
			filename:filename
		};

		$.ajax({
			type:'POST',
			data:data,
			url:'deleteattachments.php',
			success:function(data){
				$("#att_"+id).hide();
				//window.location.reload();
			}
		});
	}

	return true;
}

$(function() {

	$(".textarea").wysihtml5();

	$("#textformat").hide();

	$("#temptype").change(function(){

		var type=$("#temptype").val();

		if(type=="1")
		{
			$("#textformat").show();
			$("#htmlformat").hide();
		}else{
			$("#textformat").hide();
			$("#htmlformat").show();
		}

	});
	 
	$('#file_upload').uploadifive({
	'auto'             : true,
	'formData'         : {
						   'timestamp' : '<?php echo $timestamp;?>',
						   'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'
	                     },
	'queueID'          : 'queue',
	'uploadScript'     : 'uploadifive.php?job=add&tabname=email_templates&id='+<?php echo $id; ?>,
	'queueSizeLimit'   : 5,
	'onProgress'   	   : function(file, e) {
			    if (e.lengthComputable) {
				var percent = Math.round((e.loaded / e.total) * 100);
			    }
			    file.queueItem.find('.fileinfo').html(' - ' + percent + '%');
			    file.queueItem.find('.progress-bar').css('width', percent + '%');
        }, 
	'onUploadComplete' : function(file, data) {

				$('#output').append(data+',');

				document.getElementById('upimgids').value = $('#output').html();

				file.queueItem.find('.close').html(data);
				
			}
	
	});
});

</script>
