<?php
$pageno=2;
$pagename=" Project Report ";

require_once('genfunctions.php');
require_once('vw_header.php');
require_once('vw_leftmenu.php');

if(isset($_POST['ok'])){
 $custom_date=request_get('date');
}

	if($custom_date == '')
	{
	$sdate = date("Y-m-d",strtotime('sunday this week'));
	$sd_date = $sdate;		
	$day_date = array($sd_date);
		for($i=1; $i<7; $i++)
		{
			$date1 = $sd_date;$date11 = str_replace('-', '/', $date1);
			$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
			array_push($day_date,$tomorrow );
			$sd_date = $tomorrow;
		}
	$sw=date('m/d/Y',strtotime($sdate)); 
	$ew=date('m/d/Y',strtotime($sd_date)); 
	$custom_date = $sw."-".$ew;

	}
	else
	{
	$week_date=explode('-',$custom_date);
	$sdate =date('Y-m-d',strtotime($week_date[0])); 
	$edate =date('Y-m-d',strtotime($week_date[1]));
	$date1=date_create($sdate); $date2=date_create($edate);
	$diff=date_diff($date1,$date2);
	$c=$diff->format("%R%a")+1;
	$sd_date = $sdate;		
	$day_date = array($sd_date);
		for($i=1; $i<$c; $i++)
		{
		$date1 = $sd_date;
		$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
		array_push($day_date,$tomorrow );
		$sd_date = $tomorrow;
		} 
	}

	$date1=date_create($sdate);
	$date2=date_create($sd_date);
	$diff=date_diff($date1,$date2);
	$c=$diff->format("%R%a")+1;

$fdate=$sdate;
$day=strftime("%a",strtotime($fdate));
$week_day=array($day);
	for($i=1; $i<$c; $i++){
		$nxtdate = date('Y-m-d',strtotime($fdate . "+1 days"));
		$nxtday=strftime("%a",strtotime($nxtdate));
		array_push($week_day,$nxtday );
		$fdate = $nxtdate;
		
	}

?>
<style>
	.input-group{
	margin-left:3%;
	}

	.file{
	/*display:-moz-inline-grid; */
	display: inline-block;
	vertical-align: top; 
	width: 80%;
	word-break: break-all;
	
	}

.loader {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url('assets/img/ajax-loader1.gif') 50% 50% no-repeat rgb(249,249,249);
	}
	
	
</style>
	

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
<!-- Content Header (Page header) -->
	<section class="content-header">
	    <h1>
		 <?php echo $pagename; ?>
	       <!-- <small>it all starts here</small>-->
	    </h1>
	   <?php
		require_once('breadcrumb.php');
	    ?>
	</section><!-- /.section -->

<form method="POST" action="">
	<div class="form-group">
                <div class="input-group">
                  <h4>Date range button :&nbsp;
		<a href="#" id="prev"><img src="img/back.png" height="15px" width="15px"></a>
		<input type="text" id="datepicker" required="required" name="date" class="val" value="<?php echo $custom_date; ?>" />
		<a href="#" id="next"><img src="img/forward.png" height="15px" width="15px"></a>
		</h4>
                </div>
		
         </div><!-- /.form group -->
		<button type="submit" class="btn btn-success btn-sm but" id="ok" name="ok" style="display:none;" >Success</button>
	  <br /><br />

</form>

<!-- Main content -->
<section class="content">
<div class="loader"></div>
<div class="box box-primary ">
	<div class="box-body table-responsive component" id="table-responsive">


		
<table id="example1" class="table table-bordered table-condensed " style="font-size:15px;">
	<caption style="font-size:large;">
          <a href="#" onclick="jQuery('#example1').treetable('expandAll'); return false;">Expand all</a>
          <a href="#" onclick="jQuery('#example1').treetable('collapseAll'); return false;">Collapse all</a>
        </caption>	
 			<thead style="font-size:larger;">

            		<tr>
				<th id="tbltask" >Project</th>
				<!--<th>Mon</th>
				<th>Tue</th>
				<th>Wed</th>
				<th>Thu</th>
				 <th>Fri</th>
				<th>Sat</th>
				<th>Sun</th>-->
<?php 
	foreach($week_day as $days){
		if($days=="Sun"){
		 	echo "<th>".$days."</th>";
		}
		else
		 {
		 	echo "<th>".$days."</th>";

		}
	 } 
?>
				<th>Total Hours</th> 
			    </tr>
			</thead>
		<tbody>

<?php
$i=1;
$j=1;
$k=1;
	//select  projects
	$res1=mysql_query("select  projectname,userid,id,joinedpersons  from projects  ")or die("ERROR : ".mysql_error());
	
	while($row1=mysql_fetch_array($res1)){
		$userids=$row1['userid'];
		$projname=$row1['projectname'];
		$pid=$row1['id'];
		$joinuser=$row1['joinedpersons'];
	
		echo "<tr data-tt-id='".$pid.$k."project'   class='proj_name ' >
		<td class='task_row project_name'>".$projname."</td>";
		
		foreach($day_date as $weekdate){
			$sql1 = mysql_query("SELECT SUM(hours) as hours FROM `task_compeletion_hrs` WHERE DATE(workdate) = '$weekdate' and userid='$userids' and pid='$pid'")or die("ERROR : ".mysql_error()); 	
			$rows1= mysql_fetch_array($sql1);	
			echo "<td>".ROUND($rows1['hours'],2)."</td>";
             	}//end for each Loop

			$sql = mysql_query("SELECT SUM(hours) as tot_hours FROM `task_compeletion_hrs` where workdate BETWEEN '".$sdate. "'  AND '".$sd_date."' and userid='$userids' and pid='$pid'")or die("ERROR : ".mysql_error());
			$row = mysql_fetch_array($sql);	
			$hours = $row['tot_hours'];
              		  echo "<td>".ROUND($hours,2)."</td>";

		echo "</tr>";
		
$joinuser=$row1['joinedpersons'];

$juser = explode(",",$joinuser);
foreach($juser as $userid){ 	//array_filter($user_id);
if(trim($userid) != ''){


	// select  users
$res=mysql_query("select * from users where id='$userid' ")or die("ERROR : ".mysql_error());
	while($row=mysql_fetch_array($res)){
		$uname = $row['fname'];
		$uid=$row['id'];
				 
	echo "<tr data-tt-id=".$i.$uid."'_user'  data-tt-parent-id='".$pid.$k."project' class='user_name '  >
	<td class='user' ><span class='folder'>".$uname."</span></td>";

	 		foreach($day_date as $weekdate){
				$sql1 = mysql_query("SELECT SUM(hours) as hours FROM `task_compeletion_hrs` WHERE DATE(workdate) = '$weekdate' and userid='$uid' and pid='$pid' ")or die("ERROR : ".mysql_error()); 	
				$rows1= mysql_fetch_array($sql1);	
				echo "<td>".ROUND($rows1['hours'],2)."</td>";
		     	}//end for each Loop

			$sql = mysql_query("SELECT SUM(hours) as tot_hours FROM `task_compeletion_hrs` where workdate BETWEEN '".$sdate. "'  AND '".$sd_date."' and userid='$uid' and pid='$pid'")or die("ERROR : ".mysql_error());
			$row = mysql_fetch_array($sql);	
			$hours = ROUND($row['tot_hours'],2);
              		  echo "<td>".$hours."</td>";

		echo "</tr>";
		
	// select Tasks
		$res2=mysql_query("select * from tasks where pid='$pid' and userid='$uid'")or die("ERROR : ".mysql_error());
		while($row2=mysql_fetch_array($res2)){
			$taskname=$row2['taskname'];
			$taskid=$row2[id];
			echo "<tr data-tt-id='".$j."' data-tt-parent-id=".$i.$uid."'_user'  class='task_name info' ><td class='task_row'><span class='file' >".$taskname."</span></td>"; 

		foreach($day_date as $weekdate){
			$sql1 = mysql_query("SELECT SUM(hours) as hours FROM `task_compeletion_hrs` WHERE DATE(workdate) = '$weekdate' and userid='$uid'  and pid='$pid' and taskid='$taskid'")or die("ERROR : ".mysql_error()); 	
			$rows1= mysql_fetch_array($sql1);	
			echo "<td>".ROUND($rows1['hours'],2)."</td>";
             	}//end for each Loop

			$sql = mysql_query("SELECT SUM(hours) as tot_hours FROM `task_compeletion_hrs` where workdate BETWEEN '".$sdate. "'  AND '".$sd_date."' and userid='$uid' and pid='$pid' and taskid='$taskid'")or die("ERROR : ".mysql_error());
			$row = mysql_fetch_array($sql);	
			$hours = ROUND($row['tot_hours'],2);
              		  echo "<td>".$hours."</td>";

			echo "</tr>";
			$j++;
	}
		}
			}//end While loop
	 	$i++;
		}  //end While loop
$k++;	
} //end While loop 
?>
		</tbody>
	</table>
     </section><!-- /.content -->

</div>
</div>
</aside><!-- /.right-side -->

<?php
	require_once('vw_footer.php');
?>
 
<script>
$(document).ready(function(){
$(window).load(function() {
			$(".loader").fadeOut("slow");
		});	
$("#next").click(function(){ 
	var x =$(".val").val();
	$.ajax({
		    type: "POST",		
		    url: "get_date.php",
		    data:{ name: x, type:'next'},
		    dataType: "html",
		    success: function(result) {
			$(".val").val(result);
			$("#ok").click();
		   }
	});

});
$("#prev").click(function(){ 
	var x =$(".val").val();//alert(x);
	$.ajax({
		    type: "POST",		
		    url: "get_date.php",
		    data:{ name: x, type:'prev'},
		    dataType: "html",
		    success: function(result) {
			$(".val").val(result);
			$("#ok").click();
		    }
	});
});
 
	$('#datepicker').datepicker({
			format: 'mm/dd/yyyy',
			"setDate": new Date(), 
		});

		$('#datepicker').on('changeDate', function(ev){
			var x=$('#datepicker').val(); 
	 		datebuttonval(x);
			$(this).datepicker('hide');
			$("#ok").click(); 
		});

		$(".cus_today").click(function(){
			$('#datepicker').datepicker('setValue', today);
			var x=$('#datepicker').val(); 
	 		datebuttonval(x);
			$('#datepicker').datepicker('hide');
			$("#ok").click();	
		});
		$(".cus_tomorrow").click(function(){	
			$('#datepicker').datepicker('setValue', tomorrow);
			var x=$('#datepicker').val(); 
	 		datebuttonval(x);
			$('#datepicker').datepicker('hide');
			$("#ok").click();
		});
		$(".cus_nextweek").click(function(){
			$('#datepicker').datepicker('setValue', nextweek);
			var x=$('#datepicker').val(); 
	 		datebuttonval(x);
			$('#datepicker').datepicker('hide');
			$("#ok").click();
		});
		$('.datepicker').on('mousemove', 'tr', function () {
			$(this).css("background","#EEEEEE");        
		 });
		$('.datepicker').on('mouseleave', 'tr', function () {
			$(this).css("background","#FFFFFF");       
		});
		function datebuttonval(x){

			var curr = new Date(x);  
			var day = curr.getDay();
			firstday = new Date(curr.getTime() - 60*60*24* day*1000);
			var x=myDateFormatter (firstday); //alert(x);
			lastday = new Date(firstday.getTime() + 60 * 60 *24 * 6 * 1000); 
			var y=myDateFormatter (lastday); //alert(y);
			$('#datepicker').val(x + ' - ' + y); 
		}
		function myDateFormatter (dateObject) {
			var d = new Date(dateObject);
			var day = d.getDate();
			var month = d.getMonth() + 1;
			var year = d.getFullYear();
			if (day < 10) {
			day = "0" + day;
			}
			if (month < 10) {
			month = "0" + month;
			}
			var date = month + "/" + day + "/" + year;
			return date;
		};

	

});
</script>


      
<link rel="stylesheet" href="assets/css/treetable/jquery.treetable.theme.default.css" />
<script type="text/javascript" src="assets/js/jquery.treetable.js"></script>

<!--<link href="assets/css/treetable/week-picker-view.css" rel="stylesheet"/> 
<script type="text/javascript" src="assets/js/week-picker.js"></script>-->

<script>
 $("#example1").treetable({ expandable: true });

      // Highlight selected row
       $("#example1 tbody").on("mousedown", "tr", function() {
        $(".selected").not(this).removeClass("selected");
        $(this).toggleClass("selected");
      });

</script>

	


